﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using Simplexity.RatingEngine;
using Simplexity.RatingEngine.DTOs;
using Simplexity.RatingService;

namespace Simplexity.RatingApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class RatingController : ApiController
    {

        [HttpPost]
        [Route("CalculateSupplierRates")]
        public IHttpActionResult GetSupplierRates([FromBody] RateLoad rateLoad)
        {
            try
            {
                var ratingService = new RatingEngine.RatingService();
                var respuesta = ratingService.CalculateSupplierRates(rateLoad.RateLoadDto, rateLoad.SchemaCode, DateTime.Now);

                if(!respuesta.RateLines.Any())
                    return Ok(new Result(false, "No se encontraron tarifas."));

                if (respuesta.Message.HasError())
                {
                    return Ok(new Result(false, respuesta.Message.Message));
                }
                return Ok(new Result<List<RateLineDTO>>(true, respuesta.RateLines));
            }
            catch (Exception ex)
            {
                return Ok(new Result<string>(false, ex.Message));
            }
        }
    }
}
