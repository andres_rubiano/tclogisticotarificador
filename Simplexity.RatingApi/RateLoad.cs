﻿using Simplexity.RatingEngine.DTOs;

namespace Simplexity.RatingApi
{
    public class RateLoad
    {
        public RateLoadDTO RateLoadDto { get; set; }
        public string SchemaCode { get; set; }
    }
}