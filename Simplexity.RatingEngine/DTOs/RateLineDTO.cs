﻿namespace Simplexity.RatingEngine.DTOs
{
    /// <summary>
    /// Este DTO es el que usa el RatingEngine para depositar las lineas de tarifa calculadas
    /// Del otro lado, el usuario del RatingEngine puede decidir que hacer con estas lineas, si
    /// persistirlas, mostrarlas en el simulador, etc.
    /// </summary>
    public class RateLineDTO
    {
        public int Nat { get; set; }
        public string ArtCode { get; set; }
        public string ArtName { get; set; }
        public string CustomerCode { get; set; }
        public string UomCode { get; set; }
        public double Quantity { get; set; }
        public double UnitPrice { get; set; }
        public double ExtendedPrice { get; set; }
        public string SchemaCode { get; set; }
        public string ModelCode { get; set; }
        public string ModelName { get; set; }
        public int RateId { get; set; }
        public string RateCode { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string ShipmentNumber { get; set; }
        public string ShipmentDocumentNumber { get; set; }
        public string LoaNumber { get; set; }
        public string SupplierCode { get; set; }
        public string SupplierName { get; set; }
        public string Memo { get; set; }
    }
}
