﻿using System.Collections.Generic;
using BusinessRules.Main.DTOs.ServiceRating;

namespace Simplexity.RatingEngine.DTOs
{
    public class RatingSchemaDTO
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string RatesBatchNumber { get; set; }
        public List<RatingModelDTO> Models { get; set; }
    }
}
