﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Simplexity.RatingEngine.DTOs
{
  public class FiltroTarifaSemiautomaticaDto
  {
    public short Naturaleza { get; set; }
    public string EsquemaCodigo { get; set; }
    public string ModeloTarifa { get; set; }
    public string TerceroCodigo { get; set; }
    public string ArticuloCodigo { get; set; }
    public string UnidadDeMedidaCodigo { get; set; }
    public string CiudadOrigenCodigo { get; set; }
    public string CiudadDestinoCodigo { get; set; } 

  }
}
