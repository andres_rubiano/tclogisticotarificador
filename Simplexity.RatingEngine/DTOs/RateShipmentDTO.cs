﻿using System;
using System.Collections.Generic;
using BusinessRules.Main.DTOs.ServiceRating;

namespace Simplexity.RatingEngine.DTOs
{
    public class RateShipmentDTO
    {
        public string ShiNumber { get; set; }
        public string ShiLoadLoaNumber { get; set; }
        public string ShtCode { get; set; }
        public string CustomerUcrCode { get; set; }
        public string SubCode { get; set; }
        public string ComCode { get; set; }
        public DateTime? PickupFromDateTime { get; set; }
        public DateTime? PickupToDateTime { get; set; }
        public DateTime? DeliveryFromDatetime { get; set; }
        public DateTime? DeliveryToDateTime { get; set; }
        public DateTime? DispatchDate { get; set; }
        public string ShipFromSubCode { get; set; }
        public string ShipFromZonCode { get; set; }
        public string ShipFromCitCode { get; set; }
        public string ShipFromUloCode { get; set; }
        public string ShipFromUcrCode { get; set; }
        public string ShipToCitCode { get; set; }
        public string ShipToZonCode { get; set; }
        public string ShipToUloCode { get; set; }
        public string ShipToUcrCode { get; set; }
        public string ShipToSubCode { get; set; }
        public string UomCode { get; set; }
        public string ProCode { get; set; }
        public string ProductTypeCode { get; set; }
        public string UnloadingType { get; set; }
        public double WeightKg { get; set; }
        public double Quantity { get; set; }
        public double VolumeM3 { get; set; }
        public double ShiTotalWeightKg { get; set; }
        public double? ShiTotalVolumeM3 { get; set; }
        public double QuantityShipped { get; set; }
        public double WeightKgShipped { get; set; }
        public double QuantityDelivered { get; set; }
        public double WeightKgDelivered { get; set; }
        public string BopCode { get; set; }
        public string Incoterm { get; set; }
        public string RouCode { get; set; }
        public string TypeOfPackaging { get; set; }
        public double ManualTotalFreigh { get; set; }
        public double TotalDelaredValue { get; set; }
        public string RouteLegType { get; set; }
        public DateTime? CreationDate { get; set; }
        public List<RateShiDetailDTO> ShiDetails { get; set; }
        public List<RateWaybillDTO> Waybills { get; set; }
    }
}
