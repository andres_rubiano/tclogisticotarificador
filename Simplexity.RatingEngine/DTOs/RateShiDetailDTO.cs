﻿namespace BusinessRules.Main.DTOs.ServiceRating
{
    public class RateShiDetailDTO
    {
        public string ShiNumber { get; set; }
        public string LoaNumber { get; set; }
        public string SubCode { get; set; }
        public string ProCode { get; set; }
        public string UomCode { get; set; }
        public double Quantity { get; set; }
        public double WeightKg { get; set; }
        public double VolumeM3 { get; set; }
        public double EqPallet { get; set; }
        public double DeclaredValue { get; set; }
        public string DeclaredCurrencyCode { get; set; }
        public string SupplierUcrCode { get; set; }
        public string CustomerUcrcode { get; set; }
        public string ShipToUcrCode { get; set; }
        public string ShipToCitCode { get; set; }
        public string ShipToUloCode { get; set; }
        public string ShipToZonCode { get; set; }
    }
}
