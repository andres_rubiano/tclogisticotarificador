﻿using System;
using System.Collections.Generic;

namespace Simplexity.RatingEngine.DTOs
{
    public class RateLoadDTO
    {
        public string LoaNumber { get; set; }
        public string LoadType { get; set; }
        public string TransportMode { get; set; }
        public string VtyCode { get; set; }
        public string VcnCode { get; set; }
        public string VehGroup { get; set; }
        public float VehCapacityKg { get; set; }
        public float VehCapacityM3 { get; set; }
        public float VehCapacityPallets { get; set; }
        public string UnloadingType { get; set; }
        public string Truck { get; set; }
        public string Trailer { get; set; }
        public char RouteLegType { get; set; }
        public string DriverUcrCode { get; set; }
        public string LoaFromSubCode { get; set; }
        public string LoaFromCitCode { get; set; }
        public string LoaToSubCode { get; set; }
        public string LoaToCitCode { get; set; }
        public string LoaToZoneCode { get; set; }
        public string RouCode { get; set; }
        public string CarrierUcrCode { get; set; }
        public string ComCode { get; set; }
        public string BopCode { get; set; }
        public double WorkedHours { get; set; }
        public double? WeightKg { get; set; }
        public double WeightKgShipped { get; set; }
        public double WeightKgDelivered { get; set; }
        public string CrewType { get; set; }
        public bool? LoadPartOfTrip { get; set; }
        public string RoundTripLoad { get; set; }
        public short Nat { get; set; }
        public short? SecurityServiceAmount { get; set; }
        public double ManualTotalFreigh { get; set; }
        public string AdministratorUcrCode { get; set; }
        public DateTime? LoaCreationDate { get; set; }
        public List<RateShipmentDTO> Shipments { get; set; }
    }
}
