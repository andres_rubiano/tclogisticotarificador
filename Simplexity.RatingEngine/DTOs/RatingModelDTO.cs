﻿using System.Collections.Generic;

namespace Simplexity.RatingEngine.DTOs
{
    public class RatingModelDTO
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public int NonZeroValueRequired { get; set; }
        public string DefaultArticle { get; set; }
        public string RatesBatchNumber { get; set; }
        public List<RatingModelsAlternateDTO> AlternateModels{ get; set; }
       
    }
}
