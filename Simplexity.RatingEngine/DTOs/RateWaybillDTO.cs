﻿using System;

namespace Simplexity.RatingEngine.DTOs
{
    public class RateWaybillDTO
    {
        public DateTime Date { get; set; } // Date
        public string Number { get; set; } // Number (Primary key)
        public string TypeWtyCode { get; set; } // Type_WtyCode
        public string DispatchNumber { get; set; } // DispatchNumber
        public DateTime? DispatchDate { get; set; } // DispatchDate
        public DateTime? DispatchedDate { get; set; } // DispatchedDate
        public string CustomerUcrCode { get; set; } // Customer_UcrCode
        public string SubsidiarySubCode { get; set; } // Subsidiary_SubCode
        public string CompanyComCode { get; set; } // Company_ComCode
        public DateTime? PickupFromDatetime { get; set; } // PickupFromDatetime
        public DateTime? PickupToDatetime { get; set; } // PickupToDatetime
        public DateTime? DeliveryFromDatetime { get; set; } // DeliveryFromDatetime
        public DateTime? DeliveryToDatetime { get; set; } // DeliveryToDatetime

        public int Quantity { get; set; } // Quantity
        public double TotalWeightKg { get; set; } // TotalWeightKg
        public double TotalVolumeM3 { get; set; } // TotalVolumeM3
        public double EquivalentWeightKg { get; set; } // EquivalentWeightKg
        public double CargoDeclaredValue { get; set; } // CargoDeclaredValue
        public double CargoCurrencyCode { get; set; } // CargoCurrencyCode
        public double FreightValue { get; set; } // FreightValue
        public double InsuranceValue { get; set; } // InsuranceValue
        public string ProductProCode { get; set; } // Product_ProCode
        public string UomUomCode { get; set; } // UOM_UomCode
        public string AditionalReference1 { get; set; } // AditionalReference1
        public string AditionalReference2 { get; set; } // AditionalReference2
        public DateTime? DestinationArrivalDate { get; set; } // DestinationArrivalDate
        public DateTime? DestinationDepartureDate { get; set; } // DestinationDepartureDate
        public DateTime? CloseDate { get; set; } // CloseDate
        public string InvoiceNumber { get; set; } // InvoiceNumber
        public string PickupPreNumber { get; set; }
        public string CurrentUloCode { get; set; }
        public string CurrentSubCode { get; set; }
        public string CurrentLoaNumber { get; set; }
        public string CurrentShiNumber { get; set; } // Current_ShiNumber
        public string TypeBussWtbCode { get; set; } // TypeBuss_WtbCode

        // From
        public string ShipFromCityCitCode { get; set; } // ShipFromCity_CitCode
        public string ShipFromLocationUloCode { get; set; } // ShipFromLocation_UloCode
        public string ShipFromUcrCode { get; set; } // ShipFrom_UcrCode
        public string ShipFromUcrName { get; set; } // ShipFrom_UcrName

        // To
        public string ShipToCityCitCode { get; set; } // ShipToCity_CitCode
        public string ShipToLocationUloCode { get; set; } // ShipToLocation_UloCode
        public string ShipToUcrCode { get; set; } // ShipTo_UcrCode
        public string ShipToUcrName { get; set; } // ShipTo_UcrName

        // Datos de ruta y otros
        public string RouteRouCode { get; set; } // Route_RouCode
        public string RouteLegType { get; set; } // Aqui vamos a tener el tipo de trayecto


      //extras para que apliquen a suppla
        public string WayCustomerBusinessLineUcrCode { get; set; }
        public string WayBusinessOperationTypeBopCode { get; set; }

    }
}
