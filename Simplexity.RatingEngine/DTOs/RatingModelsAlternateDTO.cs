﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Simplexity.RatingEngine.DTOs
{
    public class RatingModelsAlternateDTO
    {
        public string Code { get; set; }
        public string CodeSchema { get; set; }
    }
}
