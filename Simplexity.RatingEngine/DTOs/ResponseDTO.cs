﻿using System.Collections.Generic;

namespace Simplexity.RatingEngine.DTOs
{
  public class ResponseDTO
  {
    private MessageDTO _message;
    private List<RateLineDTO> _rateLines;

    public MessageDTO Message
    {
      get { return _message ?? (_message = new MessageDTO()); }
      set { _message = value; }
    }

    public List<RateLineDTO> RateLines
    {
      get { return _rateLines ?? (_rateLines = new List<RateLineDTO>()); }
      set { _rateLines = value; }
    }
  }
}