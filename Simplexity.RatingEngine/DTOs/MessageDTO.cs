﻿using System.Xml.Serialization;
using Simplexity.RatingEngine.Constants;

namespace Simplexity.RatingEngine.DTOs
{
    public class MessageDTO
    {
        private MessagesConstants.Types _typeEnum;

        public MessageDTO()
        {
            _typeEnum = MessagesConstants.Types.Information;
        }

        [XmlIgnore]
        public MessagesConstants.Types TypeEnum
        {
            set { _typeEnum = value; }
            get { return _typeEnum; }
        }


        public int Type
        {
            get { return (int)_typeEnum; }
        }

        public string Message { get; set; }

        public string TransactionNumber { get; set; }

        public void SetInError(string message)
        {
            _typeEnum = MessagesConstants.Types.Error;
            Message = message;
        }

        public void SetToOk(string message)
        {
            Message = message;
        }

        public void SetToOk(string message, string transactionNumber)
        {
            Message = message;
            TransactionNumber = transactionNumber;
        }

        public bool HasError()
        {
            return _typeEnum == MessagesConstants.Types.Error;
        }
    }
}