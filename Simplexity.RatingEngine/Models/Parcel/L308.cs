﻿using System;
using System.Collections.Generic;
using Simplexity.RatingEngine.Constants;
using Simplexity.RatingEngine.DTOs;

namespace Simplexity.RatingEngine.Models.Parcel
{
  // Flete x unidad de peso teniendo en cuenta rango de pesos
  public class L308 : ParcelRate, IRating
  {
    public ResponseDTO Calculate(RateLoadDTO load, RatingSchemaDTO schema, RatingModelDTO model, List<RateLineDTO> rateLines)
    {
      
      #region 1. Load Objects

      LoadByWeightRange(load, schema, model);

      #endregion

      #region 2. Calculate Freight

      if ((ServiceRate.TrrValue * BillableWeight) < MinFreight)
      {
        RateLineDTO.Quantity = 1;
        RateLineDTO.UnitPrice = MinFreight;
        RateLineDTO.UomCode = RatesUnitsOfMeasure.MinFreight;
      }
      else
      {
        RateLineDTO.Quantity = BillableWeight;
        RateLineDTO.UnitPrice = ServiceRate.TrrValue;
        RateLineDTO.UomCode = ServiceRate.TrrRange_UOMCode;
      }

      RateLineDTO.Nat = (int)RatesConstants.Nature.Positive;
      RateLineDTO.ExtendedPrice = RateLineDTO.Quantity * RateLineDTO.UnitPrice;

      if (Math.Abs(ServiceRate.TrrValue - 0) > RatesConstants.MinDoubleValue && Math.Abs(RateLineDTO.ExtendedPrice - 0) > RatesConstants.MinDoubleValue)
      {
        ResponseDTO.RateLines.Add(RateLineDTO);
      }

      #endregion

      return ResponseDTO;
    }
  }
}
