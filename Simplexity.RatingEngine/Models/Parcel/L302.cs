﻿using System;
using System.Collections.Generic;
using Simplexity.RatingEngine.Constants;
using Simplexity.RatingEngine.DTOs;

namespace Simplexity.RatingEngine.Models.Parcel
{
  // Flete x unidad de peso teniendo en cuenta x peso mínimo primera unidad y peso mínimo unidad adicional
  public class L302 : ParcelRate,  IRating
  {
    public ResponseDTO Calculate(RateLoadDTO load, RatingSchemaDTO schema, RatingModelDTO model, List<RateLineDTO> rateLines)
    {
      #region 1. Load Objects

      Load(load, schema, model);

      #endregion

      #region 3. Load parameters
      
      var minWeightByFirstUnit = GetParameterValue(RatesArticles.MinWeightByFirstUnit);
      var minWeightByAdditionalUnit = GetParameterValue(RatesArticles.MinWeightByAdditionalUnit);
      
      #endregion

      #region  5. Calculate weightToBill

      if(Waybill.TotalWeightKg < RealWeightAdjustment)
      {

        var maxWeight = Math.Max(Waybill.TotalWeightKg, VolumetricWeight);
        var minWeight = minWeightByFirstUnit + (minWeightByAdditionalUnit * (Waybill.Quantity - 1));

        BillableWeight = minWeight < maxWeight ? maxWeight : minWeight;
				var demopesoatraificar = Math.Max(Math.Max(Waybill.TotalWeightKg, VolumetricWeight), minWeight);
      }
      else
      {
        BillableWeight = Waybill.TotalWeightKg;
      }

      #endregion

      #region 6. Calculate Freight
      if ((BillableWeight* ServiceRate.TrrValue) < MinFreight)
      {
        RateLineDTO.Quantity = 1;
        RateLineDTO.UnitPrice = MinFreight;
        RateLineDTO.UomCode = RatesUnitsOfMeasure.MinFreight;
      }
      else
      {
        RateLineDTO.Quantity = BillableWeight;
        RateLineDTO.UnitPrice = ServiceRate.TrrValue;
        RateLineDTO.UomCode = RatesUnitsOfMeasure.Kilogram;
      }

     //naturaleza--(-1) a pagar (+1) a cobrar
      RateLineDTO.Nat = (int)RatesConstants.Nature.Positive;
      RateLineDTO.ExtendedPrice = RateLineDTO.Quantity*RateLineDTO.UnitPrice;

      if (Math.Abs(ServiceRate.TrrValue - 0) > RatesConstants.MinDoubleValue && Math.Abs(RateLineDTO.ExtendedPrice - 0) > RatesConstants.MinDoubleValue)
      {
        ResponseDTO.RateLines.Add(RateLineDTO);
      }

      #endregion

      return ResponseDTO;
    }
  }
}