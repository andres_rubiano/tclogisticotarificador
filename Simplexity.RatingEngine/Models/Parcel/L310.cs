﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simplexity.RatingEngine.Constants;
using Simplexity.RatingEngine.DTOs;

namespace Simplexity.RatingEngine.Models.Parcel
{
    public class L310 : ParcelRate, IRating
    {
        public ResponseDTO Calculate(RateLoadDTO load, RatingSchemaDTO schema, RatingModelDTO model, List<RateLineDTO> rateLines)
        {
            #region 1. Load objects

            Load(load, schema, model);
            LoadGeneralParameters();
            
            #endregion

            #region 2. Load specific parameters

            var minWeightByUnit = GetParameterValue(RatesArticles.MinWeightByUnit);
            var minWeightByUniqueUnit = GetParameterValue(RatesArticles.MinWeightByUniqueUnit);
            var handlingCostOverDeclaredValue = GetParameterValue(RatesArticles.HandlingCostOverDeclaredValue);
            var minHandlingCostByDispatch = GetParameterValue(RatesArticles.MinHandlingCostByDispatch);

            
            //discount calculated?
            //var freightDiscount = GetParameterValue(RatesArticles.FreightDiscount);
            //var invoiceDiscount = GetParameterValue(RatesArticles.InvoiceDiscount);

            #endregion

            #region 3. Calculate WeightToBill

            if (Waybill.TotalWeightKg < RealWeightAdjustment)
            {
                var realWeight = Waybill.TotalWeightKg / Waybill.Quantity;
                var volWeight = VolumetricWeight / Waybill.Quantity;
                BillableWeight = Waybill.Quantity > 1
                    ? new[] { volWeight, realWeight, minWeightByUnit }.Max()*Waybill.Quantity
                    : new[] { volWeight, realWeight, minWeightByUnit, minWeightByUniqueUnit }.Max();
            }
            else
            {
                BillableWeight = Waybill.TotalWeightKg;
            }

            #endregion

            #region 4. Calculate Freight

            var handlingCost = Math.Max(Waybill.CargoDeclaredValue * handlingCostOverDeclaredValue, minHandlingCostByDispatch);

            var valueBillable = BillableWeight*ServiceRate.TrrValue;
            var valueMinFreight = (MinFreight - handlingCost/Waybill.Quantity)*Waybill.Quantity;

            if (valueBillable > valueMinFreight)
            {
                RateLineDTO.Quantity = BillableWeight;
                RateLineDTO.UnitPrice = ServiceRate.TrrValue;
                RateLineDTO.UomCode = RatesUnitsOfMeasure.Kilogram;
            }
            else
            {
                RateLineDTO.Quantity = Waybill.Quantity;
                RateLineDTO.UnitPrice = valueMinFreight/Waybill.Quantity;
                RateLineDTO.UomCode = RatesUnitsOfMeasure.MinFreight;
            }

            RateLineDTO.Nat = (int)RatesConstants.Nature.Positive;
            RateLineDTO.ExtendedPrice = RateLineDTO.Quantity * RateLineDTO.UnitPrice;

            #endregion



            ResponseDTO.RateLines.Add(RateLineDTO);

            return ResponseDTO;
        }
    }
}
