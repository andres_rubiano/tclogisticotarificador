﻿using System;
using System.Collections.Generic;
using System.Linq;
using Simplexity.RatingEngine.Constants;
using Simplexity.RatingEngine.DTOs;
using Simplexity.RatingEngine.EF;
using Simplexity.RatingEngine.Resources;

namespace Simplexity.RatingEngine.Models.Parcel
{
  public class ParcelRate : RateBase
  {
    #region Members
    private RateWaybillDTO _waybill;
    private ResponseDTO _responseDTO;
    private RateLineDTO _rateLine;
    private trServiceRates _serviceRate;
    private RatingModelDTO _model;
    private RatingSchemaDTO _schema;
    #endregion

    #region Properties
    public double Weight { get; set; }
    public double VolumetricWeight 
    {
      get
      {
        return _waybill.TotalVolumeM3 * WeightVolumeRatio;  
      }
    } 
    public double WeightVolumeRatio { get; set; }
    public double MinFreight { get; set; }
    public double RealWeightAdjustment { get; set; }
    public double BillableWeight { get; set; }

    public ResponseDTO ResponseDTO
    {
      get { return _responseDTO ?? (_responseDTO = new ResponseDTO() ); }
      set { _responseDTO = value; }
    }

    private RatingSchemaDTO Schema
    {
      get { return _schema; }
      set { _schema = value; }
    }

    private RatingModelDTO Model
    {
      get { return _model; }
      set { _model = value; }
    }

    public RateWaybillDTO Waybill
    {
      get { return _waybill; }
      set { _waybill = value; }
    }

    public trServiceRates ServiceRate
    {
      get { return _serviceRate; }
      set { _serviceRate = value; }
    }

    public RateLineDTO RateLineDTO
    {
      get { return _rateLine; }
      set { _rateLine = value; }
    }
   #endregion


    public void Load(RateLoadDTO load, RatingSchemaDTO schema, RatingModelDTO model)
    {
      // 1. Load objects
      LoadBaseProperties(load, schema, model);
      LoadFreightServiceRate();
      CheckMandatoryServiceRate();
      LoadGeneralParameters();
      LoadRateLineDefaultValues();
    }

    public void LoadByWeightRange(RateLoadDTO load, RatingSchemaDTO schema, RatingModelDTO model)
    {
      // 1. Load objects
      LoadBaseProperties(load, schema, model);
      
      WeightVolumeRatio = GetParameterValue(RatesArticles.WeightVolumeRatio);
      MinFreight = GetParameterValue(RatesArticles.MinFreight);

      BillableWeight = Math.Max(_waybill.TotalWeightKg, VolumetricWeight);

      LoadFreightServiceRateByWeightRange(BillableWeight);
      CheckMandatoryServiceRate();

      LoadRateLineDefaultValues();
    }
    
    public void LoadGeneralParameters()
    {
      WeightVolumeRatio = GetParameterValue(RatesArticles.WeightVolumeRatio);
      MinFreight = GetParameterValue(RatesArticles.MinFreight);
      RealWeightAdjustment = GetParameterValue(RatesArticles.RealWeightAdjustment);

    }

    public void LoadBaseProperties(RateLoadDTO load, RatingSchemaDTO schema, RatingModelDTO model)
    {
      _responseDTO = new ResponseDTO();
      _rateLine = new RateLineDTO();
      _schema = schema;
      _model = model;
      LoadWaybill(load);
    }

    public void LoadWaybill(RateLoadDTO load)
    {
      if (load.Shipments == null || !load.Shipments.Any())
      {
        throw new ArgumentException(string.Format(Messages.validation_CannotBeNullOrEmpty, "shipment"));
      }

      _waybill = load.Shipments.FirstOrDefault().Waybills.FirstOrDefault();

      if (_waybill == null)
      {
        throw new ArgumentException(string.Format(Messages.validation_CannotBeNullOrEmpty, "waybill"));
      }
    }

    public void LoadFreightServiceRate()
    {
      using (var ratingEntities = new RatingEntities())
      {
        // Search by Customer, RouteType, CityFrom, CityTo
        _serviceRate = (ratingEntities.trServiceRates.Where(p => p.TrrSchema_RsmCode == Schema.Code &&
                                                                p.TrrRatingModel_RmoCode == Model.Code &&
                                                                p.TrrArticle_ArtCode == RatesArticles.Freight &&
                                                                p.TrrCustomer_UcrCode == _waybill.CustomerUcrCode &&
                                                                p.TrrRouteLegType == _waybill.RouteLegType &&
                                                                p.TrrShipFrom_CitCode == _waybill.ShipFromCityCitCode &&
                                                                p.TrrShipTo_CitCode == _waybill.ShipToCityCitCode &&
                                                                _waybill.Date >= p.TrrDateFrom &&
                                                                _waybill.Date <= p.TrrDateTo &&
                                                                p.TrrActiveState == (short) RatesConstants.State.Active))
          .FirstOrDefault();
      }
    }

    public void LoadFreightServiceRateByWeightRange(double weight)
    {
      using (var ratingEntities = new RatingEntities())
      {
        // search by customer and routeype
        _serviceRate = (ratingEntities.trServiceRates.Where(p => p.TrrSchema_RsmCode == Schema.Code &&
                                                                 p.TrrRatingModel_RmoCode == Model.Code &&
                                                                 p.TrrCustomer_UcrCode == Waybill.CustomerUcrCode &&
                                                                 p.TrrArticle_ArtCode == RatesArticles.Freight &&
                                                                 p.TrrShipFrom_CitCode == Waybill.ShipFromCityCitCode &&
                                                                 p.TrrShipTo_CitCode == Waybill.ShipToCityCitCode &&
                                                                 p.TrrRouteLegType == Waybill.RouteLegType &&
                                                                 weight > p.TrrRangeMin && weight <= p.TrrRangeMax &&
                                                                 Waybill.Date >= p.TrrDateFrom &&
                                                                 Waybill.Date <= p.TrrDateTo &&
                                                                 p.TrrActiveState == (short) RatesConstants.State.Active))
          .FirstOrDefault();

      }
    }
    
    public void LoadRateLineDefaultValues()
    {
      //concepto que me van a cobrar
      _rateLine.ArtCode = string.IsNullOrEmpty(_model.DefaultArticle) ? RatesArticles.DefaultArticle : _model.DefaultArticle;
      //codigo del schema
      RateLineDTO.SchemaCode = Schema.Code;
      //codigo del modelo
      RateLineDTO.ModelCode = Model.Code;
      //Nombre del modelo
      RateLineDTO.ModelName = Model.Name;
      // Código de la tarifa
      RateLineDTO.RateCode = ServiceRate.TrrCode.ToString();
      //id service rate
      RateLineDTO.RateId = ServiceRate.TrrId;
      RateLineDTO.ProductCode = null;
      RateLineDTO.ShipmentNumber = Waybill.Number;
      RateLineDTO.CustomerCode = Waybill.CustomerUcrCode;
    }

    public double GetParameterValue(string articleCode)
    {
      var serviceRate = GetParameter(articleCode);

      if (serviceRate == null || (Math.Abs(serviceRate.TrrValue - 0) < RatesConstants.MinDoubleValue))
      {
        throw new ArgumentException(string.Format(Messages.error_ArticleNotFoundForSchemaModelCustomerRouteLegType, articleCode, Schema.Code, Model.Code, _waybill.CustomerUcrCode, _waybill.RouteLegType));
      }

      return serviceRate.TrrValue;

    }

		public double GetParameterValueByCities(string articleCode)
		{
			var serviceRate = GetParameterByCities(articleCode);

			if (serviceRate == null || (Math.Abs(serviceRate.TrrValue - 0) < RatesConstants.MinDoubleValue))
			{
				throw new ArgumentException(string.Format(Messages.error_ArticleNotFoundForSchemaModelCustomerRouteLegType, articleCode, Schema.Code, Model.Code, _waybill.CustomerUcrCode, _waybill.RouteLegType));
			}

			return serviceRate.TrrValue;

		}

    public trServiceRates GetParameter(string articleCode)
    {
      trServiceRates serviceRate;

      using (var ratingEntities = new RatingEntities())
      {
        // Search by Customer, RouteType, 
        serviceRate = (ratingEntities.trServiceRates.Where(p => p.TrrSchema_RsmCode == Schema.Code &&
                                                                p.TrrRatingModel_RmoCode == Model.Code &&
                                                                p.TrrArticle_ArtCode == articleCode &&
                                                                p.TrrCustomer_UcrCode == _waybill.CustomerUcrCode &&
                                                                p.TrrRouteLegType == _waybill.RouteLegType &&
                                                                _waybill.Date >= p.TrrDateFrom &&
                                                                _waybill.Date <= p.TrrDateTo &&
                                                                p.TrrActiveState == (short)RatesConstants.State.Active
                                                                )).FirstOrDefault();

        if (serviceRate != null)
          return serviceRate;

        // Search by Customer
        serviceRate = (ratingEntities.trServiceRates.Where(p => p.TrrSchema_RsmCode == Schema.Code &&
                                                                p.TrrRatingModel_RmoCode == Model.Code &&
                                                                p.TrrArticle_ArtCode == articleCode &&
                                                                p.TrrCustomer_UcrCode == _waybill.CustomerUcrCode &&
                                                                p.TrrRouteLegType == null &&
                                                                _waybill.Date >= p.TrrDateFrom &&
                                                                _waybill.Date <= p.TrrDateTo &&
                                                                p.TrrActiveState == (short)RatesConstants.State.Active)).FirstOrDefault();

        if (serviceRate != null)
          return serviceRate;

      }

      return serviceRate;

    }

		public trServiceRates GetParameterByCities(string articleCode)
		{
			trServiceRates serviceRate;

			using (var ratingEntities = new RatingEntities())
			{
				// Search by Customer, RouteType, 
				serviceRate = (ratingEntities.trServiceRates.Where(p => p.TrrSchema_RsmCode == Schema.Code &&
																																p.TrrRatingModel_RmoCode == Model.Code &&
																																p.TrrArticle_ArtCode == articleCode &&
																																p.TrrCustomer_UcrCode == _waybill.CustomerUcrCode &&
																																p.TrrRouteLegType == _waybill.RouteLegType &&
																																_waybill.Date >= p.TrrDateFrom &&
																																_waybill.Date <= p.TrrDateTo &&
																																(p.TrrShipFrom_CitCode == _waybill.ShipFromCityCitCode || p.TrrShipFrom_CitCode == null) &&
																																(p.TrrShipTo_CitCode == _waybill.ShipToCityCitCode || p.TrrShipTo_CitCode == null) &&
																																p.TrrActiveState == (short)RatesConstants.State.Active
																																)).OrderByDescending(p => p.TrrSupplier_UcrCode)
																																.ThenByDescending(s => s.TrrShipFrom_CitCode)
																																.ThenByDescending(t => t.TrrShipTo_CitCode).FirstOrDefault();
				if (serviceRate != null)
					return serviceRate;

				// Search by Customer
				serviceRate = (ratingEntities.trServiceRates.Where(p => p.TrrSchema_RsmCode == Schema.Code &&
																																p.TrrRatingModel_RmoCode == Model.Code &&
																																p.TrrArticle_ArtCode == articleCode &&
																																p.TrrCustomer_UcrCode == _waybill.CustomerUcrCode &&
																																p.TrrRouteLegType == null &&
																																_waybill.Date >= p.TrrDateFrom &&
																																_waybill.Date <= p.TrrDateTo &&
																																p.TrrActiveState == (short)RatesConstants.State.Active)).FirstOrDefault();

				if (serviceRate != null)
					return serviceRate;

			}

			return serviceRate;

		}

    public trServiceRates GetServiceRate(string articleCode)
    {
      trServiceRates serviceRate;

      using (var ratingEntities = new RatingEntities())
      {
        // Search by Customer, RouteType, 
        serviceRate = (ratingEntities.trServiceRates.Where(p => p.TrrSchema_RsmCode == Schema.Code &&
                                                                p.TrrRatingModel_RmoCode == Model.Code &&
                                                                p.TrrArticle_ArtCode == articleCode &&
                                                                p.TrrCustomer_UcrCode == _waybill.CustomerUcrCode &&
                                                                p.TrrRouteLegType == _waybill.RouteLegType &&
                                                                _waybill.Date >= p.TrrDateFrom &&
                                                                _waybill.Date <= p.TrrDateTo &&
                                                                p.TrrActiveState == (short)RatesConstants.State.Active)).FirstOrDefault();

        if (serviceRate != null)
          return serviceRate;
      }

      return serviceRate;

    }
    
    public void CheckMandatoryServiceRate()
    {
      if ((_serviceRate == null || Math.Abs(_serviceRate.TrrValue - 0) < RatesConstants.MinDoubleValue) || Model.NonZeroValueRequired == RatesConstants.Mandatory)
      {
        throw new InvalidOperationException(string.Format(Messages.error_MandatoryFreightParcelRateNotFound, Schema.Code, Model.Code, RatesArticles.Freight, _waybill.CustomerUcrCode, _waybill.RouteLegType, _waybill.ShipFromCityCitCode, _waybill.ShipToCityCitCode));
      }
    }
  }
}
