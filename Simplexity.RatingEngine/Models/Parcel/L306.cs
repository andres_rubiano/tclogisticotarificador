﻿using System;
using System.Collections.Generic;
using System.Linq;
using Simplexity.RatingEngine.Constants;
using Simplexity.RatingEngine.DTOs;

namespace Simplexity.RatingEngine.Models.Parcel
{
	/// ESTE MODELO CALCULA EL SEGURO DEL WAYBILL
	public class L306 : ParcelRate, IRating
	{
		public ResponseDTO Calculate(RateLoadDTO load, RatingSchemaDTO schema, RatingModelDTO model, List<RateLineDTO> rateLines)
		{

			#region 1. Load Objects
			LoadBaseProperties(load, schema, model);

			var freight = rateLines.FirstOrDefault(p => p.ArtCode == RatesArticles.Freight);

			if (freight == null)
			{
				throw new InvalidOperationException("cannot calculate FreightDiscount because no freigt article was detected. Please check yor schema configuration");
			}

			ServiceRate = GetServiceRate(RatesArticles.FreightDiscount);

			CheckMandatoryServiceRate();
			LoadRateLineDefaultValues();

			#endregion

			#region 2. Calculate Discount

			RateLineDTO.Quantity = 1;
			RateLineDTO.UnitPrice = freight.ExtendedPrice * ServiceRate.TrrValue;
			RateLineDTO.UomCode = RatesUnitsOfMeasure.Percentage;
			RateLineDTO.Nat = (int)RatesConstants.Nature.Negative;
			RateLineDTO.ExtendedPrice = RateLineDTO.UnitPrice * RateLineDTO.Quantity;

			if (Math.Abs(ServiceRate.TrrValue - 0) > RatesConstants.MinDoubleValue && Math.Abs(RateLineDTO.ExtendedPrice - 0) > RatesConstants.MinDoubleValue)
			{
				ResponseDTO.RateLines.Add(RateLineDTO);
			}

			#endregion

			return ResponseDTO;

		}
	}
}
