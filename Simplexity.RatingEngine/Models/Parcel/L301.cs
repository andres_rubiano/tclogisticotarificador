﻿using System;
using System.Collections.Generic;
using Simplexity.RatingEngine.Constants;
using Simplexity.RatingEngine.DTOs;

namespace Simplexity.RatingEngine.Models.Parcel
{
  // Flete x unidad de peso teniendo en cuenta peso mínimo x unidad
  public class L301 : ParcelRate, IRating
  {
    public ResponseDTO Calculate(RateLoadDTO load, RatingSchemaDTO schema, RatingModelDTO model, List<RateLineDTO> rateLines)
    {
      #region 1. Load objects

      Load(load, schema, model);

      #endregion

      #region 2. Load specific parameters

      var minWeightByUnit = GetParameterValue(RatesArticles.MinWeightByUnit);
      var minWeightByDispatch = GetParameterValue(RatesArticles.MinWeightByDispatch);

      #endregion

      #region 3. Calculate WeightToBill
      if (Waybill.TotalWeightKg < RealWeightAdjustment)
      {
        var maxWeight = Math.Max(Waybill.TotalWeightKg, VolumetricWeight);
        var minWeight = Math.Max(minWeightByUnit * Waybill.Quantity, minWeightByDispatch);

        BillableWeight = minWeight < maxWeight ? maxWeight : minWeight;
      }
      else
      {
        BillableWeight = Waybill.TotalWeightKg;
      }

      #endregion

      #region 4. Calculate Freight

      if ((BillableWeight * ServiceRate.TrrValue) < MinFreight)
      {
        RateLineDTO.Quantity = 1;
        RateLineDTO.UnitPrice = MinFreight;
        RateLineDTO.UomCode = RatesUnitsOfMeasure.MinFreight;
      }
      else
      {
        RateLineDTO.Quantity = BillableWeight;
        RateLineDTO.UnitPrice = ServiceRate.TrrValue;
        RateLineDTO.UomCode = RatesUnitsOfMeasure.Kilogram;
      }

      RateLineDTO.Nat = (int)RatesConstants.Nature.Positive;
      RateLineDTO.ExtendedPrice = RateLineDTO.Quantity * RateLineDTO.UnitPrice;
      
      if (Math.Abs(ServiceRate.TrrValue - 0) > RatesConstants.MinDoubleValue && Math.Abs(RateLineDTO.ExtendedPrice - 0) > RatesConstants.MinDoubleValue)
      {
        ResponseDTO.RateLines.Add(RateLineDTO);
      }

      #endregion

      return ResponseDTO;
    }
  }
}