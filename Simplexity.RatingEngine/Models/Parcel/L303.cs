﻿using System;
using System.Collections.Generic;
using Simplexity.RatingEngine.Constants;
using Simplexity.RatingEngine.DTOs;

namespace Simplexity.RatingEngine.Models.Parcel
{
  // Flete x unidad de medida + valor x peso máximo unidad
  public class L303 : ParcelRate,  IRating
  {
    public ResponseDTO Calculate(RateLoadDTO load, RatingSchemaDTO schema, RatingModelDTO model, List<RateLineDTO> rateLines)
    {

      #region 1. Load Objects

      Load(load, schema, model);

      #endregion

      #region 2. Load specific parameters
      var maxUnitWeight = GetParameterValue(RatesArticles.MaxUnitWeight);
      var maxWeight = (Math.Max(Waybill.TotalWeightKg, VolumetricWeight)) / Waybill.Quantity ;
      
      double totalFreightAdditionalWeight; 

      if (maxWeight <  maxUnitWeight)
      {
        totalFreightAdditionalWeight = 0;
      }
      else
      {
        var freightAdditionalWeight = GetParameterValue(RatesArticles.FreightAdditionalWeight);
	      totalFreightAdditionalWeight = ((maxWeight - maxUnitWeight) * freightAdditionalWeight) * Waybill.Quantity;
      }

      #endregion

      #region 5. Calculate Freight

      if (((ServiceRate.TrrValue*Waybill.Quantity)+totalFreightAdditionalWeight)< MinFreight)
      {
        RateLineDTO.Quantity = 1;
        RateLineDTO.UnitPrice = MinFreight;
        RateLineDTO.UomCode = RatesUnitsOfMeasure.MinFreight;
      }
	    else
	    {
        RateLineDTO.Quantity = Waybill.Quantity;
        RateLineDTO.UnitPrice = ServiceRate.TrrValue;
        RateLineDTO.UomCode = RatesUnitsOfMeasure.Box;
	    }

      RateLineDTO.Nat = (int)RatesConstants.Nature.Positive;
      RateLineDTO.ExtendedPrice = (RateLineDTO.Quantity * RateLineDTO.UnitPrice) + totalFreightAdditionalWeight;

      if (Math.Abs(ServiceRate.TrrValue - 0) > RatesConstants.MinDoubleValue && Math.Abs(RateLineDTO.ExtendedPrice - 0) > RatesConstants.MinDoubleValue)
      {
        ResponseDTO.RateLines.Add(RateLineDTO);
      }

      #endregion

      return ResponseDTO;
    }
  }
}