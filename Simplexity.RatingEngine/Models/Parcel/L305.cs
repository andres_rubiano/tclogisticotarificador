﻿using System;
using System.Collections.Generic;
using Simplexity.RatingEngine.Constants;
using Simplexity.RatingEngine.DTOs;

namespace Simplexity.RatingEngine.Models.Parcel
{
  /// 5. Flete x unidad de peso teniendo en cuenta unidad de peso inicial  y valor unidad de peso adicional
  public class L305 : ParcelRate, IRating
  {
    private ResponseDTO _responseDTO;

    public ResponseDTO Calculate(RateLoadDTO load, RatingSchemaDTO schema, RatingModelDTO model, List<RateLineDTO> rateLines)
    {
      #region 1. Load Objects

      Load(load, schema, model);

      #endregion

      #region 2. Load specific parameters

      var minWeightByUnit = GetParameterValue(RatesArticles.MinWeightByUnit);
      var freightAdditionalWeight = GetParameterValueByCities(RatesArticles.FreightAdditionalWeight);
	    var initWeight = GetParameterValueByCities(RatesArticles.InitWeight);
      #endregion

      #region 3. Calculate WeightToBill

      if (Waybill.TotalWeightKg < RealWeightAdjustment)
      {

        var maxWeight = Math.Max(Waybill.TotalWeightKg, VolumetricWeight);
        var minWeight = minWeightByUnit*Waybill.Quantity;
        
        BillableWeight = minWeight < maxWeight ? maxWeight : minWeight;
      }
      else
      {
        BillableWeight = Waybill.TotalWeightKg;
      }

      #endregion

      #region 4. Calculate Freight


			if ((ServiceRate.TrrValue + ((BillableWeight - initWeight) * freightAdditionalWeight)) < MinFreight)
      {
        RateLineDTO.Quantity = 1;
        RateLineDTO.UnitPrice = MinFreight;
        RateLineDTO.UomCode = RatesUnitsOfMeasure.MinFreight;
        RateLineDTO.ExtendedPrice = RateLineDTO.Quantity * RateLineDTO.UnitPrice;
      }
      else
      {
				RateLineDTO.Quantity = BillableWeight - initWeight;
        RateLineDTO.UnitPrice = 1;
        RateLineDTO.UomCode = RatesUnitsOfMeasure.KilogramAdd;
				RateLineDTO.ExtendedPrice = ServiceRate.TrrValue + ((BillableWeight - initWeight) * freightAdditionalWeight);

      }

      RateLineDTO.Nat = (int)RatesConstants.Nature.Positive;

      if (Math.Abs(ServiceRate.TrrValue - 0) > RatesConstants.MinDoubleValue && Math.Abs(RateLineDTO.ExtendedPrice - 0) > RatesConstants.MinDoubleValue)
      {
        ResponseDTO.RateLines.Add(RateLineDTO);
      }

      #endregion

      return ResponseDTO;
    }
  }
}