﻿using System;
using System.Collections.Generic;
using Simplexity.RatingEngine.Constants;
using Simplexity.RatingEngine.DTOs;

namespace Simplexity.RatingEngine.Models.Parcel
{
  /// Flete x unidad de medida teniendo en cuenta rango de pesos
  public class L307 : ParcelRate, IRating
  {

    public ResponseDTO Calculate(RateLoadDTO load, RatingSchemaDTO schema, RatingModelDTO model, List<RateLineDTO> rateLines)
    {
      #region 1. Load Objects

      LoadByWeightRange(load, schema, model);
      
      #endregion

      #region 2. Calculate Freight

      if ((ServiceRate.TrrValue * Waybill.Quantity) < MinFreight)
      {
        RateLineDTO.Quantity = Waybill.Quantity;
        RateLineDTO.UnitPrice = MinFreight;
        RateLineDTO.UomCode = RatesUnitsOfMeasure.MinFreight;
      }
      else
      {
        RateLineDTO.Quantity = Waybill.Quantity;
        RateLineDTO.UnitPrice = ServiceRate.TrrValue;
        RateLineDTO.UomCode = ServiceRate.TrrRange_UOMCode;
      }

      #endregion

      #region 5. Definimos el RateLine

      RateLineDTO.Nat = (int)RatesConstants.Nature.Positive;
      RateLineDTO.ExtendedPrice = RateLineDTO.Quantity * RateLineDTO.UnitPrice;

      if (Math.Abs(ServiceRate.TrrValue - 0) > RatesConstants.MinDoubleValue && Math.Abs(RateLineDTO.ExtendedPrice - 0) > RatesConstants.MinDoubleValue)
      {
        ResponseDTO.RateLines.Add(RateLineDTO);
      }

      #endregion

      return ResponseDTO;
    }
  }
}
