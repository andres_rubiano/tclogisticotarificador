﻿using System;
using System.Collections.Generic;
using Simplexity.RatingEngine.Constants;
using Simplexity.RatingEngine.DTOs;

namespace Simplexity.RatingEngine.Models.Parcel
{
	/// ESTE MODELO CALCULA EL SEGURO DEL WAYBILL
	public class L309 : ParcelRate, IRating
	{
		public ResponseDTO Calculate(RateLoadDTO load, RatingSchemaDTO schema, RatingModelDTO model, List<RateLineDTO> rateLines)
		{

			#region 1. Load Objects

			LoadBaseProperties(load, schema, model);
			ServiceRate = GetServiceRate(RatesArticles.MHandCostbyDispatch);
			CheckMandatoryServiceRate();
			LoadRateLineDefaultValues();

			#endregion

			#region 2. Load specific parameters

			var minHandlingCostByDispatch = GetParameterValue(RatesArticles.MinHandlingCostByDispatch);

			#endregion

			#region 3. Calculate Insurance

			if ((Waybill.CargoDeclaredValue * ServiceRate.TrrValue) < minHandlingCostByDispatch)
			{
				RateLineDTO.ExtendedPrice = RateLineDTO.UnitPrice = minHandlingCostByDispatch;
			}
			else
			{
				RateLineDTO.UnitPrice = Waybill.CargoDeclaredValue * ServiceRate.TrrValue;
				RateLineDTO.ExtendedPrice = RateLineDTO.UnitPrice;
			}

			if (Math.Abs(ServiceRate.TrrValue - 0) > RatesConstants.MinDoubleValue && Math.Abs(RateLineDTO.ExtendedPrice - 0) > RatesConstants.MinDoubleValue)
			{
				ResponseDTO.RateLines.Add(RateLineDTO);
			}
			RateLineDTO.UomCode = RatesUnitsOfMeasure.Percentage;
			RateLineDTO.Quantity = 1;
			#endregion

			return ResponseDTO;

		}
	}
}
