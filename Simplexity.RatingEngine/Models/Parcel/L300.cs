﻿using System;
using System.Collections.Generic;
using Simplexity.RatingEngine.Constants;
using Simplexity.RatingEngine.DTOs;

namespace Simplexity.RatingEngine.Models.Parcel
{
  /// ESTE MODELO CALCULA EL SEGURO DEL WAYBILL
  public class L300 : ParcelRate, IRating
  {
    public ResponseDTO Calculate(RateLoadDTO load, RatingSchemaDTO schema, RatingModelDTO model, List<RateLineDTO> rateLines)
    {

      #region 1. Load Objects

      LoadBaseProperties(load, schema, model);
      ServiceRate = GetServiceRate(RatesArticles.Insurance);
      CheckMandatoryServiceRate();
      LoadRateLineDefaultValues();

      #endregion

      #region 2. Load specific parameters

      var minHandlingCostByUnit = GetParameterValue(RatesArticles.HandlingCostOverDeclaredValue);

      #endregion

      #region 3. Calculate Insurance

      if (((Waybill.CargoDeclaredValue * ServiceRate.TrrValue) / Waybill.Quantity) < minHandlingCostByUnit)
      {
        RateLineDTO.UnitPrice = minHandlingCostByUnit;
				RateLineDTO.Quantity = Waybill.Quantity;
      }
      else
      {
        RateLineDTO.UnitPrice = Waybill.CargoDeclaredValue * ServiceRate.TrrValue;
	      RateLineDTO.Quantity = 1;
      }

      RateLineDTO.UomCode = RatesUnitsOfMeasure.Percentage;
      RateLineDTO.Nat = (int)RatesConstants.Nature.Negative;
      RateLineDTO.ExtendedPrice = RateLineDTO.UnitPrice * RateLineDTO.Quantity;
        
      if (Math.Abs(ServiceRate.TrrValue - 0) > RatesConstants.MinDoubleValue && Math.Abs(RateLineDTO.ExtendedPrice - 0) > RatesConstants.MinDoubleValue)
        {
          ResponseDTO.RateLines.Add(RateLineDTO);
        }

      #endregion

      return ResponseDTO;

    }
  }
}
