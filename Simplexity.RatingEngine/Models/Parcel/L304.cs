﻿using System;
using System.Collections.Generic;
using Simplexity.RatingEngine.Constants;
using Simplexity.RatingEngine.DTOs;

namespace Simplexity.RatingEngine.Models.Parcel
{
  /// 4. Flete x peso total teniendo en cuenta rango de pesos
  public class L304 : ParcelRate, IRating
  {

    public ResponseDTO Calculate(RateLoadDTO load, RatingSchemaDTO schema, RatingModelDTO model, List<RateLineDTO> rateLines)
    {
      #region 1. Load Objects

      LoadByWeightRange(load, schema, model);

      #endregion

      #region 2. Calculate Freight

      if (ServiceRate.TrrValue < MinFreight)
      {
        RateLineDTO.Quantity = 1;
        RateLineDTO.UnitPrice = MinFreight;
        RateLineDTO.UomCode = RatesUnitsOfMeasure.MinFreight;
        RateLineDTO.ExtendedPrice = RateLineDTO.Quantity * RateLineDTO.UnitPrice;
      }
      else
      {
        RateLineDTO.Quantity = Waybill.Quantity;
        RateLineDTO.UnitPrice = ServiceRate.TrrValue;
        RateLineDTO.UomCode = ServiceRate.TrrRange_UOMCode;
        RateLineDTO.ExtendedPrice = RateLineDTO.UnitPrice;
      }

      RateLineDTO.Nat = (int)RatesConstants.Nature.Positive;
      

      if (Math.Abs(ServiceRate.TrrValue - 0) > RatesConstants.MinDoubleValue && Math.Abs(RateLineDTO.ExtendedPrice - 0) > RatesConstants.MinDoubleValue)
      {
        ResponseDTO.RateLines.Add(RateLineDTO);
      }

      #endregion

      return ResponseDTO;
    }
  }
}