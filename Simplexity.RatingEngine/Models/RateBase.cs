﻿using System.Linq;
using Simplexity.RatingEngine.EF;

namespace Simplexity.RatingEngine.Models
{
  public class RateBase
  {
    public trServiceRates GetServiceRate(string schemaCode,  string modelCode, string batchNumber)
    {

      trServiceRates serviceRate;

      using (var ratingEntities = new RatingEntities())
      {

        serviceRate = (from p in ratingEntities.trServiceRates
                       where p.TrrSchema_RsmCode == schemaCode &&
                             p.TrrRatingModel_RmoCode == modelCode &&
                             p.TrrBatchNumber == batchNumber
                       select p).FirstOrDefault();

      }

      return serviceRate;
    }

  }
}