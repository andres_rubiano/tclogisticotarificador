﻿using System.Collections.Generic;
using Simplexity.RatingEngine.DTOs;

namespace Simplexity.RatingEngine.Models
{
    public interface IRating
    {
      ResponseDTO Calculate(RateLoadDTO load, RatingSchemaDTO schema, RatingModelDTO model, List<RateLineDTO> rateLines);
    }
}
