﻿using System;
using System.Collections.Generic;
using System.Linq;
using Simplexity.RatingEngine.Constants;
using Simplexity.RatingEngine.DTOs;
using Simplexity.RatingEngine.Resources;

namespace Simplexity.RatingEngine.Models.Suppla
{
    public class T314 : OperationRate, IRating
    {
        public ResponseDTO Calculate(RateLoadDTO load, RatingSchemaDTO schema, RatingModelDTO model, List<RateLineDTO> rateLines)
        {
            #region 1. Load Objects

            LoadBaseProperties(schema, model);
            SetLoad(load);
            LoadShipment(load);

            #endregion

            #region 2. Find rate and parameters, run formula

            if (Shipment == null) return ResponseDTO;
            LoadRateSearchModelD(RatesArticles.Freight);

            // Formula:
            var billiableQuantity = load.Shipments.Sum(x=>x.Quantity);
            double unitPrice = ServiceRate != null ? ServiceRate.TrrValue : 0;
            double extendedPrice = billiableQuantity * unitPrice;

            #endregion

            #region 3. Validate Freight

            if (model.NonZeroValueRequired == RatesConstants.Mandatory)
            {
                if (Math.Abs(extendedPrice) > 0)
                {
                    LoadRateLineDefaultValues(load);
                    RateLineDTO.Quantity = billiableQuantity;
                    RateLineDTO.UnitPrice = unitPrice;
                    RateLineDTO.ExtendedPrice = extendedPrice;
                    RateLineDTO.UomCode = RatesUnitsOfMeasure.Dispatch;
                    ResponseDTO.RateLines.Add(RateLineDTO);
                }
                else
                {
                    ResponseDTO.Message.TypeEnum = MessagesConstants.Types.Error;
                    ResponseDTO.Message.Message =
                        string.Join("|", string.Format(Messages.error_MandatoryRateNotFoundVehType, schema.Name, model.Name,
                                                RatesArticles.Loading, load.VtyCode, Shipment.CustomerUcrCode, Shipment.ShipFromCitCode, Shipment.ShipToCitCode));
                }
            }
            else
            {
                if (Math.Abs(extendedPrice) > 0)
                {
                    LoadRateLineDefaultValues(load);
                    RateLineDTO.Quantity = billiableQuantity;
                    RateLineDTO.UnitPrice = unitPrice;
                    RateLineDTO.ExtendedPrice = extendedPrice;
                    RateLineDTO.UomCode = RatesUnitsOfMeasure.Unit;
                    ResponseDTO.RateLines.Add(RateLineDTO);
                }
            }

            #endregion

            return ResponseDTO;
        }
    }
}
