﻿using System;
using System.Collections.Generic;
using System.Linq;
using Simplexity.RatingEngine.Constants;
using Simplexity.RatingEngine.DTOs;
using Simplexity.RatingEngine.EF;
using Simplexity.RatingEngine.Resources;

namespace Simplexity.RatingEngine.Models.Suppla
{
    public class T317 : OperationRate, IRating
    {
        public ResponseDTO Calculate(RateLoadDTO load, RatingSchemaDTO schema, RatingModelDTO model, List<RateLineDTO> rateLines)
        {
            Utilities.ValidateUpdatingRates();

            #region 1. Load Objects

            LoadBaseProperties(schema, model);
            SetLoad(load);
            LoadShipment(load);

            #endregion

            #region 2. Find rate and parameters, run formula

            if (Shipment == null) return ResponseDTO;

            double costoTotal = 0.0;
            foreach (var shipmentDto in load.Shipments)
            {
                if (shipmentDto.ShtCode != "TREMTRA" && shipmentDto.Waybills.Count > 0)
                {
                    foreach (var rateWaybillDto in shipmentDto.Waybills)
                    {
                        var porcentajeManejoValorDeclarado = ObtenerParametro(RatesArticles.CostHandlingDecValue, model, schema, rateWaybillDto, load.CarrierUcrCode);
                        var costoManejoPorCaja = ObtenerParametro(RatesArticles.CostoManjeCaj, model, schema, rateWaybillDto, load.CarrierUcrCode);
                        var costoManejoPorValorDeclarado = porcentajeManejoValorDeclarado * rateWaybillDto.CargoDeclaredValue;
                        var costoManejoTotalPorCajas = rateWaybillDto.Quantity * costoManejoPorCaja;
                        var costo = Math.Max(costoManejoPorValorDeclarado, costoManejoTotalPorCajas);
                        if ((Math.Abs(costo - 0) <= RatesConstants.MinDoubleValue))
                        {
                            throw new ArgumentException(string.Format(Messages.error_ArticleNotFoundForSchemaModelRouteLegType, RatesArticles.CostoManjeCaj, schema.Code, model.Code, load.CarrierUcrCode, rateWaybillDto.RouteLegType));
                        }
                        costoTotal += costo;
                    }
                }
                else
                {
                    var porcentajeManejoValorDeclarado = ObtenerParametro(RatesArticles.CostHandlingDecValue, model, schema, shipmentDto, load.CarrierUcrCode);
                    var costoManejoPorCaja = ObtenerParametro(RatesArticles.CostoManjeCaj, model, schema, shipmentDto, load.CarrierUcrCode);
                    var costoManejoPorValorDeclarado = porcentajeManejoValorDeclarado * shipmentDto.TotalDelaredValue;
                    var costoManejoTotalPorCajas = shipmentDto.Quantity * costoManejoPorCaja;
                    var costo = Math.Max(costoManejoPorValorDeclarado, costoManejoTotalPorCajas);
                    if ((Math.Abs(costo - 0) <= RatesConstants.MinDoubleValue))
                    {
                        throw new ArgumentException(string.Format(Messages.error_ArticleNotFoundForSchemaModelRouteLegType, RatesArticles.CostoManjeCaj, schema.Code, model.Code, load.CarrierUcrCode, shipmentDto.RouteLegType));
                    }
                    costoTotal += costo;
                }
            }

            #endregion

            if (model.NonZeroValueRequired == RatesConstants.Mandatory)
            {
                if (Math.Abs(costoTotal) > 0)
                {
                    RateLineDTO.ArtCode = RatesArticles.CostoManjeCaj;
                    //codigo del schema
                    RateLineDTO.SchemaCode = schema.Code;
                    //codigo del modelo
                    RateLineDTO.ModelCode = model.Code;
                    //Nombre del modelo
                    RateLineDTO.ModelName = model.Name;
                    RateLineDTO.ProductCode = null;
                    RateLineDTO.ShipmentNumber = Shipment.ShiNumber;
                    RateLineDTO.LoaNumber = Shipment.ShiLoadLoaNumber;
                    RateLineDTO.CustomerCode = load.Nat == (short)RatesConstants.Nature.Positive
                      ? Shipment.CustomerUcrCode
                      : load.AdministratorUcrCode;

                    RateLineDTO.Quantity = 1;
                    RateLineDTO.ExtendedPrice = costoTotal;
                    RateLineDTO.UnitPrice = costoTotal;
                    RateLineDTO.UomCode = RatesUnitsOfMeasure.Viaje;
                    ResponseDTO.RateLines.Add(RateLineDTO);
                }
                else
                {
                    ResponseDTO.Message.TypeEnum = MessagesConstants.Types.Error;
                    ResponseDTO.Message.Message = string.Join("|", "No se calculo tarifa.");
                }
            }
            else
            {
                if (Math.Abs(costoTotal) > 0)
                {
                    RateLineDTO.ArtCode = RatesArticles.CostoManjeCaj;
                    //codigo del schema
                    RateLineDTO.SchemaCode = schema.Code;
                    //codigo del modelo
                    RateLineDTO.ModelCode = model.Code;
                    //Nombre del modelo
                    RateLineDTO.ModelName = model.Name;
                    RateLineDTO.ProductCode = null;
                    RateLineDTO.ShipmentNumber = Shipment.ShiNumber;
                    RateLineDTO.LoaNumber = Shipment.ShiLoadLoaNumber;
                    RateLineDTO.CustomerCode = load.Nat == (short)RatesConstants.Nature.Positive
                      ? Shipment.CustomerUcrCode
                      : load.AdministratorUcrCode;

                    RateLineDTO.Quantity = 1;
                    RateLineDTO.ExtendedPrice = costoTotal;
                    RateLineDTO.UnitPrice = costoTotal;
                    RateLineDTO.UomCode = RatesUnitsOfMeasure.Viaje;
                    ResponseDTO.RateLines.Add(RateLineDTO);
                }
            }
            return ResponseDTO;
        }

        #region private

        /// <summary>
        /// Busca una tarifa de coincidencia exacta para este artículo por esquema, modelo, tipo de ruta; opcional por proveedor. Debe estar vigente y activa. para la remesa.
        /// </summary>
        /// <param name="articleCode"></param>
        /// <param name="model"></param>
        /// <param name="schema"></param>
        /// <param name="rateShipmentDto"></param>
        /// <param name="carrierUcrCode"></param>
        /// <returns>Valor de la tarifa</returns>
        private float ObtenerParametro(string articleCode, RatingModelDTO model, RatingSchemaDTO schema, RateShipmentDTO rateShipmentDto, string carrierUcrCode)
        {
            #region Conexion conEF

            //trServiceRates serviceRate;
            //var routeLegType = rateShipmentDto.RouteLegType;
            //using (var ratingEntities = new RatingEntities())
            //{
            //    serviceRate = (ratingEntities.trServiceRates.Where(p => p.TrrSchema_RsmCode == schema.Code &&
            //                                                            p.TrrRatingModel_RmoCode == model.Code &&
            //                                                            p.TrrArticle_ArtCode == articleCode &&
            //                                                            p.TrrDateFrom <= rateShipmentDto.CreationDate &&
            //                                                            p.TrrDateTo >= rateShipmentDto.CreationDate &&
            //                                                            p.TrrActiveState ==
            //                                                            (short)RatesConstants.State.Active &&
            //                                                            p.TrrRouteLegType == routeLegType &&
            //                                                            (p.TrrSupplier_UcrCode == carrierUcrCode ||
            //                                                             p.TrrSupplier_UcrCode == null)
            //      )).FirstOrDefault();
            //}

            //if (serviceRate == null || (Math.Abs(serviceRate.TrrValue - 0) < RatesConstants.MinDoubleValue))
            //{
            //    throw new ArgumentException(string.Format(Messages.error_ArticleNotFoundForSchemaModelRouteLegType, articleCode, schema.Code, model.Code, carrierUcrCode, routeLegType));
            //}

            //return (float)(serviceRate != null ? serviceRate.TrrValue : 0);

            #endregion

            #region Tarifario en memoria

            var routeLegType = rateShipmentDto.RouteLegType;

            var serviceRate = ServiceRates.RateList.Where(p => p.TrrSchema_RsmCode == schema.Code &&
                                                               p.TrrRatingModel_RmoCode == model.Code &&
                                                               p.TrrArticle_ArtCode == articleCode &&
                                                               p.TrrDateFrom <= rateShipmentDto.CreationDate &&
                                                               p.TrrDateTo >= rateShipmentDto.CreationDate &&
                                                               p.TrrActiveState ==
                                                               (short) RatesConstants.State.Active &&
                                                               p.TrrRouteLegType == routeLegType &&
                                                               (p.TrrSupplier_UcrCode == carrierUcrCode ||
                                                                p.TrrSupplier_UcrCode == null)
                ).OrderByDescending(o => o.TrrSupplier_UcrCode).FirstOrDefault();

            if (serviceRate == null || (Math.Abs(serviceRate.TrrValue - 0) < RatesConstants.MinDoubleValue))
            {
                throw new ArgumentException(string.Format(Messages.error_ArticleNotFoundForSchemaModelRouteLegType,
                    articleCode, schema.Code, model.Code, carrierUcrCode, routeLegType));
            }

            return serviceRate.TrrValue;

            #endregion

        }

        /// <summary>
        /// Busca una tarifa de coincidencia exacta para este artículo por esquema, modelo, tipo de ruta; opcional por proveedor. Debe estar vigente y activa. para la guia.
        /// </summary>
        /// <param name="articleCode"></param>
        /// <param name="model"></param>
        /// <param name="schema"></param>
        /// <param name="rateWaybillDto"></param>
        /// <param name="carrierUcrCode"></param>
        /// <returns>Valor de la tarifa</returns>
        private float ObtenerParametro(string articleCode, RatingModelDTO model, RatingSchemaDTO schema, RateWaybillDTO rateWaybillDto, string carrierUcrCode)
        {
            #region Conexion conEF

            //trServiceRates serviceRate;
            //var routeLegType = rateWaybillDto.RouteLegType;
            //using (var ratingEntities = new RatingEntities())
            //{
            //    serviceRate = (ratingEntities.trServiceRates.Where(p => p.TrrSchema_RsmCode == schema.Code &&
            //                                                            p.TrrRatingModel_RmoCode == model.Code &&
            //                                                            p.TrrArticle_ArtCode == articleCode &&
            //                                                            p.TrrDateFrom <= rateWaybillDto.Date &&
            //                                                            p.TrrDateTo >= rateWaybillDto.Date &&
            //                                                            p.TrrActiveState ==
            //                                                            (short)RatesConstants.State.Active &&
            //                                                            p.TrrRouteLegType == routeLegType &&
            //                                                            (p.TrrSupplier_UcrCode == carrierUcrCode ||
            //                                                             p.TrrSupplier_UcrCode == null)
            //      )).FirstOrDefault();
            //}

            //if (serviceRate == null || (Math.Abs(serviceRate.TrrValue - 0) < RatesConstants.MinDoubleValue))
            //{
            //    throw new ArgumentException(string.Format(Messages.error_ArticleNotFoundForSchemaModelRouteLegType, articleCode, schema.Code, model.Code, carrierUcrCode, routeLegType));
            //}

            //return (float)(serviceRate != null ? serviceRate.TrrValue : 0);

            #endregion

            #region Tarifario en memoria

            var routeLegType = rateWaybillDto.RouteLegType;

            var serviceRate = ServiceRates.RateList.Where(p => p.TrrSchema_RsmCode == schema.Code &&
                                                               p.TrrRatingModel_RmoCode == model.Code &&
                                                               p.TrrArticle_ArtCode == articleCode &&
                                                               p.TrrDateFrom <= rateWaybillDto.Date &&
                                                               p.TrrDateTo >= rateWaybillDto.Date &&
                                                               p.TrrActiveState ==
                                                               (short) RatesConstants.State.Active &&
                                                               p.TrrRouteLegType == routeLegType &&
                                                               (p.TrrSupplier_UcrCode == carrierUcrCode ||
                                                                p.TrrSupplier_UcrCode == null)
                ).OrderByDescending(o => o.TrrSupplier_UcrCode).FirstOrDefault();

            if (serviceRate == null || (Math.Abs(serviceRate.TrrValue - 0) < RatesConstants.MinDoubleValue))
            {
                throw new ArgumentException(string.Format(Messages.error_ArticleNotFoundForSchemaModelRouteLegType,
                    articleCode, schema.Code, model.Code, carrierUcrCode, routeLegType));
            }

            return serviceRate.TrrValue;

            #endregion

        }

            #endregion
    }
}