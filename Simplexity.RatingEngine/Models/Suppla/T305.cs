﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simplexity.RatingEngine.Constants;
using Simplexity.RatingEngine.DTOs;
using Simplexity.RatingEngine.EF;

namespace Simplexity.RatingEngine.Models.Suppla
{
	// Flete x unidad de medida + valor x peso máximo unidad
	public class T305 : OperationRate, IRating
	{
		public ResponseDTO Calculate(RateLoadDTO load, RatingSchemaDTO schema, RatingModelDTO model, List<RateLineDTO> rateLines)
		{

			#region 1. Load Objects

			LoadBaseProperties(schema, model);
			SetLoad(load);
			LoadShipment(load);
			LoadGeneralParameters();

			#endregion

			#region 1.5 validations

			ValidateShipmentDetail();

			#endregion

			#region 2. Load specific parameters
			var maxUnitWeight = GetParameter(RatesArticles.MaxUnitWeight);
			var maxWeight = Math.Max(Shipment.ShiTotalWeightKg, VolumetricWeight);

			double totalFreightAdditionalWeight;

			if (maxWeight < maxUnitWeight)
			{
				totalFreightAdditionalWeight = 0;
			}
			else
			{
				var freightAdditionalWeight = GetParameter(RatesArticles.FreightAdditionalWeight);
				totalFreightAdditionalWeight = ((maxWeight - maxUnitWeight) * freightAdditionalWeight) * Shipment.Quantity;
			}

			#endregion

			#region 2. Find rate and parameters, run formula

			if (Shipment == null) return ResponseDTO;
			LoadRateSearchModelA(RatesArticles.Freight);

			if (ServiceRate == null)
			{
				ResponseDTO.Message.TypeEnum = MessagesConstants.Types.Error;
				ResponseDTO.Message.Message =
					string.Join("|", string.Format("No se encontro tarifa para el articulo ({0}), esquema ({1}), modelo ({2})",
											RatesArticles.Freight, schema.Code, model.Code));
				return ResponseDTO;
			}

			#endregion

			#region 5. Calculate Freight

			if (((ServiceRate.TrrValue * Shipment.Quantity) + totalFreightAdditionalWeight) < MinFreight)
			{
				LoadRateLineDefaultValues(load);
				RateLineDTO.Quantity = 1;
				RateLineDTO.UnitPrice = MinFreight;
				RateLineDTO.UomCode = RatesUnitsOfMeasure.MinFreight;
			}
			else
			{
				LoadRateLineDefaultValues(load);
				RateLineDTO.Quantity = Shipment.Quantity;
				RateLineDTO.UnitPrice = ServiceRate.TrrValue;
				RateLineDTO.UomCode = RatesUnitsOfMeasure.Box;
			}

			RateLineDTO.Nat = (int)RatesConstants.Nature.Positive;
			RateLineDTO.ExtendedPrice = (RateLineDTO.Quantity * RateLineDTO.UnitPrice) + totalFreightAdditionalWeight;

			if (Math.Abs(ServiceRate.TrrValue - 0) > RatesConstants.MinDoubleValue && Math.Abs(RateLineDTO.ExtendedPrice - 0) > RatesConstants.MinDoubleValue)
			{
				ResponseDTO.RateLines.Add(RateLineDTO);
			}

			#endregion

			return ResponseDTO;
		}
	}
}
