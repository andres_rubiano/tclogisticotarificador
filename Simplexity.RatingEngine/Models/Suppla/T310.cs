﻿using System;
using System.Collections.Generic;
using System.Linq;
using Simplexity.RatingEngine.Constants;
using Simplexity.RatingEngine.DTOs;
using Simplexity.RatingEngine.EF;
using Simplexity.RatingEngine.Resources;

namespace Simplexity.RatingEngine.Models.Suppla
{
	/// <summary>
	/// Aplica: Cliente/proveedor
	/// Modelo: Escolta nacional por ruta: La tarifa establece un valor de flete teniendo en cuenta un trayecto (origen, destino)  
	/// y el numero de escoltas asignados al viaje.
	/// El calculo esta dado por 1 despacho y se debe enviar un load con un shipment.
	/// </summary>
	public class T310 : OperationRate, IRating
	{
		public ResponseDTO Calculate(RateLoadDTO load, RatingSchemaDTO schema, RatingModelDTO model, List<RateLineDTO> rateLines)
		{
			#region 1. Load Objects

			LoadBaseProperties(schema, model);
			SetLoad(load);
			LoadShipment(load);
			#endregion

			#region 2. Find rate and parameters, run formula

			if (Shipment == null) return ResponseDTO;
			LoadRateSearchModelA(RatesArticles.SecurityService);


			var billiableQuantity = load.SecurityServiceAmount ?? 0;
			var unitPrice = ServiceRate != null ? ServiceRate.TrrValue : 0;
			var extendedPrice = billiableQuantity * unitPrice;

			#endregion

			#region 3. Validate Freight

			if (model.NonZeroValueRequired == RatesConstants.Mandatory)
			{
				if (Math.Abs(extendedPrice) > 0)
				{
					LoadRateLineDefaultValues(load);
					RateLineDTO.Quantity = billiableQuantity;
					RateLineDTO.UnitPrice = unitPrice;
					RateLineDTO.ExtendedPrice = extendedPrice;
					RateLineDTO.UomCode = RatesUnitsOfMeasure.Unit;
					ResponseDTO.RateLines.Add(RateLineDTO);
				}
				else
				{
					ResponseDTO.Message.TypeEnum = MessagesConstants.Types.Error;
					ResponseDTO.Message.Message =
						string.Join("|", string.Format(Messages.error_MandatoryRateNotFound, schema.Name, model.Name, RatesArticles.Loading,
												Shipment.CustomerUcrCode, Shipment.ShipFromCitCode, Shipment.ShipToCitCode));
				}
			}
			else
			{
				if (Math.Abs(extendedPrice) > 0)
				{
					LoadRateLineDefaultValues(load);
					RateLineDTO.Quantity = billiableQuantity;
					RateLineDTO.UnitPrice = unitPrice;
					RateLineDTO.ExtendedPrice = extendedPrice;
					RateLineDTO.UomCode = RatesUnitsOfMeasure.Unit;
					ResponseDTO.RateLines.Add(RateLineDTO);
				}
			}

			#endregion

			return ResponseDTO;
		}
	}
}