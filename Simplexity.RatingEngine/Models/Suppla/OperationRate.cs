﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Simplexity.RatingEngine.Constants;
using Simplexity.RatingEngine.DTOs;
using Simplexity.RatingEngine.EF;
using Simplexity.RatingEngine.Resources;

namespace Simplexity.RatingEngine.Models.Suppla
{
	public class OperationRate : RateBase
	{
		#region Members
		private RateShipmentDTO _shipment;
		private RateLoadDTO _load;
		private ResponseDTO _responseDTO;
		private RateLineDTO _rateLine;
		private trServiceRates _serviceRate;
		private RatingModelDTO _model;
		private RatingSchemaDTO _schema;
		#endregion

		#region Properties

		public double WeightKg { get; set; }
		public double VolumetricWeight
		{
			get
			{
				var result = ((_shipment.ShiTotalVolumeM3 ?? 0) * WeightVolumeRatio) / _shipment.Quantity;
				return double.IsNaN(result) ? 0 : result;
			}
		}
		public ResponseDTO ResponseDTO
		{
			get { return _responseDTO ?? (_responseDTO = new ResponseDTO()); }
			set { _responseDTO = value; }
		}

		public double WeightVolumeRatio { get; set; }
		public double MinFreight { get; set; }
		public double RealWeightAdjustment { get; set; }
		public double BillableWeight { get; set; }

		private RatingSchemaDTO Schema
		{
			get { return _schema; }
			set { _schema = value; }
		}

		private RatingModelDTO Model
		{
			get { return _model; }
			set { _model = value; }
		}

		public RateShipmentDTO Shipment
		{
			get { return _shipment; }
			set { _shipment = value; }
		}

		public RateLoadDTO Load
		{
			get { return _load; }
			set { _load = value; }
		}

		public trServiceRates ServiceRate
		{
			get { return _serviceRate; }
			set { _serviceRate = value; }
		}

		public RateLineDTO RateLineDTO
		{
			get { return _rateLine; }
			set { _rateLine = value; }
		}

		#endregion


		public void LoadBaseProperties(RatingSchemaDTO schema, RatingModelDTO model)
		{
			_responseDTO = new ResponseDTO();
			_rateLine = new RateLineDTO();
			_schema = schema;
			_model = model;
		}

		public void LoadGeneralParameters()
		{
			WeightVolumeRatio = GetParameter(RatesArticles.WeightVolumeRatio);
			MinFreight = GetParameter(RatesArticles.MinFreight);
			RealWeightAdjustment = GetParameter(RatesArticles.RealWeightAdjustment);
		}

		public void LoadShipment(RateLoadDTO load)
		{
			if (load.Shipments == null || !load.Shipments.Any())
			{
				//Se cambio la siguiente line para retornar errores acomulados
				//throw new ArgumentException(string.Format(Messages.validation_CannotBeNullOrEmpty, "shipment"));
				ResponseDTO.Message.TypeEnum = MessagesConstants.Types.Error;
				ResponseDTO.Message.Message = string.Join("|", string.Format(Messages.validation_CannotBeNullOrEmpty, "shipment"));
				return;
			}

			_shipment = load.Shipments.FirstOrDefault();

		}

		public void SetLoad(RateLoadDTO load)
		{
			if (load == null)
			{
				ResponseDTO.Message.TypeEnum = MessagesConstants.Types.Error;
				ResponseDTO.Message.Message = string.Join("|", string.Format(Messages.validation_CannotBeNullOrEmpty, "load"));
				return;
			}
			_load = load;
		}

		public void LoadShipment(RateLoadDTO load, string shiNumber)
		{
			if (shiNumber == null)
			{
				//Se cambio la siguiente line para retornar errores acomulados|
				//throw new ArgumentException(string.Format(Messages.validation_CannotBeNullOrEmpty, "shipment"));
				ResponseDTO.Message.TypeEnum = MessagesConstants.Types.Error;
				ResponseDTO.Message.Message = string.Join("|", string.Format(Messages.validation_CannotBeNullOrEmpty, "shipment"));
			}

			_shipment = load.Shipments.FirstOrDefault(p => p.ShiNumber == shiNumber);

		}

		/// <summary>
		/// Obtiene una tarifa por codigo de servicio.
		/// Required: Schema, Model, Article, Active (date and state), Customer, CityFrom, CiteTo
		/// </summary>
		/// <param name="articleCode"></param>
		/// <param name="vehicleType"></param>
		/// <returns></returns>
		public float GetParameter(string articleCode)
		{
			trServiceRates serviceRate;

			using (var ratingEntities = new RatingEntities())
			{
				// Search by
				// Required: Schema, Model, Article, Active (date and state), Customer, CityFrom, CiteTo
				// Optional: 
				serviceRate = (ratingEntities.trServiceRates.Where(p => p.TrrSchema_RsmCode == Schema.Code &&
																																p.TrrRatingModel_RmoCode == Model.Code &&
																																p.TrrArticle_ArtCode == articleCode &&
																																p.TrrShipFrom_CitCode == Shipment.ShipFromCitCode &&
																																p.TrrShipTo_CitCode == Shipment.ShipToCitCode &&
																																p.TrrDateFrom <= Shipment.CreationDate &&
																																p.TrrDateTo >= Shipment.CreationDate &&
																																p.TrrActiveState == (short)RatesConstants.State.Active &&
																																(p.TrrCustomer_UcrCode == Shipment.CustomerUcrCode || p.TrrCustomer_UcrCode == null) &&
																																(p.TrrSupplier_UcrCode == _load.AdministratorUcrCode || p.TrrSupplier_UcrCode == null)
																																)).FirstOrDefault();
			}

			return (float)(serviceRate != null ? serviceRate.TrrValue : 0);

		}

		/// <summary>
		/// Obtiene una tarifa por codigo de servicio y tipo de vehiculo,
		/// Required: Schema, Model, Article, Active (date and state), Customer, CityFrom, CiteTo
		/// </summary>
		/// <param name="articleCode"></param>
		/// <param name="vehicleType"></param>
		/// <returns></returns>
		public float GetParameter(string articleCode, string vehicleType)
		{
			trServiceRates serviceRate;

			using (var ratingEntities = new RatingEntities())
			{
				// Search by
				// Required: Schema, Model, Article, Active (date and state), Customer, CityFrom, CiteTo
				// Optional: 
				serviceRate = (ratingEntities.trServiceRates.Where(p => p.TrrSchema_RsmCode == Schema.Code &&
																																p.TrrRatingModel_RmoCode == Model.Code &&
																																p.TrrArticle_ArtCode == articleCode &&
																																p.TrrShipFrom_CitCode == Shipment.ShipFromCitCode &&
																																p.TrrShipTo_CitCode == Shipment.ShipToCitCode &&
																																p.TrrDateFrom <= Shipment.CreationDate &&
																																p.TrrDateTo >= Shipment.CreationDate &&
																																p.TrrActiveState == (short)RatesConstants.State.Active &&
																																p.TrrTruck_VtyCode == vehicleType &&
																																(p.TrrCustomer_UcrCode == Shipment.CustomerUcrCode || p.TrrCustomer_UcrCode == null) &&
																																(p.TrrSupplier_UcrCode == _load.AdministratorUcrCode || p.TrrSupplier_UcrCode == null)
																																)).FirstOrDefault();
			}

			//if (serviceRate == null)
			//  throw new InvalidOperationException(string.Format(Messages.error_ArticleNotFoundForSchemaModel, articleCode, Schema.Code, Model.Code));

			return (float)(serviceRate != null ? serviceRate.TrrValue : 0);

		}

		/// <summary>
		/// Obtiene una tarifa por codigo de servicio y tipo de trayecto,
		/// Required: Schema, Model, Article, Active (date and state), Customer, CityFrom, CityTo
		/// </summary>
		/// <param name="articleCode"></param>
		/// <param name="routeLegType"></param>
		/// <returns></returns>
		public float GetParameter(string articleCode, char routeLegType)
		{
			trServiceRates serviceRate;
		    var xx = routeLegType.ToString();
			using (var ratingEntities = new RatingEntities())
			{
				// Search by
				// Required: Schema, Model, Article, Active (date and state), Customer, CityFrom, CiteTo
				// Optional: 
				serviceRate = (ratingEntities.trServiceRates.Where(p => p.TrrSchema_RsmCode == Schema.Code &&
																																p.TrrRatingModel_RmoCode == Model.Code &&
																																p.TrrArticle_ArtCode == articleCode &&
																																p.TrrShipFrom_CitCode == Shipment.ShipFromCitCode &&
																																p.TrrShipTo_CitCode == Shipment.ShipToCitCode &&
																																p.TrrDateFrom <= Shipment.CreationDate &&
																																p.TrrDateTo >= Shipment.CreationDate &&
																																p.TrrActiveState == (short)RatesConstants.State.Active &&
																																p.TrrRouteLegType == xx &&
																																(p.TrrCustomer_UcrCode == Shipment.CustomerUcrCode || p.TrrCustomer_UcrCode == null) &&
																																(p.TrrSupplier_UcrCode == _load.AdministratorUcrCode || p.TrrSupplier_UcrCode == null)
																																)).FirstOrDefault();
			}

			//if (serviceRate == null)
			//  throw new InvalidOperationException(string.Format(Messages.error_ArticleNotFoundForSchemaModel, articleCode, Schema.Code, Model.Code));

			return (float)(serviceRate != null ? serviceRate.TrrValue : 0);

		}

		/// <summary>
		/// Obtiene una tarifa (porcentaje) de seguro 
		/// </summary>
		/// <param name="insuranceCode"></param>
		/// <param name="ucrCode"></param>
		/// <param name="nat"></param>
		/// <param name="cityCodeFrom"></param>
		/// <param name="cityCodeTo"></param>
		/// <param name="operationDateTime"></param>
		/// <returns></returns>
		public float GetInsuranceRate(string insuranceCode, string ucrCode, short nat, string cityCodeFrom, string cityCodeTo, DateTime operationDateTime)
		{
			using (var ratingEntities = new RatingEntities())
			{
				// Search by
				// Required: Schema, Model, Article, Active (date and state)
				// Optional: Customer, CityFrom, CiteTo:

				ServiceRate = (ratingEntities.trServiceRates.Where(p => p.TrrSchema_RsmCode == Schema.Code &&
																																p.TrrRatingModel_RmoCode == Model.Code &&
																																p.TrrArticle_ArtCode == insuranceCode &&
																																operationDateTime >= p.TrrDateFrom &&
																																operationDateTime <= p.TrrDateTo &&
																																p.TrrActiveState == (short)RatesConstants.State.Active &&
																																p.TrrNat == nat &&
																																(p.TrrCustomer_UcrCode == ucrCode || p.TrrCustomer_UcrCode == null) &&
																																(p.TrrShipFrom_CitCode == cityCodeFrom || p.TrrShipFrom_CitCode == null) &&
																																(p.TrrShipTo_CitCode == cityCodeTo || p.TrrShipTo_CitCode == null)
					)).FirstOrDefault();
			}
			if (ServiceRate == null)
			{
				//Se cambio la siguiente line para retornar errores acomulados
				//throw new InvalidOperationException(string.Format("No se encontro tarifa con porcentaje de seguro."));
				ResponseDTO.Message.TypeEnum = MessagesConstants.Types.Error;
				ResponseDTO.Message.Message = string.Join("|", "No se encontro tarifa con porcentaje de seguro.");
				return 0;
			}
			return (float)ServiceRate.TrrValue;
		}

		public void LoadRateLineDefaultValues(RateLoadDTO load)
		{
			//concepto que me van a cobrar
			_rateLine.ArtCode = string.IsNullOrEmpty(_model.DefaultArticle) ? RatesArticles.DefaultArticle : _model.DefaultArticle;
			//codigo del schema
			RateLineDTO.SchemaCode = Schema.Code;
			//codigo del modelo
			RateLineDTO.ModelCode = Model.Code;
			//Nombre del modelo
			RateLineDTO.ModelName = Model.Name;
			// Código de la tarifa
			RateLineDTO.RateCode = ServiceRate.TrrCode.ToString();
			//id service rate
			RateLineDTO.RateId = ServiceRate.TrrId;
			RateLineDTO.ProductCode = null;
			RateLineDTO.ShipmentNumber = Shipment.ShiNumber;
			RateLineDTO.LoaNumber = Shipment.ShiLoadLoaNumber;
			RateLineDTO.CustomerCode = load.Nat == (short)RatesConstants.Nature.Positive
				? Shipment.CustomerUcrCode
				: load.AdministratorUcrCode;
		}

		public void CheckMandatoryServiceRate()
		{
			if ((_serviceRate == null || Math.Abs(_serviceRate.TrrValue - 0) < RatesConstants.MinDoubleValue) || Model.NonZeroValueRequired == RatesConstants.Mandatory)
			{
				//Se cambio la siguiente line para retornar errores acomulado
				//throw new InvalidOperationException(string.Format(Messages.error_MandatoryFreightParcelRateNotFound, Schema.Code, Model.Code, RatesArticles.Freight, _shipment.CustomerUcrCode, _shipment.RouCode, _shipment.ShipFromCitCode, _shipment.ShipToCitCode));
				ResponseDTO.Message.TypeEnum = MessagesConstants.Types.Error;
				ResponseDTO.Message.Message = string.Format(Messages.error_MandatoryFreightParcelRateNotFound, Schema.Code, Model.Code, RatesArticles.Freight, _shipment.CustomerUcrCode, _shipment.RouCode, _shipment.ShipFromCitCode, _shipment.ShipToCitCode);

			}
		}

		public void ValidateShipmentDetail()
		{
			if (Shipment.ShiDetails != null)
			{
				var shiProduct = Shipment.ShiDetails.FirstOrDefault();

				if (shiProduct != null && shiProduct.UomCode != RatesUnitsOfMeasure.Box)
				{
					ResponseDTO.Message.TypeEnum = MessagesConstants.Types.Error;
					ResponseDTO.Message.Message = string.Join("|", "La unidad de medida del producto no es caja.");
				}

			}
			else
			{
				ResponseDTO.Message.TypeEnum = MessagesConstants.Types.Error;
				ResponseDTO.Message.Message = string.Join("|", "No existe producto asociado a la remesa.");
			}
		}

		#region LoadRate

		//Modelo de busqueda de la tarifa 1
		public void LoadRateSearchModelA(string article)
		{
			using (var ratingEntities = new RatingEntities())
			{
				if (_load.Nat == (short)RatesConstants.Nature.Positive)
				{

					_serviceRate = (ratingEntities.trServiceRates.Where(
																												p => p.TrrSchema_RsmCode == Schema.Code &&
																												p.TrrRatingModel_RmoCode == Model.Code &&
																												p.TrrArticle_ArtCode == article &&
																												Shipment.CreationDate >= p.TrrDateFrom &&
																												Shipment.CreationDate <= p.TrrDateTo &&
																												p.TrrActiveState == (short)RatesConstants.State.Active &&
																												p.TrrNat == _load.Nat &&
																												(p.TrrCustomer_UcrCode == Shipment.CustomerUcrCode || p.TrrCustomer_UcrCode == null) &&
																												(p.TrrShipFrom_CitCode == Shipment.ShipFromCitCode || p.TrrShipFrom_CitCode == null) &&
																												(p.TrrShipTo_CitCode == Shipment.ShipToCitCode || p.TrrShipTo_CitCode == null)
										)).OrderByDescending(p => p.TrrCustomer_UcrCode)
										.ThenByDescending(s => s.TrrShipFrom_CitCode)
										.ThenByDescending(t => t.TrrShipTo_CitCode).FirstOrDefault();

				}
				else
				{
					_serviceRate = (ratingEntities.trServiceRates.Where(
																							p => p.TrrSchema_RsmCode == Schema.Code &&
																							p.TrrRatingModel_RmoCode == Model.Code &&
																							p.TrrArticle_ArtCode == article &&
																							Load.LoaCreationDate >= p.TrrDateFrom &&
																							Load.LoaCreationDate <= p.TrrDateTo &&
																							p.TrrActiveState == (short)RatesConstants.State.Active &&
																							p.TrrNat == _load.Nat &&
																							(p.TrrSupplier_UcrCode == Load.AdministratorUcrCode || p.TrrSupplier_UcrCode == null) &&
																							(p.TrrShipFrom_CitCode == Load.LoaFromCitCode || p.TrrShipFrom_CitCode == null) &&
																							(p.TrrShipTo_CitCode == Load.LoaToCitCode || p.TrrShipTo_CitCode == null)
										)).OrderByDescending(p => p.TrrSupplier_UcrCode)
										.ThenByDescending(s => s.TrrShipFrom_CitCode)
										.ThenByDescending(t => t.TrrShipTo_CitCode).FirstOrDefault();
				}

			}
		}

		//Modelo de busqueda de la tarifa 2
		public void LoadRateSearchModelB(string article)
		{
			using (var ratingEntities = new RatingEntities())
			{
				if (Load.Nat == (short)RatesConstants.Nature.Positive)
				{
					_serviceRate = (ratingEntities.trServiceRates.Where(p => p.TrrSchema_RsmCode == Schema.Code &&
																												p.TrrRatingModel_RmoCode == Model.Code &&
																												p.TrrArticle_ArtCode == article &&
																												Shipment.CreationDate >= p.TrrDateFrom &&
																												Shipment.CreationDate <= p.TrrDateTo &&
																												p.TrrActiveState == (short)RatesConstants.State.Active &&
																												p.TrrNat == Load.Nat &&
																												p.TrrTruck_VtyCode == Load.VtyCode &&
																												p.TrrUOMOficial_UofCode == Shipment.TypeOfPackaging &&
																												(p.TrrCustomer_UcrCode == Shipment.CustomerUcrCode || p.TrrCustomer_UcrCode == null) &&
																												(p.TrrShipFrom_CitCode == Shipment.ShipFromCitCode || p.TrrShipFrom_CitCode == null) &&
																												(p.TrrShipTo_CitCode == Shipment.ShipToCitCode || p.TrrShipTo_CitCode == null)
								)).OrderByDescending(p => p.TrrCustomer_UcrCode)
									.ThenByDescending(s => s.TrrShipFrom_CitCode)
									.ThenByDescending(t => t.TrrShipTo_CitCode).FirstOrDefault();
				}
				else
				{
					_serviceRate = (ratingEntities.trServiceRates.Where(p => p.TrrSchema_RsmCode == Schema.Code &&
																							p.TrrRatingModel_RmoCode == Model.Code &&
																							p.TrrArticle_ArtCode == article &&
																							Load.LoaCreationDate >= p.TrrDateFrom &&
																							Load.LoaCreationDate <= p.TrrDateTo &&
																							p.TrrActiveState == (short)RatesConstants.State.Active &&
																							p.TrrNat == Load.Nat &&
																							p.TrrTruck_VtyCode == Load.VtyCode &&
																							p.TrrUOMOficial_UofCode == Shipment.TypeOfPackaging &&
																							(p.TrrSupplier_UcrCode == Load.AdministratorUcrCode || p.TrrSupplier_UcrCode == null) &&
																							(p.TrrShipFrom_CitCode == Load.LoaFromCitCode || p.TrrShipFrom_CitCode == null) &&
																							(p.TrrShipTo_CitCode == Load.LoaToCitCode || p.TrrShipTo_CitCode == null)
								)).OrderByDescending(r => r.TrrSupplier_UcrCode)
									.ThenByDescending(s => s.TrrShipFrom_CitCode)
									.ThenByDescending(t => t.TrrShipTo_CitCode).FirstOrDefault();
				}

			}
		}

		//Modelo de busqueda de la tarifa 3
		public void LoadRateSearchModelC(string article)
		{
			using (var ratingEntities = new RatingEntities())
			{
				if (Load.Nat == (short)RatesConstants.Nature.Positive)
				{
					// Tarifa cliente:
					_serviceRate = (ratingEntities.trServiceRates.Where(p => p.TrrSchema_RsmCode == Schema.Code &&
																											p.TrrRatingModel_RmoCode == Model.Code &&
																											p.TrrArticle_ArtCode == article &&
																											Shipment.CreationDate >= p.TrrDateFrom &&
																											Shipment.CreationDate <= p.TrrDateTo &&
																											p.TrrActiveState == (short)RatesConstants.State.Active &&
																											p.TrrNat == Load.Nat &&
																											p.TrrTruck_VtyCode == Load.VtyCode &&
																											(p.TrrCustomer_UcrCode == Shipment.CustomerUcrCode || p.TrrCustomer_UcrCode == null) &&
																											(p.TrrShipFrom_CitCode == Shipment.ShipFromCitCode || p.TrrShipFrom_CitCode == null) &&
																											(p.TrrShipTo_CitCode == Shipment.ShipToCitCode || p.TrrShipTo_CitCode == null)
								)).OrderByDescending(r => r.TrrCustomer_UcrCode)
									.ThenByDescending(s => s.TrrShipFrom_CitCode)
									.ThenByDescending(t => t.TrrShipTo_CitCode).FirstOrDefault();


				}
				else
				{
					// Tarifa proveedor
					_serviceRate = (ratingEntities.trServiceRates.Where(p => p.TrrSchema_RsmCode == Schema.Code &&
																						p.TrrRatingModel_RmoCode == Model.Code &&
																						p.TrrArticle_ArtCode == article &&
																						Load.LoaCreationDate >= p.TrrDateFrom &&
																						Load.LoaCreationDate <= p.TrrDateTo &&
																						p.TrrActiveState == (short)RatesConstants.State.Active &&
																						p.TrrNat == Load.Nat &&
																						p.TrrTruck_VtyCode == Load.VtyCode &&
																						(p.TrrSupplier_UcrCode == Load.AdministratorUcrCode || p.TrrSupplier_UcrCode == null) &&
																						(p.TrrShipFrom_CitCode == Load.LoaFromCitCode || p.TrrShipFrom_CitCode == null) &&
																						(p.TrrShipTo_CitCode == Load.LoaToCitCode || p.TrrShipTo_CitCode == null)
								)).OrderByDescending(r => r.TrrSupplier_UcrCode)
									.ThenByDescending(s => s.TrrShipFrom_CitCode)
									.ThenByDescending(t => t.TrrShipTo_CitCode).FirstOrDefault();
				}
			}
		}

        //Modelo de busqueda de la tarifa 3

        public void LoadRateSearchModelD(string article)
        {
            using (var ratingEntities = new RatingEntities())
            {
                if (Load.Nat == (short)RatesConstants.Nature.Positive)
                {
                    // Tarifa cliente:
                    _serviceRate = (ratingEntities.trServiceRates.Where(p => p.TrrSchema_RsmCode == Schema.Code &&
                                                                                                            p.TrrRatingModel_RmoCode == Model.Code &&
                                                                                                            p.TrrArticle_ArtCode == article &&
                                                                                                            Shipment.CreationDate >= p.TrrDateFrom &&
                                                                                                            Shipment.CreationDate <= p.TrrDateTo &&
                                                                                                            p.TrrActiveState == (short)RatesConstants.State.Active &&
                                                                                                            p.TrrNat == Load.Nat &&
                                                                                                            p.TrrTruck_VtyCode == Load.VtyCode &&
                                                                                                            (p.TrrCustomer_UcrCode == Shipment.CustomerUcrCode || p.TrrCustomer_UcrCode == null) &&
                                                                                                            (p.TrrShipFrom_CitCode == Shipment.ShipFromCitCode) &&
                                                                                                            (p.TrrShipTo_CitCode == Shipment.ShipToCitCode)
                                )).OrderByDescending(r => r.TrrCustomer_UcrCode)
                                    .ThenByDescending(s => s.TrrShipFrom_CitCode)
                                    .ThenByDescending(t => t.TrrShipTo_CitCode).FirstOrDefault();


                }
                else
                {
                    // Tarifa proveedor
                    _serviceRate = (ratingEntities.trServiceRates.Where(p => p.TrrSchema_RsmCode == Schema.Code &&
                                                                                        p.TrrRatingModel_RmoCode == Model.Code &&
                                                                                        p.TrrArticle_ArtCode == article &&
                                                                                        Load.LoaCreationDate >= p.TrrDateFrom &&
                                                                                        Load.LoaCreationDate <= p.TrrDateTo &&
                                                                                        p.TrrActiveState == (short)RatesConstants.State.Active &&
                                                                                        p.TrrNat == Load.Nat &&
                                                                                        p.TrrTruck_VtyCode == Load.VtyCode &&
                                                                                        (p.TrrSupplier_UcrCode == Load.AdministratorUcrCode || p.TrrSupplier_UcrCode == null) &&
                                                                                        (p.TrrShipFrom_CitCode == Load.LoaFromCitCode) &&
                                                                                        (p.TrrShipTo_CitCode == Load.LoaToCitCode )
                                )).OrderByDescending(r => r.TrrSupplier_UcrCode)
                                    .ThenByDescending(s => s.TrrShipFrom_CitCode)
                                    .ThenByDescending(t => t.TrrShipTo_CitCode).FirstOrDefault();
                }
            }
        }

		#endregion


	}
}
