﻿using System;
using System.Collections.Generic;
using System.Linq;
using Simplexity.RatingEngine.Constants;
using Simplexity.RatingEngine.DTOs;
using Simplexity.RatingEngine.EF;
using Simplexity.RatingEngine.Resources;

namespace Simplexity.RatingEngine.Models.Suppla
{
	/// <summary>
	/// Aplica: Cliente
	/// Modelo: Seguro por valor de flete: La tarifa establece un valor de seguro teniendo el valor del flete
	/// El calculo esta dado por 1 despacho y se debe enviar un load con un shipment.
	/// </summary>
	public class T312 : OperationRate, IRating
	{
		public ResponseDTO Calculate(RateLoadDTO load, RatingSchemaDTO schema, RatingModelDTO model, List<RateLineDTO> rateLines)
		{
			#region 1. Load Objects

			LoadBaseProperties(schema, model);
			SetLoad(load);
			LoadShipment(load);

			#endregion

			#region 2. Find rate and parameters, run formula

			if (Shipment == null) return ResponseDTO;
			LoadRateSearchModelC(RatesArticles.InsuranceOnFreight);

			//Jair 18/06/2015: Se calcula el valor del seguro teniendo en cuenta el valor del flete
			var billiableQuantity = rateLines.Where(p => p.ArtCode == RatesArticles.Freight).Sum(p => p.ExtendedPrice);
			var unitPrice = ServiceRate != null ? ServiceRate.TrrValue : 0;
			var extendedPrice = billiableQuantity * unitPrice;

			#endregion

			#region 3. Validate Freight

			if (model.NonZeroValueRequired == RatesConstants.Mandatory)
			{
				if (Math.Abs(extendedPrice) > 0)
				{
					LoadRateLineDefaultValues(load);
					RateLineDTO.Quantity = billiableQuantity;
					RateLineDTO.UnitPrice = unitPrice;
					RateLineDTO.ExtendedPrice = extendedPrice;
					RateLineDTO.UomCode = RatesUnitsOfMeasure.Unit;
					ResponseDTO.RateLines.Add(RateLineDTO);
				}
				else
				{
					ResponseDTO.Message.TypeEnum = MessagesConstants.Types.Error;
					ResponseDTO.Message.Message =
						string.Join("|", string.Format(Messages.error_MandatoryRateNotFoundVehType, schema.Name, model.Name,
												RatesArticles.Loading, load.VtyCode, Shipment.CustomerUcrCode, Shipment.ShipFromCitCode, Shipment.ShipToCitCode));
				}
			}
			else
			{
				if (Math.Abs(extendedPrice) > 0)
				{
					LoadRateLineDefaultValues(load);
					RateLineDTO.Quantity = billiableQuantity;
					RateLineDTO.UnitPrice = unitPrice;
					RateLineDTO.ExtendedPrice = extendedPrice;
					RateLineDTO.UomCode = RatesUnitsOfMeasure.Unit;
					ResponseDTO.RateLines.Add(RateLineDTO);
				}
			}

			#endregion

			return ResponseDTO;
		}
	}
}