﻿using System;
using System.Collections.Generic;
using System.Linq;
using Simplexity.RatingEngine.Constants;
using Simplexity.RatingEngine.DTOs;
using Simplexity.RatingEngine.EF;

namespace Simplexity.RatingEngine.Models.Suppla
{
	public class T303 : OperationRate, IRating
	{
		public ResponseDTO Calculate(RateLoadDTO load, RatingSchemaDTO schema, RatingModelDTO model, List<RateLineDTO> rateLines)
		{
			#region 1. Load Objects

			LoadBaseProperties(schema, model);
			SetLoad(load);
			LoadShipment(load);
			LoadGeneralParameters();

			#endregion

			#region 1.5 validations

			ValidateShipmentDetail();

			#endregion

			#region 2. Find rate and parameters, run formula

			if (Shipment == null) return ResponseDTO;
			LoadRateSearchModelA(RatesArticles.Freight);

			if (ServiceRate == null)
			{
				ResponseDTO.Message.TypeEnum = MessagesConstants.Types.Error;
				ResponseDTO.Message.Message = 
					string.Join("|", string.Format("No se encontro tarifa para el articulo ({0}), esquema ({1}), modelo ({2})",
											RatesArticles.Freight, schema.Code, model.Code));
				return ResponseDTO;
			}
			

			#endregion

			#region 3. Load parameters

			var minWeightByFirstUnit = GetParameter(RatesArticles.MinWeightByFirstUnit);
			var minWeightByAdditionalUnit = GetParameter(RatesArticles.MinWeightByAdditionalUnit);

			#endregion

			#region  5. Calculate weightToBill

			if (Shipment.ShiTotalWeightKg < RealWeightAdjustment)
			{

				var maxWeight = Math.Max(Shipment.ShiTotalWeightKg, VolumetricWeight);
				var minWeight = minWeightByFirstUnit + (minWeightByAdditionalUnit * (Shipment.Quantity - 1));

				BillableWeight = minWeight < maxWeight ? maxWeight : minWeight;

			}
			else
			{
				BillableWeight = Shipment.ShiTotalWeightKg;
			}

			#endregion

			#region 6. Calculate Freight
			if ((BillableWeight * ServiceRate.TrrValue) < MinFreight)
			{
				LoadRateLineDefaultValues(load);
				RateLineDTO.Quantity = 1;
				RateLineDTO.UnitPrice = MinFreight;
				RateLineDTO.UomCode = RatesUnitsOfMeasure.MinFreight;
			}
			else
			{
				LoadRateLineDefaultValues(load);
				RateLineDTO.Quantity = BillableWeight;
				RateLineDTO.UnitPrice = ServiceRate.TrrValue;
				RateLineDTO.UomCode = RatesUnitsOfMeasure.Kilogram;
			}

			//naturaleza--(-1) a pagar (+1) a cobrar
			RateLineDTO.Nat = load.Nat;
			RateLineDTO.ExtendedPrice = RateLineDTO.Quantity * RateLineDTO.UnitPrice;

			if (Math.Abs(ServiceRate.TrrValue - 0) > RatesConstants.MinDoubleValue && Math.Abs(RateLineDTO.ExtendedPrice - 0) > RatesConstants.MinDoubleValue)
			{
				ResponseDTO.RateLines.Add(RateLineDTO);
			}

			#endregion

			return ResponseDTO;
		}
	}
}
