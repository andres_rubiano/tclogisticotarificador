﻿using System;
using System.Collections.Generic;
using System.Linq;
using Simplexity.RatingEngine.Constants;
using Simplexity.RatingEngine.DTOs;
using Simplexity.RatingEngine.EF;
using Simplexity.RatingEngine.Resources;

namespace Simplexity.RatingEngine.Models.Suppla
{
	/// <summary>
	/// Aplica: Cliente
	/// Modelo: Costo sobre manejo urbano: La tarifa establece un valor de felte teniendo en cuenta el costo de manejo sobre valor declarado y el minimo costo por unidad
	/// El calculo esta dado por 1 despacho y se debe enviar un load con un shipment.
	/// </summary>
	public class T313 : OperationRate, IRating
	{
		public ResponseDTO Calculate(RateLoadDTO load, RatingSchemaDTO schema, RatingModelDTO model, List<RateLineDTO> rateLines)
		{
			#region 1. Load Objects

			LoadBaseProperties(schema, model);
			SetLoad(load);
			LoadShipment(load);

			#endregion

			#region 2. Find rate and parameters, run formula

			if (Shipment == null) return ResponseDTO;

			LoadRateSearchModelA(RatesArticleService.CostHandlingDecValue);

			if (ServiceRate == null)
			{
				ResponseDTO.Message.TypeEnum = MessagesConstants.Types.Error;
				ResponseDTO.Message.Message =
					string.Join("|", string.Format("No se encotró tarifa para el articulo ({0})", RatesArticleService.CostHandlingDecValue));
				return ResponseDTO;
			}

			// Formula:

			var minCostHandlingUnit = GetParameter(RatesArticleService.MinCostHandlingUnit);

			var shiProduct = Shipment.ShiDetails.FirstOrDefault();

			if (shiProduct == null)
			{
				ResponseDTO.Message.TypeEnum = MessagesConstants.Types.Error;
				ResponseDTO.Message.Message = string.Join("|", "La remesa no tiene producto asginado");
				return ResponseDTO;
			}

			var unitPrice = (ServiceRate.TrrValue * shiProduct.DeclaredValue) / shiProduct.Quantity < minCostHandlingUnit
				? minCostHandlingUnit * shiProduct.Quantity
				: shiProduct.DeclaredValue * ServiceRate.TrrValue;
			
			var extendedPrice = unitPrice;

			#endregion

			#region 3. Validate Freight

			if (model.NonZeroValueRequired == RatesConstants.Mandatory)
			{
				if (Math.Abs(extendedPrice) > 0)
				{
					LoadRateLineDefaultValues(load);
					RateLineDTO.Quantity = 1;
					RateLineDTO.UnitPrice = unitPrice;
					RateLineDTO.ExtendedPrice = extendedPrice;
					RateLineDTO.UomCode = RatesUnitsOfMeasure.Unit;
					ResponseDTO.RateLines.Add(RateLineDTO);
				}
				else
				{
					ResponseDTO.Message.TypeEnum = MessagesConstants.Types.Error;
					ResponseDTO.Message.Message =
						string.Join("|", string.Format(Messages.error_MandatoryRateNotFound, schema.Name, model.Name, RatesArticles.Loading,
												Shipment.CustomerUcrCode, Shipment.ShipFromCitCode, Shipment.ShipToCitCode));
				}
			}
			else
			{
				if (Math.Abs(extendedPrice) > 0)
				{
					LoadRateLineDefaultValues(load);
					RateLineDTO.Quantity = 1;
					RateLineDTO.UnitPrice = unitPrice;
					RateLineDTO.ExtendedPrice = extendedPrice;
					RateLineDTO.UomCode = RatesUnitsOfMeasure.Unit;
					ResponseDTO.RateLines.Add(RateLineDTO);
				}
			}

			#endregion

			return ResponseDTO;
		}
	}
}