﻿using System;
using System.Collections.Generic;
using System.Linq;
using Simplexity.RatingEngine.Constants;
using Simplexity.RatingEngine.DTOs;
using Simplexity.RatingEngine.EF;

namespace Simplexity.RatingEngine.Models.Suppla
{
	/// <summary>
	/// Aplica: Cliente / Proveedor
	/// Modelo: Flete por contenedor: La tarifa establece un valor de flete teniendo en cuenta el tipo de contenedor y el tipo de vehículo
	/// El calculo esta dado por 1 viaje para el caso del proveedor o 1 remesa para el caso del cliente.
	/// </summary>
	public class T302 : OperationRate, IRating
	{
		public ResponseDTO Calculate(RateLoadDTO load, RatingSchemaDTO schema, RatingModelDTO model, List<RateLineDTO> rateLines)
		{
			#region 1. Load Objects
			LoadBaseProperties(schema, model);
			SetLoad(load);
			if(load.Nat == (short)RatesConstants.Nature.Positive)
			{
				LoadShipment(load);
				Calculate(load, model);
			}
			else
			{
				foreach (var shipment in load.Shipments)
				{
					LoadShipment(load, shipment.ShiNumber);
					Calculate(load, model);
				}

			}

			#endregion

			return ResponseDTO;
		}

		private void Calculate(RateLoadDTO load, RatingModelDTO model)
		{

			#region 2. Find rate and parameters, run formula

			if (Shipment == null) return;
			LoadRateSearchModelB(RatesArticles.Freight);
			
			// Es para un contenedor:
			var billiableQuantity = 1;
			var unitPrice = ServiceRate != null ? ServiceRate.TrrValue : 0;
			var extendedPrice = billiableQuantity * unitPrice;

			#endregion

			#region 3. Validate Freight

			if (model.NonZeroValueRequired == RatesConstants.Mandatory)
			{
				if (Math.Abs(extendedPrice) > 0)
				{
					LoadRateLineDefaultValues(load);
					RateLineDTO.Quantity = billiableQuantity;
					RateLineDTO.UnitPrice = unitPrice;
					RateLineDTO.ExtendedPrice = extendedPrice;
					RateLineDTO.UomCode = RatesUnitsOfMeasure.Container;
					ResponseDTO.RateLines.Add(RateLineDTO);
				}
				else
				{
					ResponseDTO.Message.TypeEnum = MessagesConstants.Types.Error;
					ResponseDTO.Message.Message = string.Join("|", "No se calculo tarifa.");
				}
			}
			else
			{
				if (!(Math.Abs(extendedPrice) > 0)) return;
				LoadRateLineDefaultValues(load);
				RateLineDTO.Quantity = billiableQuantity;
				RateLineDTO.UnitPrice = unitPrice;
				RateLineDTO.ExtendedPrice = extendedPrice;
				RateLineDTO.UomCode = RatesUnitsOfMeasure.Container;
				ResponseDTO.RateLines.Add(RateLineDTO);
			}

			#endregion
			
		}



	}
}