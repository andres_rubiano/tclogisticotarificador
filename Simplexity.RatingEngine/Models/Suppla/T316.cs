﻿using System;
using System.Collections.Generic;
using System.Linq;
using Simplexity.RatingEngine.Constants;
using Simplexity.RatingEngine.DTOs;
using Simplexity.RatingEngine.EF;
using Simplexity.RatingEngine.Resources;

namespace Simplexity.RatingEngine.Models.Suppla
{
    public class T316 : OperationRate, IRating
    {
        public ResponseDTO Calculate(RateLoadDTO load, RatingSchemaDTO schema, RatingModelDTO model, List<RateLineDTO> rateLines)
        {
            Utilities.ValidateUpdatingRates();

            #region 1. Load Objects

            LoadBaseProperties(schema, model);
            SetLoad(load);
            LoadShipment(load);

            #endregion

            #region 2. Find rate and parameters, run formula

            if (Shipment == null) return ResponseDTO;

            #endregion

            double costoTotal = 0;
            foreach (var shipmentDto in load.Shipments)
            {
                if (shipmentDto.ShtCode != "TREMTRA" && shipmentDto.Waybills.Count > 0)
                {
                    foreach (var rateWaybillDto in shipmentDto.Waybills)
                    {
                        var costoKgInicial = ObtenerTarifa(RatesArticles.CostoKgIni, model, schema, rateWaybillDto, load.CarrierUcrCode);
                        var costoKgAdicional = ObtenerTarifa(RatesArticles.CostoKgAdd, model, schema, rateWaybillDto, load.CarrierUcrCode);
                        var pesoMinimoPorCajaEnKg = ObtenerParametro(RatesArticles.PesoMinCajaKg, model, schema, rateWaybillDto, load.CarrierUcrCode);
                        var relacionPesoVolumen = ObtenerParametro(RatesArticles.WeightVolumeRatio, model, schema, rateWaybillDto, load.CarrierUcrCode);
                        var pesoMinimoPorDespacho = ObtenerParametro(RatesArticles.MinWeightByDispatch, model, schema,
                          rateWaybillDto, load.CarrierUcrCode);
                        var costoMinimoPorUnidad = ObtenerParametro(RatesArticles.CostoMinUnid, model, schema, rateWaybillDto, load.CarrierUcrCode);
                        var costoMinimoPorDespacho = ObtenerParametro(RatesArticles.CostoMinDesp, model, schema, rateWaybillDto, load.CarrierUcrCode);
                        var pesoCajas = rateWaybillDto.Quantity * pesoMinimoPorCajaEnKg;
                        var pesoVolumetrico = rateWaybillDto.TotalVolumeM3 * relacionPesoVolumen;
                        var pesoATarificar = Math.Max(Math.Max(Math.Max(rateWaybillDto.TotalWeightKg, pesoCajas), pesoVolumetrico),
                          pesoMinimoPorDespacho);
                        var costoTotalPorPeso = costoKgInicial + ((pesoATarificar - 1) * costoKgAdicional);
                        var costoTotalPorUnidad = rateWaybillDto.Quantity * costoMinimoPorUnidad;
                        var costo = Math.Max(Math.Max(costoTotalPorPeso, costoTotalPorUnidad), costoMinimoPorDespacho);
                        if ((Math.Abs(costo - 0) <= RatesConstants.MinDoubleValue))
                        {
                            throw new ArgumentException(string.Format(Messages.error_ArticleNotFoundForSchemaModelRouteLegType, RatesArticles.Freight, schema.Code, model.Code, load.CarrierUcrCode, rateWaybillDto.RouteLegType));
                        }
                        costoTotal += costo;
                    }
                }
                else
                {
                    var costoKgInicial = ObtenerTarifa(RatesArticles.CostoKgIni, model, schema, shipmentDto, load.CarrierUcrCode);
                    var costoKgAdicional = ObtenerTarifa(RatesArticles.CostoKgAdd, model, schema, shipmentDto, load.CarrierUcrCode);
                    var pesoMinimoPorCajaEnKg = ObtenerParametro(RatesArticles.PesoMinCajaKg, model, schema, shipmentDto, load.CarrierUcrCode);
                    var relacionPesoVolumen = ObtenerParametro(RatesArticles.WeightVolumeRatio, model, schema, shipmentDto, load.CarrierUcrCode);
                    var pesoMinimoPorDespacho = ObtenerParametro(RatesArticles.MinWeightByDispatch, model, schema, shipmentDto, load.CarrierUcrCode);
                    var costoMinimoPorUnidad = ObtenerParametro(RatesArticles.CostoMinUnid, model, schema, shipmentDto, load.CarrierUcrCode);
                    var costoMinimoPorDespacho = ObtenerParametro(RatesArticles.CostoMinDesp, model, schema, shipmentDto, load.CarrierUcrCode);
                    var pesoCajas = shipmentDto.Quantity * pesoMinimoPorCajaEnKg;
                    var pesoVolumetrico = shipmentDto.VolumeM3 * relacionPesoVolumen;
                    var pesoATarificar = Math.Max(Math.Max(Math.Max(shipmentDto.WeightKg, pesoCajas), pesoVolumetrico),
                      pesoMinimoPorDespacho);
                    var costoTotalPorPeso = costoKgInicial + ((pesoATarificar - 1) * costoKgAdicional);
                    var costoTotalPorUnidad = shipmentDto.Quantity * costoMinimoPorUnidad;
                    var costo = Math.Max(Math.Max(costoTotalPorPeso, costoTotalPorUnidad), costoMinimoPorDespacho);
                    if ((Math.Abs(costo - 0) <= RatesConstants.MinDoubleValue))
                    {
                        throw new ArgumentException(string.Format(Messages.error_ArticleNotFoundForSchemaModelRouteLegType, RatesArticles.Freight, schema.Code, model.Code, load.CarrierUcrCode, shipmentDto.RouteLegType));
                    }
                    costoTotal += costo;
                }
            }

            if (model.NonZeroValueRequired == RatesConstants.Mandatory)
            {
                if (Math.Abs(costoTotal) > 0)
                {
                    RateLineDTO.ArtCode = RatesArticles.Freight;
                    //codigo del schema
                    RateLineDTO.SchemaCode = schema.Code;
                    //codigo del modelo
                    RateLineDTO.ModelCode = model.Code;
                    //Nombre del modelo
                    RateLineDTO.ModelName = model.Name;
                    // Código de la tarifa
                    //RateLineDTO.RateCode = ServiceRate.TrrCode.ToString();
                    //id service rate
                    //RateLineDTO.RateId = ServiceRate.TrrId;
                    RateLineDTO.ProductCode = null;
                    RateLineDTO.ShipmentNumber = Shipment.ShiNumber;
                    RateLineDTO.LoaNumber = Shipment.ShiLoadLoaNumber;
                    RateLineDTO.CustomerCode = load.Nat == (short)RatesConstants.Nature.Positive
                      ? Shipment.CustomerUcrCode
                      : load.AdministratorUcrCode;

                    RateLineDTO.Quantity = 1;
                    RateLineDTO.ExtendedPrice = costoTotal;
                    RateLineDTO.UnitPrice = costoTotal;
                    RateLineDTO.UomCode = RatesUnitsOfMeasure.Viaje;
                    ResponseDTO.RateLines.Add(RateLineDTO);
                }
                else
                {
                    ResponseDTO.Message.TypeEnum = MessagesConstants.Types.Error;
                    ResponseDTO.Message.Message = string.Join("|", "No se calculo tarifa.");
                }
            }
            else
            {
                if (Math.Abs(costoTotal) > 0)
                {
                    RateLineDTO.ArtCode = RatesArticles.Freight;
                    //codigo del schema
                    RateLineDTO.SchemaCode = schema.Code;
                    //codigo del modelo
                    RateLineDTO.ModelCode = model.Code;
                    //Nombre del modelo
                    RateLineDTO.ModelName = model.Name;
                    // Código de la tarifa
                    //RateLineDTO.RateCode = ServiceRate.TrrCode.ToString();
                    //id service rate
                    //RateLineDTO.RateId = ServiceRate.TrrId;
                    RateLineDTO.ProductCode = null;
                    RateLineDTO.ShipmentNumber = Shipment.ShiNumber;
                    RateLineDTO.LoaNumber = Shipment.ShiLoadLoaNumber;
                    RateLineDTO.CustomerCode = load.Nat == (short)RatesConstants.Nature.Positive
                      ? Shipment.CustomerUcrCode
                      : load.AdministratorUcrCode;

                    RateLineDTO.Quantity = 1;
                    RateLineDTO.ExtendedPrice = costoTotal;
                    RateLineDTO.UnitPrice = costoTotal;
                    RateLineDTO.UomCode = RatesUnitsOfMeasure.Viaje;
                    ResponseDTO.RateLines.Add(RateLineDTO);
                }
            }

            return ResponseDTO;
        }

        #region private

        /// <summary>
        /// busca una tarifa de coincidencia exacta para este artículo por esquema, modelo, tipo de ruta, ciudad origen y ciudad destino; opcional por proveedor. para la remesa
        /// </summary>
        /// <param name="articleCode"></param>
        /// <param name="model"></param>
        /// <param name="schema"></param>
        /// <param name="rateShipmentDto"></param>
        /// <returns>Valor de la tarifa</returns>
        private float ObtenerTarifa(string articleCode, RatingModelDTO model, RatingSchemaDTO schema, RateShipmentDTO rateShipmentDto, string carrierUcrCode)
        {
            #region Conexion conEF

            //trServiceRates serviceRate;
            //var routeLegType = rateShipmentDto.RouteLegType;
            //using (var ratingEntities = new RatingEntities())
            //{
            //    serviceRate = (ratingEntities.trServiceRates.Where(p => p.TrrSchema_RsmCode == schema.Code &&
            //                                                            p.TrrNat == (int)RatesConstants.Nature.Negative &&
            //                                                            p.TrrRatingModel_RmoCode == model.Code &&
            //                                                            p.TrrArticle_ArtCode == articleCode &&
            //                                                            p.TrrShipFrom_CitCode == rateShipmentDto.ShipFromCitCode &&
            //                                                            p.TrrShipTo_CitCode == rateShipmentDto.ShipToCitCode &&
            //                                                            p.TrrDateFrom <= rateShipmentDto.CreationDate &&
            //                                                            p.TrrDateTo >= rateShipmentDto.CreationDate &&
            //                                                            p.TrrActiveState ==
            //                                                            (short)RatesConstants.State.Active &&
            //                                                            p.TrrRouteLegType == routeLegType &&
            //                                                            (p.TrrSupplier_UcrCode == carrierUcrCode ||
            //                                                             p.TrrSupplier_UcrCode == null)
            //      )).OrderByDescending(o => o.TrrSupplier_UcrCode).FirstOrDefault();
            //}

            //if (serviceRate == null || (Math.Abs(serviceRate.TrrValue - 0) < RatesConstants.MinDoubleValue))
            //{
            //    throw new ArgumentException(string.Format(Messages.error_ArticleNotFoundForSchemaModelRouteLegTypeFrom_CitCode, articleCode, schema.Code, model.Code, carrierUcrCode, routeLegType, rateShipmentDto.ShipFromCitCode, rateShipmentDto.ShipToCitCode));
            //}

            //return (float)(serviceRate != null ? serviceRate.TrrValue : 0);

            #endregion

            #region Tarifario en memoria

            var routeLegType = rateShipmentDto.RouteLegType;

            var serviceRate = ServiceRates.RateList.Where(p => p.TrrSchema_RsmCode == schema.Code &&
                                                               p.TrrNat == (int)RatesConstants.Nature.Negative &&
                                                               p.TrrRatingModel_RmoCode == model.Code &&
                                                               p.TrrArticle_ArtCode == articleCode &&
                                                               p.TrrShipFrom_CitCode == rateShipmentDto.ShipFromCitCode &&
                                                               p.TrrShipTo_CitCode == rateShipmentDto.ShipToCitCode &&
                                                               p.TrrDateFrom <= rateShipmentDto.CreationDate &&
                                                               p.TrrDateTo >= rateShipmentDto.CreationDate &&
                                                               p.TrrActiveState ==
                                                               (short)RatesConstants.State.Active &&
                                                               p.TrrRouteLegType == routeLegType &&
                                                               (p.TrrSupplier_UcrCode == carrierUcrCode ||
                                                                p.TrrSupplier_UcrCode == null)
                ).OrderByDescending(o => o.TrrSupplier_UcrCode).FirstOrDefault();

            if (serviceRate == null || (Math.Abs(serviceRate.TrrValue - 0) < RatesConstants.MinDoubleValue))
            {
                throw new ArgumentException(
                    string.Format(Messages.error_ArticleNotFoundForSchemaModelRouteLegTypeFrom_CitCode, articleCode,
                        schema.Code, model.Code, carrierUcrCode, routeLegType, rateShipmentDto.ShipFromCitCode,
                        rateShipmentDto.ShipToCitCode));
            }

            return serviceRate.TrrValue;

            #endregion
        }

        /// <summary>
        /// Busca una tarifa de coincidencia exacta para este artículo por esquema, modelo, tipo de ruta, ciudad origen y ciudad destino; opcional por proveedor. para la guia
        /// </summary>
        /// <param name="articleCode"></param>
        /// <param name="model"></param>
        /// <param name="schema"></param>
        /// <param name="rateWaybillDto"></param>
        /// <returns>Valor de la tarifa</returns>
        private float ObtenerTarifa(string articleCode, RatingModelDTO model, RatingSchemaDTO schema, RateWaybillDTO rateWaybillDto, string carrierUcrCode)
        {
            #region Conexion conEF

            //trServiceRates serviceRate;
            //var routeLegType = rateWaybillDto.RouteLegType;
            //using (var ratingEntities = new RatingEntities())
            //{
            //    serviceRate = (ratingEntities.trServiceRates.Where(p => p.TrrSchema_RsmCode == schema.Code &&
            //                                                            p.TrrNat == (int)RatesConstants.Nature.Negative &&
            //                                                            p.TrrRatingModel_RmoCode == model.Code &&
            //                                                            p.TrrArticle_ArtCode == articleCode &&
            //                                                            p.TrrShipFrom_CitCode ==
            //                                                            rateWaybillDto.ShipFromCityCitCode &&
            //                                                            p.TrrShipTo_CitCode == rateWaybillDto.ShipToCityCitCode &&
            //                                                            p.TrrDateFrom <= rateWaybillDto.Date &&
            //                                                            p.TrrDateTo >= rateWaybillDto.Date &&
            //                                                            p.TrrActiveState ==
            //                                                            (short)RatesConstants.State.Active &&
            //                                                            p.TrrRouteLegType == routeLegType &&
            //                                                            (p.TrrSupplier_UcrCode == carrierUcrCode ||
            //                                                             p.TrrSupplier_UcrCode == null)
            //      )).OrderByDescending(o => o.TrrSupplier_UcrCode).FirstOrDefault();
            //}

            //if (serviceRate == null || (Math.Abs(serviceRate.TrrValue - 0) < RatesConstants.MinDoubleValue))
            //{
            //    throw new ArgumentException(string.Format(Messages.error_ArticleNotFoundForSchemaModelRouteLegTypeFrom_CitCode, articleCode, schema.Code, model.Code, carrierUcrCode, routeLegType, rateWaybillDto.ShipFromCityCitCode, rateWaybillDto.ShipToCityCitCode));
            //}

            //return (float)(serviceRate != null ? serviceRate.TrrValue : 0);

            #endregion

            #region Tarifario en memoria

            var routeLegType = rateWaybillDto.RouteLegType;

            var serviceRate = ServiceRates.RateList.Where(p => p.TrrSchema_RsmCode == schema.Code &&
                                                               p.TrrNat == (int)RatesConstants.Nature.Negative &&
                                                               p.TrrRatingModel_RmoCode == model.Code &&
                                                               p.TrrArticle_ArtCode == articleCode &&
                                                               p.TrrShipFrom_CitCode ==
                                                               rateWaybillDto.ShipFromCityCitCode &&
                                                               p.TrrShipTo_CitCode == rateWaybillDto.ShipToCityCitCode &&
                                                               p.TrrDateFrom <= rateWaybillDto.Date &&
                                                               p.TrrDateTo >= rateWaybillDto.Date &&
                                                               p.TrrActiveState ==
                                                               (short)RatesConstants.State.Active &&
                                                               p.TrrRouteLegType == routeLegType &&
                                                               (p.TrrSupplier_UcrCode == carrierUcrCode ||
                                                                p.TrrSupplier_UcrCode == null)
                ).OrderByDescending(o => o.TrrSupplier_UcrCode).FirstOrDefault();

            if (serviceRate == null || (Math.Abs(serviceRate.TrrValue - 0) < RatesConstants.MinDoubleValue))
            {
                throw new ArgumentException(
                    string.Format(Messages.error_ArticleNotFoundForSchemaModelRouteLegTypeFrom_CitCode, articleCode,
                        schema.Code, model.Code, carrierUcrCode, routeLegType, rateWaybillDto.ShipFromCityCitCode,
                        rateWaybillDto.ShipToCityCitCode));
            }

            return serviceRate.TrrValue;

            #endregion
        }

        /// <summary>
        /// Busca una tarifa de coincidencia exacta para este artículo por esquema, modelo, tipo de ruta; opcional por proveedor. Debe estar vigente y activa. para la remesa.
        /// </summary>
        /// <param name="articleCode"></param>
        /// <param name="model"></param>
        /// <param name="schema"></param>
        /// <param name="rateShipmentDto"></param>
        /// <returns>Valor de la tarifa</returns>
        private float ObtenerParametro(string articleCode, RatingModelDTO model, RatingSchemaDTO schema, RateShipmentDTO rateShipmentDto, string carrierUcrCode)
        {
            #region Conexion conEF

            //trServiceRates serviceRate;
            //var routeLegType = rateShipmentDto.RouteLegType;
            //using (var ratingEntities = new RatingEntities())
            //{
            //    serviceRate = (ratingEntities.trServiceRates.Where(p => p.TrrSchema_RsmCode == schema.Code &&
            //                                                            p.TrrNat == (int)RatesConstants.Nature.Negative &&
            //                                                            p.TrrRatingModel_RmoCode == model.Code &&
            //                                                            p.TrrArticle_ArtCode == articleCode &&
            //                                                            p.TrrDateFrom <= rateShipmentDto.CreationDate &&
            //                                                            p.TrrDateTo >= rateShipmentDto.CreationDate &&
            //                                                            p.TrrActiveState ==
            //                                                            (short)RatesConstants.State.Active &&
            //                                                            p.TrrRouteLegType == routeLegType &&
            //                                                            (p.TrrSupplier_UcrCode == carrierUcrCode ||
            //                                                             p.TrrSupplier_UcrCode == null)
            //      )).OrderByDescending(o => o.TrrSupplier_UcrCode).FirstOrDefault();
            //}

            //if (serviceRate == null || (Math.Abs(serviceRate.TrrValue - 0) < RatesConstants.MinDoubleValue))
            //{
            //    throw new ArgumentException(string.Format(Messages.error_ArticleNotFoundForSchemaModelRouteLegType, articleCode, schema.Code, model.Code, carrierUcrCode, routeLegType));
            //}

            //return (float)(serviceRate != null ? serviceRate.TrrValue : 0);

            #endregion

            #region Tarifario en memoria

            var routeLegType = rateShipmentDto.RouteLegType;

            var serviceRate = ServiceRates.RateList.Where(p => p.TrrSchema_RsmCode == schema.Code &&
                                                               p.TrrNat == (int)RatesConstants.Nature.Negative &&
                                                               p.TrrRatingModel_RmoCode == model.Code &&
                                                               p.TrrArticle_ArtCode == articleCode &&
                                                               p.TrrDateFrom <= rateShipmentDto.CreationDate &&
                                                               p.TrrDateTo >= rateShipmentDto.CreationDate &&
                                                               p.TrrActiveState ==
                                                               (short)RatesConstants.State.Active &&
                                                               p.TrrRouteLegType == routeLegType &&
                                                               (p.TrrSupplier_UcrCode == carrierUcrCode ||
                                                                p.TrrSupplier_UcrCode == null)
                ).OrderByDescending(o => o.TrrSupplier_UcrCode).FirstOrDefault();

            if (serviceRate == null || (Math.Abs(serviceRate.TrrValue - 0) < RatesConstants.MinDoubleValue))
            {
                throw new ArgumentException(string.Format(Messages.error_ArticleNotFoundForSchemaModelRouteLegType,
                    articleCode, schema.Code, model.Code, carrierUcrCode, routeLegType));
            }

            return serviceRate.TrrValue;

            #endregion

        }

        /// <summary>
        /// Busca una tarifa de coincidencia exacta para este artículo por esquema, modelo, tipo de ruta; opcional por proveedor. Debe estar vigente y activa. para la guia.
        /// </summary>
        /// <param name="articleCode"></param>
        /// <param name="model"></param>
        /// <param name="schema"></param>
        /// <param name="rateWaybillDto"></param>
        /// <returns>Valor de la tarifa</returns>
        private float ObtenerParametro(string articleCode, RatingModelDTO model, RatingSchemaDTO schema, RateWaybillDTO rateWaybillDto, string carrierUcrCode)
        {
            #region Conexion conEF

            //trServiceRates serviceRate;
            //var routeLegType = rateWaybillDto.RouteLegType;
            //using (var ratingEntities = new RatingEntities())
            //{
            //    serviceRate = (ratingEntities.trServiceRates.Where(p => p.TrrSchema_RsmCode == schema.Code &&
            //                                                            p.TrrNat == (int)RatesConstants.Nature.Negative &&
            //                                                            p.TrrRatingModel_RmoCode == model.Code &&
            //                                                            p.TrrArticle_ArtCode == articleCode &&
            //                                                            p.TrrDateFrom <= rateWaybillDto.Date &&
            //                                                            p.TrrDateTo >= rateWaybillDto.Date &&
            //                                                            p.TrrActiveState ==
            //                                                            (short)RatesConstants.State.Active &&
            //                                                            p.TrrRouteLegType == routeLegType &&
            //                                                            (p.TrrSupplier_UcrCode == carrierUcrCode ||
            //                                                             p.TrrSupplier_UcrCode == null)
            //      )).OrderByDescending(o => o.TrrSupplier_UcrCode).FirstOrDefault();
            //}

            //if (serviceRate == null || (Math.Abs(serviceRate.TrrValue - 0) < RatesConstants.MinDoubleValue))
            //{
            //    throw new ArgumentException(string.Format(Messages.error_ArticleNotFoundForSchemaModelRouteLegType,
            //        articleCode, schema.Code, model.Code, carrierUcrCode, routeLegType));
            //}

            //return (float)(serviceRate != null ? serviceRate.TrrValue : 0);

            #endregion

            #region Tarifario en memoria

            var routeLegType = rateWaybillDto.RouteLegType;

            var serviceRate = ServiceRates.RateList.Where(p => p.TrrSchema_RsmCode == schema.Code &&
                                                               p.TrrNat == (int)RatesConstants.Nature.Negative &&
                                                               p.TrrRatingModel_RmoCode == model.Code &&
                                                               p.TrrArticle_ArtCode == articleCode &&
                                                               p.TrrDateFrom <= rateWaybillDto.Date &&
                                                               p.TrrDateTo >= rateWaybillDto.Date &&
                                                               p.TrrActiveState ==
                                                               (short)RatesConstants.State.Active &&
                                                               p.TrrRouteLegType == routeLegType &&
                                                               (p.TrrSupplier_UcrCode == carrierUcrCode ||
                                                                p.TrrSupplier_UcrCode == null)
                ).OrderByDescending(o => o.TrrSupplier_UcrCode).FirstOrDefault();

            if (serviceRate == null || (Math.Abs(serviceRate.TrrValue - 0) < RatesConstants.MinDoubleValue))
            {
                throw new ArgumentException(string.Format(Messages.error_ArticleNotFoundForSchemaModelRouteLegType,
                    articleCode, schema.Code, model.Code, carrierUcrCode, routeLegType));
            }

            return serviceRate.TrrValue;

            #endregion
        }

        #endregion
    }
}