﻿using System;
using System.Collections.Generic;
using System.Linq;
using Simplexity.RatingEngine.Constants;
using Simplexity.RatingEngine.DTOs;
using Simplexity.RatingEngine.EF;

namespace Simplexity.RatingEngine.Models.Suppla
  {
    /// <summary>
    /// Aplica: Cliente
    /// Modelo: Flete por m3: La tarifa establece un valor de flete teniendo en cuenta el minimo de ocupacion del vehiculo y el minimo m3 por despacho.
    /// El calculo esta dado por 1 despacho y se debe enviar un load con un shipment.
    /// </summary>
    public class T301 : OperationRate, IRating
      {
        public ResponseDTO Calculate(RateLoadDTO load, RatingSchemaDTO schema, RatingModelDTO model, List<RateLineDTO> rateLines)
          {
            #region 1. Load Objects

            LoadBaseProperties(schema, model);
						SetLoad(load);
						LoadShipment(load);

            #endregion

            #region 2. Find rate and parameters, run formula

						if (Shipment == null) return ResponseDTO;
						LoadRateSearchModelA(RatesArticles.Freight);
					
            // Vehicle capacity units
            var vehicleCapacityUnits = GetParameter(RatesArticleService.VehicularCapacityUnits, load.VtyCode);
            var percentajeMinVehicle = GetParameter(RatesArticleService.MinimumPercentajeOccupanceVehicleUnits);

            //Jair 17/06/2015: Se añandio la siguiente instruccion para obtener las unidades minimas por despacho
            var minUnByDispatch = GetParameter(RatesArticleService.MinUnitByDispatch);

            // Formula:
            var minimumOccupanceVehicle = percentajeMinVehicle * vehicleCapacityUnits;

            var billiableQuantity = Math.Max(Math.Max(minimumOccupanceVehicle, minUnByDispatch), Shipment.Quantity);

            var unitPrice = ServiceRate != null ? ServiceRate.TrrValue : 0;
            var extendedPrice = billiableQuantity * unitPrice;

            #endregion

            #region 3. Validate Freight

            if (model.NonZeroValueRequired == RatesConstants.Mandatory)
              {
	              if (Math.Abs(extendedPrice) > 0)
	              {
									LoadRateLineDefaultValues(load);
		              RateLineDTO.Quantity = billiableQuantity;
		              RateLineDTO.UnitPrice = unitPrice;
		              RateLineDTO.ExtendedPrice = extendedPrice;
		              RateLineDTO.UomCode = RatesUnitsOfMeasure.Unit;
		              ResponseDTO.RateLines.Add(RateLineDTO);
	              }
	              else
	              {
		              ResponseDTO.Message.TypeEnum = MessagesConstants.Types.Error;
		              ResponseDTO.Message.Message = string.Join("|", "No se calculo tarifa.");
	              }
              }
            else
              {
                if (Math.Abs(extendedPrice) > 0)
                  {
										LoadRateLineDefaultValues(load);
                    RateLineDTO.Quantity = billiableQuantity;
                    RateLineDTO.UnitPrice = unitPrice;
                    RateLineDTO.ExtendedPrice = extendedPrice;
                    RateLineDTO.UomCode = RatesUnitsOfMeasure.Unit;
                    ResponseDTO.RateLines.Add(RateLineDTO);
                  }
              }

            #endregion

            return ResponseDTO;
          }
      }
  }