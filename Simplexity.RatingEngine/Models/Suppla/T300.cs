﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Simplexity.RatingEngine.Constants;
using Simplexity.RatingEngine.DTOs;
using Simplexity.RatingEngine.EF;

namespace Simplexity.RatingEngine.Models.Suppla
{
	/// <summary>
	/// Aplica: Cliente
	/// Modelo: Flete por m3: La tarifa establece un valor de flete teniendo en cuenta el minimo de ocupacion del vehiculo y el minimo m3 por despacho.
	/// El calculo esta dado por 1 despacho y se debe enviar un load con un shipment.
	/// </summary>
	public class T300 : OperationRate, IRating
	{
		public ResponseDTO Calculate(RateLoadDTO load, RatingSchemaDTO schema, RatingModelDTO model, List<RateLineDTO> rateLines)
		{
			#region 1. Load Objects

			LoadBaseProperties(schema, model);
			SetLoad(load);
			LoadShipment(load);

			#endregion

			#region 2. Find rate and parameters, run formula
			
			if (Shipment == null) return ResponseDTO;
			LoadRateSearchModelA(RatesArticles.Freight);

			if (ServiceRate == null)
			{
				//Se cambio para acomular mensajes y no lanzar excepcion cada vez
				//throw new InvalidOperationException(string.Format("No se encontro tarifa para el articulo ({0})", RatesArticles.Freight));
				ResponseDTO.Message.TypeEnum = MessagesConstants.Types.Error;
				ResponseDTO.Message.Message = string.Join("|",
					string.Format("No se encontró tarifa para el articulo ({0})", RatesArticles.Freight));
			}
			// parameters

			// Vehicle capacity m3
			var minOccupancyVehicleM3 = GetParameter(RatesArticleService.MinimumOccupanceVehicleM3);
			var percentajeMinVehicle = GetParameter(RatesArticleService.MinimumPercentajeOccupanceVehicleM3);

			// formula:
			var minimumOccupanceVehicle = float.Parse(String.Format("{0:0.##}", (percentajeMinVehicle * load.VehCapacityM3)));

			var billiableQuantity = Math.Max(Math.Max(minimumOccupanceVehicle, minOccupancyVehicleM3), float.Parse(Shipment.ShiTotalVolumeM3.ToString()));
			var unitPrice = ServiceRate != null ? ServiceRate.TrrValue : 0;
			var extendedPrice = float.Parse(String.Format("{0:0.##}", (billiableQuantity * unitPrice)));

			#endregion

			#region 3. Calculate Freight

			if (model.NonZeroValueRequired == RatesConstants.Mandatory)
			{
				if (Math.Abs(extendedPrice) > 0)
				{
					LoadRateLineDefaultValues(load);
					RateLineDTO.Quantity = billiableQuantity;
					RateLineDTO.UnitPrice = unitPrice;
					RateLineDTO.ExtendedPrice = extendedPrice;
					RateLineDTO.UomCode = RatesUnitsOfMeasure.CubicMeter;
					ResponseDTO.RateLines.Add(RateLineDTO);
				}
				else
				{
					//throw new InvalidOperationException("No se calculo tarifa.");
					ResponseDTO.Message.TypeEnum = MessagesConstants.Types.Error;
					ResponseDTO.Message.Message = string.Join("|", "No se calculo tarifa.");
				}
			}
			else
			{
				if (Math.Abs(extendedPrice) > 0)
				{
					LoadRateLineDefaultValues(load);
					RateLineDTO.Quantity = billiableQuantity;
					RateLineDTO.UnitPrice = unitPrice;
					RateLineDTO.ExtendedPrice = extendedPrice;
					RateLineDTO.UomCode = RatesUnitsOfMeasure.CubicMeter;
					ResponseDTO.RateLines.Add(RateLineDTO);
				}
			}

			#endregion

			return ResponseDTO;
		}
	}
}