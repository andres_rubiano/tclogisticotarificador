﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Simplexity.RatingEngine.Constants;
using Simplexity.RatingEngine.DTOs;
using Simplexity.RatingEngine.EF;

namespace Simplexity.RatingEngine.Models.Suppla
{
	/// <summary>
	/// Aplica: Cliente / Proveedor
	/// Modelo: Nacional por tipo de vehículo: En este modelo se tiene en cuenta el tipo de vehículo (turbo, sencillo, etc...)
	/// El calculo esta dado por 1 viaje para el caso del proveedor o 1 remesa para el caso del cliente.
	/// </summary>
	public class T304 : OperationRate, IRating
	{
		public ResponseDTO Calculate(RateLoadDTO load, RatingSchemaDTO schema, RatingModelDTO model, List<RateLineDTO> rateLines)
		{
			#region 1. Load Objects

			LoadBaseProperties(schema, model);
			SetLoad(load);
			LoadShipment(load);

			#endregion

			#region 2. Find rate and parameters, run formula

			if (Shipment == null) return ResponseDTO;
			LoadRateSearchModelC(RatesArticles.Freight);
			
			// Es fijo por tipo de vehículo:
			var billiableQuantity = 1;
			var unitPrice = ServiceRate != null ? ServiceRate.TrrValue : 0;
			var extendedPrice = billiableQuantity * unitPrice;

			#endregion

			#region 3. Validate Freight

			if (model.NonZeroValueRequired == RatesConstants.Mandatory)
			{
				if (Math.Abs(extendedPrice) > 0)
				{
					LoadRateLineDefaultValues(load);
					RateLineDTO.Quantity = billiableQuantity;
					RateLineDTO.UnitPrice = unitPrice;
					RateLineDTO.ExtendedPrice = extendedPrice;
					RateLineDTO.UomCode = RatesUnitsOfMeasure.Dispatch;
					ResponseDTO.RateLines.Add(RateLineDTO);
				}
				else
				{
					ResponseDTO.Message.TypeEnum = MessagesConstants.Types.Error;
					ResponseDTO.Message.Message = string.Join("|", "No se calculo tarifa.");
				}
			}
			else
			{
				if (Math.Abs(extendedPrice) > 0)
				{
					LoadRateLineDefaultValues(load);
					RateLineDTO.Quantity = billiableQuantity;
					RateLineDTO.UnitPrice = unitPrice;
					RateLineDTO.ExtendedPrice = extendedPrice;
					RateLineDTO.UomCode = RatesUnitsOfMeasure.Dispatch;
					ResponseDTO.RateLines.Add(RateLineDTO);
				}
			}

			#endregion

			return ResponseDTO;
		}
	}
}