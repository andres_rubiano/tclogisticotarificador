﻿using System;
using System.Linq;
using System.Threading;
using Simplexity.RatingEngine.Resources;
using Simplexity.RatingService.Repository;

namespace Simplexity.RatingEngine
{
    public static class Utilities
    {
        public static void StartRates()
        {
            if (ServiceRates.UpdatingRates)
                return;

            ServiceRates.UpdatingRates = true;

            try
            {
                ServiceRates.RateList = RateRepository.GetRates();
            }
            catch (Exception ex)
            {
                ServiceRates.Error = true;
                ServiceRates.Message = ex.Message;
            }

            ServiceRates.UpdatingRates = false;
        }

        public static void ValidateUpdatingRates()
        {
            // si se estan cargando tarifas, no se puede procesar:

            if (ServiceRates.UpdatingRates)
                throw new ArgumentException(Messages.Info_ServiceRatesUpdating);

            // si no hay tarifas se inicia el recargue:
            if (!ServiceRates.RateList.Any())
            {
                StartRates();
                throw new ArgumentException(Messages.Info_ServiceRatesUpdating);
            }
            
        }
    }
}
