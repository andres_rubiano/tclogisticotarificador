﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Remoting;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using Dapper;
using Simplexity.RatingEngine.Constants;
using Simplexity.RatingEngine.DTOs;
using Simplexity.RatingEngine.EF;
using Simplexity.RatingEngine.Models;
using Simplexity.RatingEngine.Resources;

namespace Simplexity.RatingEngine
{
    public class RatingService
    {
        /// <summary>
        /// Esta funcion es parte de la interfaz expuesta por RatingService
        /// Calcula las tarifas para un load a partir de un Schema
        /// Rota por los modelos, pasandoles el DTO de Load completo
        /// </summary>
        /// <param name="load"></param>
        /// <param name="schemaCode"></param>
        /// <param name="batchNumber"></param>
        /// <param name="dateSchema"></param>
        /// <returns></returns>
        private DateTime _dateSchema;

        public ResponseDTO CalculateCustomerRates(RateLoadDTO load, string schemaCode, DateTime dateSchema)
        {
            #region Validations Input Data

            ValidateLoad(load);
            ValidateSchema(schemaCode);

            //si la fecha viene nula o vacia, se debe tomar la fecha de hoy 
            _dateSchema = ValidateDateSchema(dateSchema);

            #endregion

            /*ResponseDTO para todos los modelos*/
            var responseDTO = new ResponseDTO();
            /*ReposeDTO para el modelo que se esté calculando en el ciclo*/

            #region Load Schema and Models

            var schemaDTO = FindSchema(schemaCode);

            if (schemaDTO == null)
            {
                responseDTO.Message.TypeEnum = MessagesConstants.Types.Error;
                responseDTO.Message.Message = Messages.error_ValidSchemaNotFound;
                return responseDTO;
            }

            schemaDTO.Models = FindModelsBySchema(schemaDTO.Code);

            if (schemaDTO.Models.Count <= 0)
            {
                responseDTO.Message.TypeEnum = MessagesConstants.Types.Error;
                responseDTO.Message.Message = Messages.warning_ModelsNotFound;
                return responseDTO;
            }

            #endregion

            //Se tarifica con cada modelo
            foreach (var model in schemaDTO.Models)
            {
                #region Seleccion del modelo

                var iRating = (IRating)Spring.GetObject(model.Code, RatesConstants.SpringContext);
                var ratingEngine = new RatingEngine(iRating);

                #endregion

                var responseModelDTO = ratingEngine.Calculate(load, schemaDTO, model, responseDTO.RateLines);

                switch (responseDTO.Message.TypeEnum)
                {
                    case MessagesConstants.Types.Information:
                        responseDTO.RateLines.AddRange(responseModelDTO.RateLines);
                        continue;

                    case MessagesConstants.Types.Error:
                    case MessagesConstants.Types.Warning:
                        if (model.AlternateModels.Count == 0)
                        {
                            // Como es obligatorio, busca los alternativos y como no existen, devuelva el error correspondiente
                            if (responseModelDTO.Message.TransactionNumber == ErrorConstants.MandatoryRateNotFound)
                            {
                                responseDTO.Message.TypeEnum = MessagesConstants.Types.Error;
                                responseDTO.Message.Message = string.Format(Messages.error_MandatoryRateNotFound,
                                                                            model.Code);
                                ;
                                return responseDTO;
                            }

                            // como no es mandatorio y no hay modelos alternativos, no pasa nada, siga con el siguiente modelo
                            continue;
                        }

                        bool flag = false;
                        /*ReponseDTO solo para los modelos alternativos*/

                        foreach (var alternateModel in model.AlternateModels)
                        {
                            iRating = (IRating)Spring.GetObject(alternateModel.Code, RatesConstants.SpringContext);
                            ratingEngine = new RatingEngine(iRating);

                            var responseAlternateModelDTO = ratingEngine.Calculate(load, schemaDTO, model, responseDTO.RateLines);

                            if (responseAlternateModelDTO.Message.TypeEnum == MessagesConstants.Types.Information)
                            {
                                flag = true;
                                responseDTO.RateLines.AddRange(responseAlternateModelDTO.RateLines);
                                break;
                            }
                        }

                        if (!flag &&
                            responseModelDTO.Message.TransactionNumber == ErrorConstants.MandatoryRateNotFound)
                        {
                            responseDTO.Message.TypeEnum = MessagesConstants.Types.Error;
                            responseDTO.Message.Message = Messages.error_NoMandatoryAltCantFailed;
                            responseDTO.Message.TransactionNumber = model.Code;
                            return responseDTO;
                        }

                        break;
                }
            }

            return responseDTO;
        }

        public ResponseDTO CalculateSupplierRates(RateLoadDTO load, string schemaCode, DateTime dateSchema)
        {
            #region Validations Input Data
            
            ValidateLoad(load);

            if (string.IsNullOrEmpty(schemaCode))
                schemaCode = GetSchemaForLoad(load.CarrierUcrCode, load.BopCode, load.LoaCreationDate);

            ValidateSchema(schemaCode);

            // solo pq aja:
            if (load.LoaCreationDate == null || load.LoaCreationDate == DateTime.MinValue)
                load.LoaCreationDate = DateTime.Now;
            
            //si la fecha viene nula o vacia, se debe tomar la fecha de hoy 
            _dateSchema = ValidateDateSchema(dateSchema);

            #endregion

            /*ResponseDTO para todos los modelos*/
            var responseDTO = new ResponseDTO();
            /*ReposeDTO para el modelo que se esté calculando en el ciclo*/

            #region Load Schema and Models

            var schemaDTO = FindSchema(schemaCode);

            if (schemaDTO == null)
            {
                responseDTO.Message.SetInError(string.Format("El esquema [{0}] no existe.", schemaCode));
                return responseDTO;
            }

            schemaDTO.Models = FindModelsBySchema(schemaDTO.Code);

            if (schemaDTO.Models.Count <= 0)
            {
                responseDTO.Message.TypeEnum = MessagesConstants.Types.Error;
                responseDTO.Message.Message = Messages.warning_ModelsNotFound;
                return responseDTO;
            }

            #endregion

            //Se tarifica con cada modelo
            foreach (var model in schemaDTO.Models)
            {
                #region Seleccion del modelo

                var iRating = (IRating)Spring.GetObject(model.Code, RatesConstants.SpringContext);
                var ratingEngine = new RatingEngine(iRating);

                #endregion

                var responseModelDTO = ratingEngine.Calculate(load, schemaDTO, model, responseDTO.RateLines);

                switch (responseDTO.Message.TypeEnum)
                {
                    case MessagesConstants.Types.Information:
                        responseDTO.RateLines.AddRange(responseModelDTO.RateLines);
                        continue;

                    case MessagesConstants.Types.Error:
                    case MessagesConstants.Types.Warning:
                        if (model.AlternateModels.Count == 0)
                        {
                            // Como es obligatorio, busca los alternativos y como no existen, devuelva el error correspondiente
                            if (responseModelDTO.Message.TransactionNumber == ErrorConstants.MandatoryRateNotFound)
                            {
                                responseDTO.Message.TypeEnum = MessagesConstants.Types.Error;
                                responseDTO.Message.Message = string.Format(Messages.error_MandatoryRateNotFound,
                                                                            model.Code);
                                ;
                                return responseDTO;
                            }

                            // como no es mandatorio y no hay modelos alternativos, no pasa nada, siga con el siguiente modelo
                            continue;
                        }

                        bool flag = false;
                        /*ReponseDTO solo para los modelos alternativos*/

                        foreach (var alternateModel in model.AlternateModels)
                        {
                            iRating = (IRating)Spring.GetObject(alternateModel.Code, RatesConstants.SpringContext);
                            ratingEngine = new RatingEngine(iRating);

                            var responseAlternateModelDTO = ratingEngine.Calculate(load, schemaDTO, model, responseDTO.RateLines);

                            if (responseAlternateModelDTO.Message.TypeEnum == MessagesConstants.Types.Information)
                            {
                                flag = true;
                                responseDTO.RateLines.AddRange(responseAlternateModelDTO.RateLines);
                                break;
                            }
                        }

                        if (!flag &&
                            responseModelDTO.Message.TransactionNumber == ErrorConstants.MandatoryRateNotFound)
                        {
                            responseDTO.Message.TypeEnum = MessagesConstants.Types.Error;
                            responseDTO.Message.Message = Messages.error_NoMandatoryAltCantFailed;
                            responseDTO.Message.TransactionNumber = model.Code;
                            return responseDTO;
                        }

                        break;
                }
            }

            return responseDTO;
        }

        /// <summary>
        /// con esta funcion verifico si existe el esquema en base de datos
        /// </summary>
        /// <param name="schemaCode"></param>
        /// <returns></returns>
        /// 
        public RatingSchemaDTO FindSchema(string schemaCode)
        {
            RatingSchemaDTO schemaDTO;
            using (var ratingEntities = new RatingEntities())
            {
                var trRatingSchema = (from s in ratingEntities.trRatingSchemas
                                      where s.RsmCode == schemaCode
                                      select s).FirstOrDefault();

                if (trRatingSchema != null)
                {
                    schemaDTO = new RatingSchemaDTO
                                    {
                                        Code = trRatingSchema.RsmCode,
                                        Name = trRatingSchema.RsmName,
                                        Models = new List<RatingModelDTO>()
                                    };
                }
                else
                {
                    schemaDTO = null;
                }


                return schemaDTO;
            }
        }

        public List<RatingModelDTO> FindModelsBySchema(string schemaCode)
        {
            var modelsListDTO = new List<RatingModelDTO>();

            using (var ratingEntities = new RatingEntities())
            {
                List<trRatingModels> models = (from rs in ratingEntities.trRatingSchemas
                                               join sm in ratingEntities.trSchemaModels on rs.RsmCode equals sm.RmsSchema_RsmCode
                                               join rm in ratingEntities.trRatingModels on sm.RmsModel_RmoCode equals rm.RmoCode
                                               where rs.RsmCode == schemaCode
                                               orderby sm.RmsPos
                                               select rm).ToList();


                foreach (var model in models)
                {
                    var modelDTO = new RatingModelDTO
                                       {
                                           Code = model.RmoCode,
                                           DefaultArticle = model.RmoDefault_ArtCode,
                                           Name = model.RmoName,
                                           AlternateModels = new List<RatingModelsAlternateDTO>()
                                       };

                    modelDTO.AlternateModels = FindAlternateModelsByModel(modelDTO.Code);
                    modelsListDTO.Add(modelDTO);
                }
            }

            return modelsListDTO;
        }

        public List<RatingModelsAlternateDTO> FindAlternateModelsByModel(string modelCode)
        {
            var modelsAlternateListDTO = new List<RatingModelsAlternateDTO>();

            using (var ratingEntities = new RatingEntities())
            {
                List<trSchemaModelsAlternate> alternateModels = (from rm in ratingEntities.trRatingModels
                                                                 join sm in ratingEntities.trSchemaModels on rm.RmoCode
                                                                     equals
                                                                     sm.RmsModel_RmoCode
                                                                 join ma in ratingEntities.trSchemaModelsAlternate on
                                                                     sm.RmsCode
                                                                     equals ma.Sma_RmsCode
                                                                 where rm.RmoCode == modelCode
                                                                 orderby ma.SmaPosition
                                                                 select ma).ToList();

                foreach (var alternateModel in alternateModels)
                {
                    var alternateModelDTO = new RatingModelsAlternateDTO
                                                {
                                                    Code = alternateModel.SmaModel_RmoCode,
                                                    CodeSchema = alternateModel.Sma_RmsCode
                                                };
                    modelsAlternateListDTO.Add(alternateModelDTO);
                }
            }

            return modelsAlternateListDTO;
        }

        #region Private

        private void ValidateLoad(RateLoadDTO load)
        {
            if (load == null)
            {
                throw new ArgumentException(Messages.warning_RateNotFound);
            }
        }

        private void ValidateSchema(string schemaCode)
        {
            if (string.IsNullOrEmpty(schemaCode))
            {
                throw new ArgumentException("No existe un esquema valido para el objeto a tarificar.");
            }
        }

        private DateTime ValidateDateSchema(DateTime dateSchema)
        {
            if (dateSchema == DateTime.MinValue)
            {
                dateSchema = DateTime.Now;
            }
            return dateSchema;
        }

        #endregion

        /// <summary>
        /// Este metodo vence las tarifas que vienen en "tarifasId", 
        /// </summary>
        /// <param name="tarifasId"></param>
        /// <param name="fechaVencimiento"></param>
        /// <returns></returns>
        public MessageDTO VencerTarifas(IEnumerable<int> tarifasId, DateTime fechaVencimiento, string usuario)
        {
            var mensajeRespuesta = new MessageDTO();

            // Validaciones:
            // id deben existir

            IEnumerable<trServiceRates> tarifas;

            using (var ratingEntities = new RatingEntities())
            {
                tarifas = from t in ratingEntities.trServiceRates
                          where tarifasId.Contains(t.TrrId)
                          select t;

                var recibidas = tarifasId.Count();
                var encontradas = tarifas.Count();

                if (encontradas != recibidas)
                {
                    mensajeRespuesta.TypeEnum = MessagesConstants.Types.Error;
                    mensajeRespuesta.Message = string.Format(Messages.error_TarifasEncontradasDiferenteTarifasRecibidas, encontradas, recibidas);
                    return mensajeRespuesta;
                }

                // deben estar vigentes

                var vigentes = tarifas.Count(t => t.TrrDateTo >= DateTime.Now);
                if (vigentes <= 0)
                {
                    mensajeRespuesta.TypeEnum = MessagesConstants.Types.Error;
                    mensajeRespuesta.Message = string.Format(Messages.NoHayTarifasVigentes);
                    return mensajeRespuesta;
                }

                // Persistir en base de datos:
                foreach (var tarifa in tarifas)
                {
                    tarifa.TrrDateTo = fechaVencimiento;
                    tarifa.TrrLastUpdate = DateTime.Now;
                    tarifa.TrrLastUpdate_UsrCode = usuario;
                }

                // registrar traza
                var json =
                    new JavaScriptSerializer().Serialize(
                        new { IdTarifas = tarifasId, FechaVencimiento = fechaVencimiento.ToString() });

                ratingEntities.TrzServiceRates.Add(new TrzServiceRates()
                                                       {
                                                           TzrUser = usuario,
                                                           TzrDate = DateTime.Now,
                                                           TzrOperation = "U",
                                                           TzrSource = "VencerTarifas",
                                                           TzrSourceReference = string.Empty,
                                                           TzrJsonObject = json
                                                       });

                ratingEntities.SaveChanges();
            }
            return mensajeRespuesta;
        }

        public MessageDTO VencerTarifasPorBatch(string codigoBatch, DateTime fechaVencimiento, string usuario)
        {
            var mensajeRespuesta = new MessageDTO();

            using (var ratingEntities = new RatingEntities())
            {
                // el numero de batch debe existir

                var batch = from b in ratingEntities.trRateBatches
                            where b.TbaCode.Equals(codigoBatch)
                            select b;

                if (!batch.Any())
                {
                    mensajeRespuesta.TypeEnum = MessagesConstants.Types.Error;
                    mensajeRespuesta.Message = string.Format(Messages.BatchDeTarifasNoExiste, codigoBatch);
                    return mensajeRespuesta;
                }

                var tarifasPorBatch = from tb in ratingEntities.trServiceRates
                                      where tb.TrrBatchNumber.Equals(codigoBatch)
                                      select tb;

                // deben estar vigentes

                var vigentes = tarifasPorBatch.Count(t => t.TrrDateTo >= DateTime.Now);
                if (vigentes <= 0)
                {
                    mensajeRespuesta.TypeEnum = MessagesConstants.Types.Error;
                    mensajeRespuesta.Message = string.Format(Messages.NoHayTarifasVigentes);
                    return mensajeRespuesta;
                }

                // Persistir en base de datos:
                foreach (var tarifa in tarifasPorBatch)
                {
                    tarifa.TrrDateTo = fechaVencimiento;
                    tarifa.TrrLastUpdate = DateTime.Now;
                    tarifa.TrrLastUpdate_UsrCode = usuario;
                }

                // registrar traza
                var jsonBatch = new JavaScriptSerializer().Serialize(new { Batch = codigoBatch, FechaVencimiento = fechaVencimiento.ToString() });

                ratingEntities.TrzServiceRates.Add(new TrzServiceRates()
                                                       {
                                                           TzrUser = usuario,
                                                           TzrDate = DateTime.Now,
                                                           TzrOperation = "U",
                                                           TzrSource = "VencerTarifasPorBatch",
                                                           TzrSourceReference = codigoBatch,
                                                           TzrJsonObject = jsonBatch
                                                       });


                ratingEntities.SaveChanges();
            }
            return mensajeRespuesta;
        }

        public double ObtenerValorPesoVolumen(string codigoEsquema, string clienteCodigo, string tipoRuta)
        {
            var valor = 0.0;

            using (var ratingEntities = new RatingEntities())
            {
                valor =
                    ratingEntities.trServiceRates.Where(
                        t =>
                            t.TrrArticle_ArtCode == RatesArticles.WeightVolumeRatio &&
                            t.TrrSchema_RsmCode == codigoEsquema && t.TrrCustomer_UcrCode == clienteCodigo &&
                            t.TrrActiveState == (short) RatesConstants.State.Active &&
                            (t.TrrRouteLegType == tipoRuta || string.IsNullOrEmpty(t.TrrRouteLegType)))
                        .OrderByDescending(o => o.TrrRouteLegType).ThenByDescending(o => o.TrrId)
                        .Select(v => v.TrrValue)
                        .FirstOrDefault();
            }


            return valor;
        }

        public double ObtenerValorTarifa(FiltroTarifaSemiautomaticaDto filtroTarifa)
        {
            var valor = 0.0;
            var naturaleza = filtroTarifa.Naturaleza;

            if (naturaleza == 1)
            {
                using (var ratingEntities = new RatingEntities())
                {
                    var query =
                      ratingEntities.trServiceRates.Where(
                        t =>
                          t.TrrNat == filtroTarifa.Naturaleza &&
                          t.TrrSchema_RsmCode == filtroTarifa.EsquemaCodigo &&
                          t.TrrRatingModel_RmoCode == filtroTarifa.ModeloTarifa &&
                          t.TrrCustomer_UcrCode == filtroTarifa.TerceroCodigo &&
                          t.TrrArticle_ArtCode == filtroTarifa.ArticuloCodigo &&
                          t.TrrArticle_UomCode == filtroTarifa.UnidadDeMedidaCodigo);

                    if (filtroTarifa.CiudadOrigenCodigo.Any())
                        query = query.Where(t => t.TrrShipFrom_CitCode == filtroTarifa.CiudadOrigenCodigo);
                    if (filtroTarifa.CiudadDestinoCodigo.Any())
                        query = query.Where(t => t.TrrShipTo_CitCode == filtroTarifa.CiudadDestinoCodigo);

                    valor = query.Select(v => v.TrrValue).FirstOrDefault();
                }

            }
            else
            {
                using (var ratingEntities = new RatingEntities())
                {
                    var query =
                      ratingEntities.trServiceRates.Where(
                        t =>
                          t.TrrNat == filtroTarifa.Naturaleza &&
                          t.TrrSchema_RsmCode == filtroTarifa.EsquemaCodigo &&
                          t.TrrRatingModel_RmoCode == filtroTarifa.ModeloTarifa &&
                          t.TrrSupplier_UcrCode == filtroTarifa.TerceroCodigo &&
                          t.TrrArticle_ArtCode == filtroTarifa.ArticuloCodigo &&
                          t.TrrArticle_UomCode == filtroTarifa.UnidadDeMedidaCodigo);

                    if (filtroTarifa.CiudadOrigenCodigo.Any())
                        query = query.Where(t => t.TrrShipFrom_CitCode == filtroTarifa.CiudadOrigenCodigo);
                    if (filtroTarifa.CiudadDestinoCodigo.Any())
                        query = query.Where(t => t.TrrShipTo_CitCode == filtroTarifa.CiudadDestinoCodigo);

                    valor = query.Select(v => v.TrrValue).FirstOrDefault();
                }
            }

            return valor;

        }

        public MessageDTO RecargarTarifario()
        {
            var mensajeRespuesta = new MessageDTO();

            Task.Run(() => Utilities.StartRates());

            mensajeRespuesta.Message = string.Format(Messages.Info_ServiceRateStart);
            return mensajeRespuesta;
        }

        public MessageDTO ConsultarEstadoTarifario()
        {
            var mensajeRespuesta = new MessageDTO();

            if (ServiceRates.Error)
            {
                mensajeRespuesta.Message = "Error presentado: " + ServiceRates.Message;
                return mensajeRespuesta;
            }


            if (ServiceRates.UpdatingRates)
            {
                mensajeRespuesta.Message = Messages.Info_ServiceRatesUpdating;
                return mensajeRespuesta;
            }

            if (!ServiceRates.RateList.Any())
            {
                mensajeRespuesta.Message = "No hay tarifas disponibles.";
                return mensajeRespuesta;
            }

            mensajeRespuesta.Message = "Tarifario actualizado, tarifas cargadas: " + ServiceRates.RateList.Count();

            return mensajeRespuesta;
        }

        public string GetSchemaForLoad(string supplierCode, string bopCode, DateTime? loadDate)
        {
            if (loadDate == null || loadDate == DateTime.MinValue)
                loadDate = DateTime.Now;

                string schemaCode;
                const string sql = "Select Top 1 ScoSchema_RsmCode "
                                   + "From   trSchemaConfiguration "
                                   + "Where  ScoType = 2 "
                                   + "       And (ScoSupplier_UcrCode = @supplierCode "
                                   + "            Or Coalesce(ScoSupplier_UcrCode, '') = '') "
                                   + "       And ScoLoaBusinessOperationType_BopCode = @bopCode "
                                   + "       And @LoadDate Between ScoValidFrom And ScoValidTo "
                                   + "Order By ScoId Desc ";
                using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["Simplexity.Rating"].ToString()))
                {
                    schemaCode = db.Query<string>(sql, new { supplierCode, bopCode, loadDate }).FirstOrDefault();
                }
                return schemaCode;
        }
    }
}