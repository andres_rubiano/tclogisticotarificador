﻿using System.Collections.Generic;

namespace Simplexity.RatingEngine.Constants
{
	public class MessagesConstants
	{
		public enum Types
		{
			None = 0,
			Information = 1,
			Warning = 2,
			Error = 3
		}
	}

	public static class RatesArticleService
	{
		public static string MinimumOccupanceVehicleM3
		{
			get { return "MinOccupVehM3"; }
		}

		public static string MinimumPercentajeOccupanceVehicleM3
		{
			get { return "MinPerOccuVehM3"; }
		}

		public static string VehicularCapacityUnits
		{
			get { return "VehCapacityUnit"; }
		}

		public static string MinimumPercentajeOccupanceVehicleUnits
		{
			get { return "MinPerOccuVehUn"; }
		}

		public static string MinUnitByDispatch
		{
			get { return "minUnByDispatch"; }
		}

		public static string CostHandlingDecValue
		{
			get { return "CostDecValue"; }
		}

		public static string MinCostHandlingUnit
		{
			get { return "MinCostHandUni"; }
		}
	}

	public static class RatesArticles
	{
		public static string Freight
		{
			get { return "FLETE"; }
		}

		public static string Unload
		{
			get { return "DESCA"; }
		}

		public static string Loading
		{
			get { return "CARGU"; }
		}

		public static string DefaultArticle
		{
			get { return "FLETE"; }
		}

		public static string FreightAdditionalWeight
		{
			get { return "FreightAddWeigh"; }
		}

		public static string Insurance
		{
			get { return "MCostDeclV"; }
		}

		public static string FreightDiscount
		{
			get { return "FreightDiscount"; }
		}

		public static string InvoiceDiscount
		{
			get { return "InvoiceDiscount"; }
		}

		public static string MinimunInsurance
		{
			get { return "SEGURO MINIMO"; }
		}

		public static string MinWeightByUnit
		{
			get { return "MinWeightByUnit"; }
		}

		public static string MinWeightByUniqueUnit
		{
			get { return "MinWeightUUnit"; }
		}

		public static string MinimunAdditionalKg
		{
			get { return "KgMinAdd"; }
		}

		public static string RealWeightAdjustment
		{
			get { return "AdjRealWeight"; }
		}

		public static string WeightVolumeRatio
		{
			get { return "WeightVolRatio"; }
		}

		public static string MinFreight
		{
			get { return "MinFreight"; }
		}

		public static string MinimunHandlingUnitCost
		{
			get { return "MCostMagnU"; }
		}

		public static string InitWeight
		{
			get { return "InitWeight"; }
		}

		public static string HandlingCostOverDeclaredValue
		{
			get { return "MCostMagnU"; }
		}

		public static string MaxUnitWeight
		{
			get { return "MaxUnitWeight"; }
		}

		public static string MinWeightByFirstUnit
		{
			get { return "MinWeightFirstU"; }
		}

		public static string MinWeightByAdditionalUnit
		{
			get { return "MinWeightAddU"; }
		}

		public static string MHandCostbyDispatch
		{
			get { return "MinCostByDisp"; }
		}

		public static string MinHandlingCostByDispatch
		{
			get { return "MCostByDispatch"; }
		}

		public static string MinWeightByDispatch
		{
			get { return "MWByDispatch"; }
		}

		public static string SecurityService
		{
			get { return "SecurityService"; }
		}

		public static string InsuranceOnFreight
		{
			get { return "insuOnFreight"; }
		}

        public static string PesoMinCajaKg
        {
            get { return "PesoMinCajaKg"; }
        }

        public static string CostoMinDesp
        {
            get { return "CostoMinDesp"; }
        }

        public static string CostoMinUnid
        {
            get { return "CostoMinUnid"; }
        }

        public static string CostoKgIni
        {
            get { return "CostoKgIni"; }
        }

        public static string CostoKgAdd
        {
            get { return "CostoKgAdd"; }
        }

        public static string CostoManjeCaj
        {
            get { return "CostoManejCaj"; }
        }
        public static string CostHandlingDecValue
        {
            get { return "CostDecValue"; }
        }
	}

	public static class RatesUnitsOfMeasure
	{
		public static string MinFreight
		{
			get { return "FMIN"; }
		}

		public static string Percentage
		{
			get { return "PORC"; }
		}

		public static string Ton
		{
			get { return "TON"; }
		}

		public static string Kilogram
		{
			get { return "KG"; }
		}

		public static string KilogramAdd
		{
			get { return "KGADD"; }
		}

		public static string CubicMeter
		{
			get { return "M3"; }
		}

		public static string Box
		{
			get { return "CAJA"; }
		}

		public static string Dispatch
		{
			get { return "DESPA"; }
		}

		public static string Container
		{
			get { return "CONT"; }
		}

		public static string Unit
		{
			get { return "UN"; }
		}
        public static string Viaje
        {
            get { return "VIA"; }
        }

        public static string Fijo
        {
            get { return "FIJO"; }
        }
	}

	public static class RatesRoutes
	{
		public static string Urban
		{
			get { return "U"; }
		}

		public static string National
		{
			get { return "N"; }
		}

		public static string Reexpedition
		{
			get { return "R"; }
		}
	}

	public static class RatesConstants
	{
		public static int Mandatory
		{
			get { return 1; }
		}

		public static double MinDoubleValue
		{
			get { return 0.00001; }
		}


		public const string SpringContext = "ServiceRatingContext";

		public enum State
		{
			Active = 1,
			Inactive = 2,
			Expired = 3
		}

		public enum Nature
		{
			Negative = -1,
			Positive = 1
		}
	}

	public static class ErrorConstants
	{
		public const string MandatoryRateNotFound = "0001";
		public const string NoMandatoryRateNotFound = "0002";
	}
}