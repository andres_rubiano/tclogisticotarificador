﻿using System;
using System.Collections.Generic;
using Simplexity.RatingEngine.DTOs;
using Simplexity.RatingEngine.Models;

namespace Simplexity.RatingEngine
{
  public class RatingEngine
  {
    private IRating _rating;

    public RatingEngine(IRating rating)
    {
      if (rating == null)
        throw new ArgumentNullException(Resources.Messages.warning_RateNotFound);
      _rating = rating;
    }


    public ResponseDTO Calculate(RateLoadDTO load, RatingSchemaDTO schema, RatingModelDTO model, List<RateLineDTO> rateLines)
    {
      return _rating.Calculate(load, schema, model, rateLines);
    }
  }
}