﻿using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Simplexity.RatingEngine;

namespace Simplexity.RatingService.Repository
{
    public static class RateRepository
    {
        public static List<Rate> GetRates()
        {
            var cxString = ConfigurationManager.ConnectionStrings["Simplexity.Rating"].ToString();

            #region consulta SQL:

            const string sqlQuery = "Select TrrId, "
                                    + "       TrrSchema_RsmCode, "
                                    + "       TrrRatingModel_RmoCode, "
                                    + "       TrrBatchNumber, "
                                    + "       TrrCustomer_UcrCode, "
                                    + "       TrrSupplier_UcrCode, "
                                    + "       Trr_SubCode, "
                                    + "       TrrNat, "
                                    + "       TrrShipFrom_SubCode, "
                                    + "       TrrShipFrom_UcrCode, "
                                    + "       TrrShipFrom_UloCode, "
                                    + "       TrrShipFromZONE1_ZonCode, "
                                    + "       TrrShipFromZONE2_ZonCode, "
                                    + "       TrrShipFromZONE3_ZonCode, "
                                    + "       TrrShipFrom_CitCode, "
                                    + "       TrrShipTo_SubCode, "
                                    + "       TrrShipTo_UcrCode, "
                                    + "       TrrShipTo_UloCode, "
                                    + "       TrrShipToZONE1_ZonCode, "
                                    + "       TrrShipToZONE2_ZonCode, "
                                    + "       TrrShipToZONE3_ZonCode, "
                                    + "       TrrShipTo_CitCode, "
                                    + "       TrrRoute_RouCode, "
                                    + "       TrrTruck_VehCode, "
                                    + "       TrrTruck_VtyCode, "
                                    + "       TrrTransitMode_TrmCode, "
                                    + "       TrrProduct_ProCode, "
                                    + "       TrrProductType_PtyCode, "
                                    + "       TrrRange_UOMCode, "
                                    + "       TrrRangeMin, "
                                    + "       TrrRangeMax, "
                                    + "       TrrArticle_ArtCode, "
                                    + "       TrrArticle_UomCode, "
                                    + "       TrrValue, "
                                    + "       TrrCurrencyCode, "
                                    + "       TrrDefaultProvider_UcrCode, "
                                    + "       TrrSignature, "
                                    + "       TrrDateFrom, "
                                    + "       TrrDateTo, "
                                    + "       TrrActiveState, "
                                    + "       TrrCreationDate, "
                                    + "       TrrCreation_UsrCode, "
                                    + "       TrrLastUpdate, "
                                    + "       TrrLastUpdate_UsrCode, "
                                    + "       TrrRouteLegType, "
                                    + "       TrrCode, "
                                    + "       Trr_LoadInTrip, "
                                    + "       TrrUnloadingType_UltCode, "
                                    + "       TrrCrewType, "
                                    + "       TrrLogisticTypeto_LotCode, "
                                    + "       TrrUOMOficial_UofCode "
                                    + "From   dbo.trServiceRates "
                                    + "Where  Getdate() Between TrrDateFrom And TrrDateTo "
                                    + "       And TrrRatingModel_RmoCode In ('T315', 'T316', 'T317') "
                                    + "Order By TrrId Desc";

            #endregion

            List<Rate> tarifas;

            using (var sqlConn = new SqlConnection(cxString))
            {
                sqlConn.Open();

                tarifas = sqlConn.Query<Rate>(sqlQuery).ToList();
            }

            return tarifas;
        }
    }
}