﻿using System;
using System.Configuration;
using System.IO;
using System.Reflection;
using System.Text;
using System.Web;

namespace Simplexity.RatingEngine
{
  public static class WriteLog
  {
    public static void WriteInFile(StringBuilder log, string reference)
    {
      // Log activo:
      var logWrite = ConfigurationManager.AppSettings["LogRates"];
      if (!logWrite.ToUpper().Equals("TRUE"))
        return;

      // carpeta de log

      string path = ConfigurationManager.AppSettings["PathLogRates"] + @"LogRates\";

      try
      {
        if (!Directory.Exists(path))
          Directory.CreateDirectory(path);
      }
      catch (Exception)
      {
        return;
      }

      reference += "_" + ObtenerFechaActualSerializada();

      using (FileStream stream = new FileStream(path + reference + ".txt", FileMode.Create))
      {
        var byteData = Encoding.ASCII.GetBytes(log.ToString());
        stream.Write(byteData, 0, byteData.Length);
        stream.Close();
      }
    }

    private static string ObtenerFechaActualSerializada()
      {
      var dia = "0" + DateTime.Now.Day;
      var mes = "0" + DateTime.Now.Month;
      var hora = "0" + DateTime.Now.Hour;
      var minuto = "0" + DateTime.Now.Minute;
      var segundo = "0" + DateTime.Now.Second;

      return DateTime.Now.Year + mes.Substring(mes.Length - 2, 2) +
             dia.Substring(dia.Length - 2, 2) + "_" + hora.Substring(hora.Length - 2, 2) +
             minuto.Substring(minuto.Length - 2, 2) + segundo.Substring(segundo.Length - 2, 2);
      }
  }
}