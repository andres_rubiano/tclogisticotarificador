﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Xml;

namespace Simplexity.RatingEngine.Settings
{
  class ModelHandler: IConfigurationSectionHandler
  {
    private const string connectionXpath = "//Models[@enabled='true']";
    private const string configDefaultArticle   = "defaultArticle";
    private const string configConnectionString = "connectionString";
    private const string configNameModel = "nameModel";
    private const string configSchema = "schema";

    public object Create(object parent, object configContext, XmlNode section)
    {
      Dictionary<string,string> models = new Dictionary<string, string>();
      //List<string> models = new List<string>();
      XmlElement root = ((XmlElement)section);

      string masterDefaultArticle = string.Empty;
      string masterConnectionString = string.Empty;

      if (null != root.Attributes[configDefaultArticle])
      {
        masterDefaultArticle = root.Attributes[configDefaultArticle].Value;
      }
      if (null != root.Attributes[configConnectionString])
      {
        masterConnectionString = root.Attributes[configConnectionString].Value;
      }

      XmlNodeList nodeListModels = root.SelectNodes(connectionXpath);

      if (nodeListModels.Count <= 0)
      {
        throw new ArgumentException("No matches found in config file with Xpath : " + connectionXpath);
      }
      foreach (XmlNode nodeModel in nodeListModels)
      {

        //Keep the variable scope correct, for each specific smtp server
        string nameModel = string.Empty;       
        string schema = string.Empty;

        string currentXpath = string.Empty;
        try
        {
          currentXpath = configNameModel;
          nameModel = nodeModel.Attributes[currentXpath].Value;

          currentXpath = configSchema;
          schema = nodeModel.Attributes[currentXpath].Value;          
        }
        catch (ArgumentException argex)
        {
          throw argex;
        }

        models.Add(nameModel,schema);

      }
      return models; 
    }
  }
}
