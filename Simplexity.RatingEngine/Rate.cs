﻿using System;

namespace Simplexity.RatingEngine
{
    public class Rate
    {
        public int TrrId { get; set; }
        public string TrrSchema_RsmCode { get; set; }
        public string TrrRatingModel_RmoCode { get; set; }
        public string TrrBatchNumber { get; set; }
        public string TrrCustomer_UcrCode { get; set; }
        public string TrrSupplier_UcrCode { get; set; }
        public string Trr_SubCode { get; set; }
        public int TrrNat { get; set; }
        public string TrrShipFrom_SubCode { get; set; }
        public string TrrShipFrom_UcrCode { get; set; }
        public string TrrShipFrom_UloCode { get; set; }
        public string TrrShipFromZONE1_ZonCode { get; set; }
        public string TrrShipFromZONE2_ZonCode { get; set; }
        public string TrrShipFromZONE3_ZonCode { get; set; }
        public string TrrShipFrom_CitCode { get; set; }
        public string TrrShipTo_SubCode { get; set; }
        public string TrrShipTo_UcrCode { get; set; }
        public string TrrShipTo_UloCode { get; set; }
        public string TrrShipToZONE1_ZonCode { get; set; }
        public string TrrShipToZONE2_ZonCode { get; set; }
        public string TrrShipToZONE3_ZonCode { get; set; }
        public string TrrShipTo_CitCode { get; set; }
        public string TrrRoute_RouCode { get; set; }
        public string TrrTruck_VehCode { get; set; }
        public string TrrTruck_VtyCode { get; set; }
        public string TrrTransitMode_TrmCode { get; set; }
        public string TrrProduct_ProCode { get; set; }
        public string TrrProductType_PtyCode { get; set; }
        public string TrrRange_UOMCode { get; set; }
        public float TrrRangeMin { get; set; }
        public float TrrRangeMax { get; set; }
        public string TrrArticle_ArtCode { get; set; }
        public string TrrArticle_UomCode { get; set; }
        public float TrrValue { get; set; }
        public string TrrCurrencyCode { get; set; }
        public string TrrDefaultProvider_UcrCode { get; set; }
        public string TrrSignature { get; set; }
        public DateTime TrrDateFrom { get; set; }
        public DateTime TrrDateTo { get; set; }
        public int TrrActiveState { get; set; }
        public DateTime TrrCreationDate { get; set; }
        public string TrrCreation_UsrCode { get; set; }
        public DateTime TrrLastUpdate { get; set; }
        public string TrrLastUpdate_UsrCode { get; set; }
        public string TrrRouteLegType { get; set; }
        public Guid TrrCode { get; set; }
        public bool Trr_LoadInTrip { get; set; }
        public string TrrUnloadingType_UltCode { get; set; }
        public string TrrCrewType { get; set; }
        public string TrrLogisticTypeto_LotCode { get; set; }
        public string TrrUOMOficial_UofCode { get; set; }

    }
}
