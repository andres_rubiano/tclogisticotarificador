﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BusinessRules.Main.DTOs.ServiceRating;
using Simplexity.RatingEngine.DTOs;
using Simplexity.RatingEngine.Models;
using Spring.Context;
using Spring.Context.Support;

namespace SpringTest
{
  public partial class Form1 : Form
  {
    public Form1()
    {
      InitializeComponent();
    }

    private void button1_Click(object sender, EventArgs e)
    {
      IApplicationContext ctx = ContextRegistry.GetContext();
      var a = (IRating)ctx.GetObject("Test");

      a.Calculate(new RateLoadDTO(), new RatingSchemaDTO(), new RatingModelDTO(), new ResponseDTO());
    }
  }
}
