﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.ServiceModel;
using System.Threading.Tasks;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using Simplexity.RatingEngine;
using Simplexity.RatingEngine.DTOs;

namespace Simplexity.RatingService
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    [ExceptionShielding("WCF Exception Shielding")]
    public class RatingService : BaseService, IRatingService
    {
        public ResponseDTO CalculateCustomerRates(RateLoadDTO load, string schemaCode, DateTime dateSchema)
        {
            var responseDTO = new ResponseDTO();

            try
            {
                var ratingService = new RatingEngine.RatingService();
                responseDTO = ratingService.CalculateCustomerRates(load, schemaCode, dateSchema);
            }
            catch (InvalidOperationException ex)
            {
                responseDTO.Message = HandleException(ex);
            }
            catch (Exception ex)
            {
                responseDTO.Message = HandleException(ex);
            }

            return responseDTO;
        }

        public ResponseDTO CalculateSupplierRates(RateLoadDTO load, string schemaCode, DateTime dateSchema)
        {
            var responseDTO = new ResponseDTO();

            try
            {
                var ratingService = new RatingEngine.RatingService();
                responseDTO = ratingService.CalculateSupplierRates(load, schemaCode, dateSchema);
            }
            catch (Exception ex)
            {
                responseDTO.Message = HandleException(ex);
            }

            return responseDTO;
        }

        public RatingSchemaDTO FindSchema(string schemaCode)
        {
            return new RatingEngine.RatingService().FindSchema(schemaCode);
        }

        public List<RatingModelDTO> FindModelsBySchema(string schemaCode)
        {
            return new RatingEngine.RatingService().FindModelsBySchema(schemaCode);
        }

        public List<RatingModelsAlternateDTO> FindAlternateModelsByModel(string modelCode)
        {
            return new RatingEngine.RatingService().FindAlternateModelsByModel(modelCode);
        }

        public MessageDTO VencerTarifas(IEnumerable<int> tarifasId, DateTime fechaVencimiento, string usuario)
        {
            MessageDTO mensajeRespuestaDto;

            try
            {
                var ratingService = new RatingEngine.RatingService();
                mensajeRespuestaDto = ratingService.VencerTarifas(tarifasId, fechaVencimiento, usuario);
            }
            catch (Exception ex)
            {
                mensajeRespuestaDto = HandleException(ex);
            }

            return mensajeRespuestaDto;
        }

        public MessageDTO VencerTarifasPorBatch(string codigoBatch, DateTime fechaVencimiento, string usuario)
        {
            MessageDTO mensajeRespuestaDto;

            try
            {
                var ratingService = new RatingEngine.RatingService();
                mensajeRespuestaDto = ratingService.VencerTarifasPorBatch(codigoBatch, fechaVencimiento, usuario);
            }
            catch (Exception ex)
            {
                mensajeRespuestaDto = HandleException(ex);
            }

            return mensajeRespuestaDto;
        }

        public double ObtenerValorPesoVolumen(string codigoEsquema, string clienteCodigo, string tipoRuta)
        {
            var valor = 0.0;
            try
            {
                var ratingService = new RatingEngine.RatingService();
                valor = ratingService.ObtenerValorPesoVolumen(codigoEsquema, clienteCodigo, tipoRuta);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }

            return valor;
        }

        public double ObtenerValorTarifa(FiltroTarifaSemiautomaticaDto filtroTarifa)
        {
            var valor = 0.0;
            try
            {
                var ratingService = new RatingEngine.RatingService();
                valor = ratingService.ObtenerValorTarifa(filtroTarifa);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }

            return valor;

        }

        public MessageDTO RecargarTarifario()
        {
            MessageDTO mensajeRespuestaDto;

            try
            {
                var ratingService = new RatingEngine.RatingService();
                mensajeRespuestaDto = ratingService.RecargarTarifario();
            }
            catch (Exception ex)
            {
                mensajeRespuestaDto = HandleException(ex);
            }

            return mensajeRespuestaDto;
        }

        public MessageDTO ConsultarEstadoTarifario()
        {
            MessageDTO mensajeRespuestaDto;

            try
            {
                var ratingService = new RatingEngine.RatingService();
                mensajeRespuestaDto = ratingService.ConsultarEstadoTarifario();
            }
            catch (Exception ex)
            {
                mensajeRespuestaDto = HandleException(ex);
            }

            return mensajeRespuestaDto;
        }
    }
}