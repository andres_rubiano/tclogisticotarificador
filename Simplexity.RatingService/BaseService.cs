﻿using System;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.EnterpriseLibrary.Logging.Configuration;
using Simplexity.RatingEngine.Constants;
using Simplexity.RatingEngine.DTOs;

namespace Simplexity.RatingService
{
  public class BaseService
  {
    protected MessageDTO HandleException(Exception ex)
    {
      var message = new MessageDTO { TypeEnum = MessagesConstants.Types.Error };

      IConfigurationSource configurationSource = ConfigurationSourceFactory.Create();

      if (configurationSource.GetSection(DatabaseSettings.SectionName) != null)
      {
        var databaseFactory = new DatabaseProviderFactory();
        DatabaseFactory.SetDatabaseProviderFactory(databaseFactory, false);
      }

      if (configurationSource.GetSection(LoggingSettings.SectionName) != null)
      {
        var loggerFactory = new LogWriterFactory(configurationSource);
        Logger.SetLogWriter(loggerFactory.Create(), false);
      }

      var exceptionPolicyFactory = new ExceptionPolicyFactory(configurationSource);
      var exManager = exceptionPolicyFactory.CreateManager(); 
      ExceptionPolicy.SetExceptionManager(exManager, false);

      if (ex is InvalidOperationException)
      {
        exManager.HandleException(ex, "WCF InvalidOperation");

        message.Message = ex.Message;
        message.TypeEnum = MessagesConstants.Types.Error;
      }
      else if (ex is ArgumentException)
      {
        exManager.HandleException(ex, "WCF ArgumentException");

        message.Message = ex.Message;
        message.TypeEnum = MessagesConstants.Types.Error;
      }
      else
      {
        Exception newException;
        bool booResult = exManager.HandleException(ex, "WCF Exception Shielding", out newException);
        if (!booResult)
        {
          message.Message = "Error cannot be handled";
          return message;
        }
        message.Message = newException.Message;
        message.TypeEnum = MessagesConstants.Types.Error;
      }
      return message;
    }
  }
}