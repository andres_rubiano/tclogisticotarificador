﻿using System;
using System.Runtime.Serialization;

namespace Simplexity.RatingService.ErrorHandler
{
  [DataContract]
  public class ServiceFaultException
  {
    [DataMember]
    public Guid FaultID { get; set; }

    [DataMember]
    public string FaultMessage { get; set; }
  }
}