﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
using Simplexity.RatingEngine.DTOs;

namespace Simplexity.RatingService
{
    [ServiceContract]
    public interface IRatingService
    {
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ResponseDTO CalculateCustomerRates(RateLoadDTO load, string schemaCode, DateTime dateSchema);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ResponseDTO CalculateSupplierRates(RateLoadDTO load, string schemaCode, DateTime dateSchema);

        [OperationContract]
        RatingSchemaDTO FindSchema(string schemaCode);

        [OperationContract]
        List<RatingModelDTO> FindModelsBySchema(string schemaCode);

        [OperationContract]
        List<RatingModelsAlternateDTO> FindAlternateModelsByModel(string modelCode);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        MessageDTO VencerTarifas(IEnumerable<int> tarifasId, DateTime fechaVencimiento, string usuario);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        MessageDTO VencerTarifasPorBatch(string codigoBatch, DateTime fechaVencimiento, string usuario);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        Double ObtenerValorPesoVolumen(string codigoEsquema, string clienteCodigo, string tipoRuta);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        double ObtenerValorTarifa(FiltroTarifaSemiautomaticaDto filtroTarifa);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        MessageDTO RecargarTarifario();

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        MessageDTO ConsultarEstadoTarifario();
    }
}