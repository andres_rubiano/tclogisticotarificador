﻿using Infraestructure.Data.EF;

namespace Infraestructure.Data
{
    public interface ILoadRepository : IGenericRepository<opLoads>
    {
    }
}
