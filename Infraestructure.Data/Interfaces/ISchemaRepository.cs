﻿using Infraestructure.Data.EF;

namespace Infraestructure.Data
{
    public interface ISchemaRepository : IGenericRepository<trRatingSchemas>
    {
    }
}
