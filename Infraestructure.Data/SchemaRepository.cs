﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Infraestructure.Data.EF;

namespace Infraestructure.Data
{
    public class SchemaRepository : GenericRepository<RatingsDBContainer, trRatingSchemas>, ISchemaRepository
    {
        public trRatingSchemas GetLoadByCode_includeShipments(string schemaCode, DateTime dateSchema)
        {
            //return SelectWhere(p => p.LoaNumber == LoaNumber).First();
            using (var dataBase = new RatingsDBContainer())
            {
                var result = (from s in dataBase.trRatingSchemas
                                      join p in dataBase.trRatingSchemaPeriods on s.RsmCode equals p.RspSchema_RsmCode
                                      where s.RsmCode == schemaCode &&
                                      dateSchema > p.RspDateFrom && dateSchema <= p.RspDateTo
                                      select s).FirstOrDefault();
                return result;
            }
        }
    }
}
