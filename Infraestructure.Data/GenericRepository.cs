﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using System.Data;


namespace Infraestructure.Data
{
    public abstract class GenericRepository<C, T> : IGenericRepository<T>, IDisposable
        where C : ObjectContext, new()
         where T : class 
    {
        private C dataBase = new C();

        public C Context
        {
            get { return dataBase; }
            set { dataBase = value; }
        }

        public virtual IEnumerable<T> SelectAll()
        {
            IEnumerable<T> query = dataBase.CreateObjectSet<T>();
            return query;
        }

        public virtual T SelectOne(Expression<Func<T, bool>> predicate)
        {
            T entity = dataBase.CreateObjectSet<T>().Where(predicate).FirstOrDefault<T>();
            return entity;
        }

        public IEnumerable<T> SelectWhere(Expression<Func<T, bool>> predicate)
        {
            IQueryable<T> query = dataBase.CreateObjectSet<T>().Where(predicate);
            return query;
        }

        public virtual void Add(T entity)
        {
            string entityType = entity.GetType().ToString();
            int start = entityType.LastIndexOf(".") + 1;
            string entityName = entityType.Substring(start, entityType.Length - start);

            dataBase.AddObject(String.Format("{0}.{1}", dataBase.DefaultContainerName, entityName), entity);
        }

        public virtual void Delete(T entity)
        {
            dataBase.DeleteObject(entity);
        }

        public virtual void Edit(T entity)
        {
            dataBase.ObjectStateManager.ChangeObjectState(entity, EntityState.Modified);
        }

        public virtual void Save()
        {
            dataBase.SaveChanges();
        }

        public  void Dispose()
        {
            if (dataBase != null)
            {
                dataBase.Dispose();
            }
            GC.SuppressFinalize(this);
        }


    }
}
