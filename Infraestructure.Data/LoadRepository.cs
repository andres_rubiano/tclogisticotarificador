﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Infraestructure.Data.EF;

namespace Infraestructure.Data
{
    public class LoadRepository : GenericRepository<LoadsDBContainer, opLoads>, ILoadRepository
    {
        public opLoads GetLoadByCode_includeShipments(string LoaNumber)
        {
            //return SelectWhere(p => p.LoaNumber == LoaNumber).First();
            using (var dataBase = new LoadsDBContainer())
            {
                var result = dataBase.opLoads.Include("opShipments").FirstOrDefault(a => a.LoaNumber == LoaNumber);
                return result;
            }
        }
    }
}
