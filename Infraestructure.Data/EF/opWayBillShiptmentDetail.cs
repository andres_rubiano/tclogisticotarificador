//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Infraestructure.Data.EF
{
    using System;
    using System.Collections.Generic;
    
    public partial class opWayBillShiptmentDetail
    {
        public int WsdId { get; set; }
        public string WsdShiptment_ShiNumber { get; set; }
        public string WsdWayBill_WayNumber { get; set; }
        public System.DateTime WsdDate { get; set; }
        public Nullable<int> WsdSincro1 { get; set; }
        public Nullable<int> WsdSincro2 { get; set; }
    
        public virtual opShipments opShipments { get; set; }
        public virtual opWaybills opWaybills { get; set; }
    }
}
