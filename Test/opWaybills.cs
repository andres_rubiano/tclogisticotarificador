//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Test
{
    using System;
    using System.Collections.Generic;
    
    public partial class opWaybills
    {
        public int WayId { get; set; }
        public string WayNumber { get; set; }
        public System.DateTime WayDate { get; set; }
        public string WayType_WtyCode { get; set; }
        public string WayDispatchNumber { get; set; }
        public Nullable<System.DateTime> WayDispatchDate { get; set; }
        public Nullable<System.DateTime> WayDispatchedDate { get; set; }
        public string WayCustomer_UcrCode { get; set; }
        public string WayCustomer_UcrName { get; set; }
        public string WayCustomer_UcrIdentification { get; set; }
        public string WaySubsidiary_SubCode { get; set; }
        public string WayCompany_ComCode { get; set; }
        public Nullable<System.DateTime> WayPickupFromDatetime { get; set; }
        public Nullable<System.DateTime> WayPickupToDatetime { get; set; }
        public Nullable<System.DateTime> WayDeliveryFromDatetime { get; set; }
        public Nullable<System.DateTime> WayDeliveryToDatetime { get; set; }
        public string WayShipFromCity_CitCode { get; set; }
        public string WayShipFromAddress { get; set; }
        public string WayShipFromLocation_UloCode { get; set; }
        public string WayShipFromContact { get; set; }
        public string WayShipFromContact_UcoCode { get; set; }
        public string WayShipFrom_UcrCode { get; set; }
        public string WayShipFrom_UcrName { get; set; }
        public string WayShipFrom_UcrIdentification { get; set; }
        public string WayShipFrom_UcrIdentificationType { get; set; }
        public string WayShipFromPhone { get; set; }
        public string WayShipFromEmail { get; set; }
        public string WayShipToCity_CitCode { get; set; }
        public string WayShipToAddress { get; set; }
        public string WayShipToLocation_UloCode { get; set; }
        public string WayShipToContact { get; set; }
        public string WayShipToContact_UcoCode { get; set; }
        public string WayShipTo_UcrCode { get; set; }
        public string WayShipTo_UcrName { get; set; }
        public string WayShipTo_UcrIdentification { get; set; }
        public string WayShipTo_UcrIdentificationType { get; set; }
        public string WayShipToPhone { get; set; }
        public string WayShipToEmail { get; set; }
        public int WayQuantity { get; set; }
        public double WayTotalWeightKg { get; set; }
        public double WayTotalVolumeM3 { get; set; }
        public double WayEquivalentWeightKg { get; set; }
        public double WayCargoDeclaredValue { get; set; }
        public double WayCargoCurrencyCode { get; set; }
        public double WayFreightValue { get; set; }
        public double WayInsuranceValue { get; set; }
        public string WayProduct_ProCode { get; set; }
        public string WayUOM_UomCode { get; set; }
        public short WayOperationalStatus { get; set; }
        public short WayInvoicingStatus { get; set; }
        public Nullable<System.DateTime> WayDeliveryETA { get; set; }
        public string WayCustomerMemo { get; set; }
        public string WayShipperMemo { get; set; }
        public string WayRecipientMemo { get; set; }
        public string WayReference { get; set; }
        public string WayAditionalReference1 { get; set; }
        public string WayAditionalReference2 { get; set; }
        public string WayRoute_RouCode { get; set; }
        public string WaySalesOrganitation { get; set; }
        public string WayBusinessAdviser { get; set; }
        public string WayCreationUsrCode { get; set; }
        public Nullable<System.DateTime> WayCreationDate { get; set; }
        public Nullable<short> WayExportedToERP { get; set; }
        public string WayExportedToERPDescription { get; set; }
        public Nullable<System.DateTime> WayAnulationDate { get; set; }
        public string WayAnulationUsrCode { get; set; }
        public Nullable<System.DateTime> WayReceiptReportDate { get; set; }
        public string WayReceiptName { get; set; }
        public Nullable<System.DateTime> WayDestinationArrivalDate { get; set; }
        public Nullable<System.DateTime> WayDestinationDepartureDate { get; set; }
        public Nullable<System.DateTime> WayCloseDate { get; set; }
        public string WayInvoiceNumber { get; set; }
        public Nullable<int> WaySincro1 { get; set; }
        public Nullable<int> WaySincro2 { get; set; }
        public Nullable<short> WayTimeTraceProcessedFlag { get; set; }
        public byte[] Timestamp { get; set; }
        public string WayPickup_PreNumber { get; set; }
        public string WayCurrent_UloCode { get; set; }
        public string WayCurrent_SubCode { get; set; }
        public string WayCurrent_LoaNumber { get; set; }
        public string WayCurrent_ShiNumber { get; set; }
        public Nullable<int> WayCurrentHandlingEvent_EventId { get; set; }
        public string WayBusinessOperationType_BopCode { get; set; }
        public string WayHasDeliveryAppointment { get; set; }
        public string WayExternalReference { get; set; }
        public Nullable<short> WayDeliveryAttemps { get; set; }
        public string WayCustomerBusinessLine_UcrCode { get; set; }
        public string WayAditionalReference3 { get; set; }
        public string WayRouteType_WrtCode { get; set; }
        public string WayCurrent_SerNumber { get; set; }
        public Nullable<double> WayQuantityDelivered { get; set; }
        public Nullable<double> WayTotalWeightKgDelivered { get; set; }
        public string WayDeliveryComment { get; set; }
    
        public virtual opLoads opLoads { get; set; }
        public virtual opShipments opShipments { get; set; }
    }
}
