﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Linq;
using Simplexity.RatingEngine.Constants;
using Simplexity.RatingEngine.DTOs;

namespace Test
{
  public partial class Form1 : Form
  {
    public Form1()
    {
      InitializeComponent();
    }

    private void btnRate_Click(object sender, EventArgs e)
    {

      var responseDto = new ResponseDTO();

      using (var entity = new TestEntities())
      {
        //se carga el rateloaddto con datos del load q llegue (en el servicio debe llegar lleno no importa donde se llame pero lo llenan sea en json o lo q sea)
        var load = entity.opLoads.FirstOrDefault(loads => loads.LoaNumber == txtLoad.Text);
        var shipments = entity.opShipments.Where(shi => shi.ShiLoad_LoaNumber == txtLoad.Text).ToList();
        if (load != null)
        {
          var rl = new RateLoadDTO()
            {
              LoaNumber = load.LoaNumber,
              Truck = load.LoaTruck_VehCode,
              Trailer = load.LoaTrailer_VehCode,
              DriverUcrCode = load.LoaPrimaryDriver_UCRCode,
              LoaFromCitCode = load.LoaFrom_CitCode,
              LoaToCitCode = load.LoaTo_CitCode,
              LoaToZoneCode = load.LoaZone_ZonCode,
              RouCode = load.LoaRouteCode,
              WeightKg = load.LoaTotalWeightKg,
              VcnCode = load.LoaConfiguration_VcnCode,
              CarrierUcrCode = load.LoaCarrier_UCRCode,
              LoaFromSubCode = load.LoaSubsidiary_SubCode,
              ComCode = load.LoaCompany_ComCode,
							Shipments = new List<RateShipmentDTO>()
            };
          foreach (var shipment in shipments)
          {
            var waybills = entity.opWaybills.Where(way => way.WayCurrent_ShiNumber == shipment.ShiNumber).ToList();
            var rateWaybillDtos = waybills.Select(waybill => new RateWaybillDTO
              {
                CustomerUcrCode = waybill.WayCustomer_UcrCode,
                ShipFromCityCitCode = waybill.WayShipFromCity_CitCode,
                ShipToCityCitCode = waybill.WayShipToCity_CitCode,
                RouteLegType = "CP",
                Date = waybill.WayDate,
                TotalWeightKg = waybill.WayTotalWeightKg,
                TotalVolumeM3 = waybill.WayTotalVolumeM3,
                Quantity = waybill.WayQuantity,
                Number = waybill.WayNumber
                ,WayBusinessOperationTypeBopCode = waybill.WayBusinessOperationType_BopCode
                ,WayCustomerBusinessLineUcrCode = waybill.WayCustomerBusinessLine_UcrCode
              }).ToList();

            rl.Shipments.Add(new RateShipmentDTO
              {
                ShiNumber = shipment.ShiNumber,
                ShtCode = shipment.ShiShipmentType_ShtCode,
                CustomerUcrCode = shipment.ShiCustomer_UcrCode,
                ShipFromSubCode = shipment.ShiSubsidiary_SubCode,
                SubCode = shipment.ShiSubsidiary_SubCode,
                ComCode = shipment.ShiCompany_ComCode,
                //ProductTypeCode = logisticType,
                ShipFromCitCode = shipment.ShiShipFromCity_CitCode,
                ShipFromUloCode = shipment.ShiShipFromLocation_UloCode,
                ShipFromUcrCode = shipment.ShiShipFrom_UcrCode,
                ShipToCitCode = shipment.ShiShipToCity_CitCode,
                ShipToUloCode = shipment.ShiShipToLocation_UloCode,
                ShipToUcrCode = shipment.ShiShipTo_UcrCode,
                WeightKg = shipment.ShiTotalWeightKg,
                Quantity = shipment.ShiQuantity,
                UomCode = shipment.ShiUOM_UomCode,
                ProCode = shipment.ShiProduct_ProCode,
                QuantityShipped = shipment.ShiQuantityShipped,
                WeightKgShipped = shipment.ShiTotalWeightKgShipped,
                QuantityDelivered = shipment.ShiQuantityDelivered,
                WeightKgDelivered = shipment.ShiTotalWeightKgDelivered,
                BopCode = shipment.ShiBusinessOperationType_BopCode,
                Incoterm = shipment.ShiIncoterms_IncCode,
                RouCode = shipment.ShiRoute_RouCode,
                ShipToSubCode = shipment.ShiShipTo_SubCode,
                DispatchDate = shipment.ShiDispatchDate ?? DateTime.Now
                ,Waybills = rateWaybillDtos
              });
          
          
          }

          var service = new RatingReference.RatingServiceClient();


					//var bop = rl.Shipments.FirstOrDefault().Waybills.FirstOrDefault().WayBusinessOperationTypeBopCode;
					//var ucr = rl.Shipments.FirstOrDefault().Waybills.FirstOrDefault().WayCustomerBusinessLineUcrCode;    
	        var bop = "DESTER";
	        var ucr = load.LoaAdministrator_UcrCode;
          //Obtengo el schema  a partir de la configuración
          //1.0 obtiene una configuración válida a partir del bopCode y que tenga fechas válidas
          var configuration = entity.trSchemaConfiguration.FirstOrDefault(p => p.ScoLoaBusinessOperationType_BopCode == bop
            && p.ScoSupplier_UcrCode == ucr
            && p.ScoValidFrom <= DateTime.Now
            && p.ScoValidTo >= DateTime.Now);

					if (configuration == null)
					{
						MessageBox.Show("MALA CONFIGURACION");
						return;
					}


          //Ejecuto el rating
          responseDto = service.CalculateSupplierRates(rl,configuration.ScoSchema_RsmCode,DateTime.Now);
        }


        //Validar que la tarificacion fue exitosa
          if (responseDto.Message.TypeEnum != MessagesConstants.Types.Information)
          {
            MessageBox.Show(responseDto.Message.Message);
          }

          //Si el resultado es satisfactorio (hay lineas, y las requeridas están)
        dataGridView1.DataSource = responseDto.RateLines;
      }
    }

  }
}
