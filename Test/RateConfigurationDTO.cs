﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Test
{
  public class RateConfigurationDTO
  {
    public string BopCode { get; set; }
    public DateTime ValidFrom { get; set; }
    public DateTime ValidTo { get; set; }
    public string SchemaCode { get; set; }
    public string BatchCode { get; set; }
    public string CreationUserCode { get; set; }
    public DateTime? CreationDate { get; set; }
    public string UpdatedUserCode { get; set; }
    public DateTime? UpdatedDate { get; set; }
  }
}
