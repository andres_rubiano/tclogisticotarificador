﻿namespace Test
{
  partial class Form1
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.lbModelCode = new System.Windows.Forms.Label();
      this.lbUomCode = new System.Windows.Forms.Label();
      this.lbQuantity = new System.Windows.Forms.Label();
      this.lbUnitPrice = new System.Windows.Forms.Label();
      this.lbExtendedPrice = new System.Windows.Forms.Label();
      this.lbSchema = new System.Windows.Forms.Label();
      this.lbRateId = new System.Windows.Forms.Label();
      this.label8 = new System.Windows.Forms.Label();
      this.txtLoad = new System.Windows.Forms.TextBox();
      this.btnRate = new System.Windows.Forms.Button();
      this.dataGridView1 = new System.Windows.Forms.DataGridView();
      ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
      this.SuspendLayout();
      // 
      // lbModelCode
      // 
      this.lbModelCode.AutoSize = true;
      this.lbModelCode.Location = new System.Drawing.Point(129, 132);
      this.lbModelCode.Name = "lbModelCode";
      this.lbModelCode.Size = new System.Drawing.Size(0, 13);
      this.lbModelCode.TabIndex = 2;
      // 
      // lbUomCode
      // 
      this.lbUomCode.AutoSize = true;
      this.lbUomCode.Location = new System.Drawing.Point(195, 132);
      this.lbUomCode.Name = "lbUomCode";
      this.lbUomCode.Size = new System.Drawing.Size(0, 13);
      this.lbUomCode.TabIndex = 2;
      // 
      // lbQuantity
      // 
      this.lbQuantity.AutoSize = true;
      this.lbQuantity.Location = new System.Drawing.Point(291, 132);
      this.lbQuantity.Name = "lbQuantity";
      this.lbQuantity.Size = new System.Drawing.Size(0, 13);
      this.lbQuantity.TabIndex = 2;
      // 
      // lbUnitPrice
      // 
      this.lbUnitPrice.AutoSize = true;
      this.lbUnitPrice.Location = new System.Drawing.Point(379, 132);
      this.lbUnitPrice.Name = "lbUnitPrice";
      this.lbUnitPrice.Size = new System.Drawing.Size(0, 13);
      this.lbUnitPrice.TabIndex = 2;
      // 
      // lbExtendedPrice
      // 
      this.lbExtendedPrice.AutoSize = true;
      this.lbExtendedPrice.Location = new System.Drawing.Point(471, 132);
      this.lbExtendedPrice.Name = "lbExtendedPrice";
      this.lbExtendedPrice.Size = new System.Drawing.Size(0, 13);
      this.lbExtendedPrice.TabIndex = 2;
      // 
      // lbSchema
      // 
      this.lbSchema.AutoSize = true;
      this.lbSchema.Location = new System.Drawing.Point(64, 132);
      this.lbSchema.Name = "lbSchema";
      this.lbSchema.Size = new System.Drawing.Size(0, 13);
      this.lbSchema.TabIndex = 2;
      // 
      // lbRateId
      // 
      this.lbRateId.AutoSize = true;
      this.lbRateId.Location = new System.Drawing.Point(24, 132);
      this.lbRateId.Name = "lbRateId";
      this.lbRateId.Size = new System.Drawing.Size(0, 13);
      this.lbRateId.TabIndex = 8;
      // 
      // label8
      // 
      this.label8.AutoSize = true;
      this.label8.Location = new System.Drawing.Point(80, 15);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(50, 13);
      this.label8.TabIndex = 12;
      this.label8.Text = "No. Viaje";
      // 
      // txtLoad
      // 
      this.txtLoad.Location = new System.Drawing.Point(136, 14);
      this.txtLoad.Name = "txtLoad";
      this.txtLoad.Size = new System.Drawing.Size(116, 20);
      this.txtLoad.TabIndex = 13;
      // 
      // btnRate
      // 
      this.btnRate.Location = new System.Drawing.Point(265, 12);
      this.btnRate.Name = "btnRate";
      this.btnRate.Size = new System.Drawing.Size(75, 23);
      this.btnRate.TabIndex = 14;
      this.btnRate.Text = "Tarificar";
      this.btnRate.UseVisualStyleBackColor = true;
      this.btnRate.Click += new System.EventHandler(this.btnRate_Click);
      // 
      // dataGridView1
      // 
      this.dataGridView1.AllowUserToAddRows = false;
      this.dataGridView1.AllowUserToDeleteRows = false;
      this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dataGridView1.Location = new System.Drawing.Point(12, 41);
      this.dataGridView1.Name = "dataGridView1";
      this.dataGridView1.ReadOnly = true;
      this.dataGridView1.Size = new System.Drawing.Size(539, 289);
      this.dataGridView1.TabIndex = 15;
      // 
      // Form1
      // 
      this.ClientSize = new System.Drawing.Size(563, 342);
      this.Controls.Add(this.dataGridView1);
      this.Controls.Add(this.btnRate);
      this.Controls.Add(this.txtLoad);
      this.Controls.Add(this.label8);
      this.Controls.Add(this.lbRateId);
      this.Controls.Add(this.lbUnitPrice);
      this.Controls.Add(this.lbQuantity);
      this.Controls.Add(this.lbUomCode);
      this.Controls.Add(this.lbSchema);
      this.Controls.Add(this.lbExtendedPrice);
      this.Controls.Add(this.lbModelCode);
      this.Name = "Form1";
      ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Label lbModelCode;
    private System.Windows.Forms.Label lbUomCode;
    private System.Windows.Forms.Label lbQuantity;
    private System.Windows.Forms.Label lbUnitPrice;
    private System.Windows.Forms.Label lbExtendedPrice;
    private System.Windows.Forms.Label lbSchema;
    private System.Windows.Forms.Label lbRateId;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.TextBox txtLoad;
    private System.Windows.Forms.Button btnRate;
    private System.Windows.Forms.DataGridView dataGridView1;
  }
}

